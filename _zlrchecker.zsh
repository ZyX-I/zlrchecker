#compdef zlrchecker.pl zlrchecker
local -a _LIBRULIBS
local -r _OPTDIR="$HOME/.zlrchecker"
local -r _ACACHEDIR="$_OPTDIR/authors"
set -A _LIBRULIBS samlib.ru world.lib.ru lib.ru fan.lib.ru
local -r _LIBRULIBS
local -a CAOPTS
set -A CAOPTS -M 'm:{[:lower:]}={[:upper:]}'
function __getval()
{
    local OPTDIR=$1
    local word
    unsetopt rematchpcre
    local -i I=1
    while (( I<=$#words )) ; do
        [[ "${words[I]}" == '--' ]] && break
        if [[ "${words[I]}" =~ "^($2=|$3)(.+)" ]] ; then
            eval local -r OPTDIR=\"${match[2]}\"
            break
        elif [[ "${words[I]}" == "$2" ]] || [[ "${words[I]}" == "$3" ]] ; then
            eval local -r OPTDIR=\"${words[I+1]}\"
            break
        fi
        (( I++ ))
    done
    echo "$OPTDIR"
}
function __indir()
{
    local -r OPD="${OLDPASSWORD}"
    pushd -q $1 2>/dev/null || return 1
    shift
    $@
    popd -q
    OLDPASSWORD="${OPD}"
}
function __inoptdir()
{
    local -r OPTDIR="$(__getval $_OPTDIR --optdir -o)"
    __indir $OPTDIR $@
}
function __inacachedir()
{
    local -r ACACHEDIR="$(__getval $_ACACHEDIR --acache -a)"
    __inoptdir __indir $ACACHEDIR $@
}
function _pdirectories()
{
    __inoptdir _directories
}
function __publicationlist()
{
    unsetopt rematchpcre
    setopt extendedglob
    setopt nullglob
    local line
    local IFS=
    cat $1(.cache|) | iconv -f UTF-8 -t ascii -c | while read -r line ; do
        if [[ "$line" =~ '^\s+([a-z0-9_\-]+):$' ]] ; then
            echo ${match[1]}
        fi
    done
}
function __sectionlist()
{
    local -a FILES
    setopt extendedglob
    setopt nullglob
    set -A FILES [0-9a-z_\-]*(.cache|)(.)
    set -A FILES ${(L)FILES%.cache}
    local -a CAO
    if [[ ! -z $1 ]] ; then
        set -A CAO -q -S /
    fi
    compadd $CAOPTS $CAO -a FILES
}
function _authors()
{
    __inacachedir __sectionlist
}
function _publications()
{
    compadd $CAOPTS -- $(__inacachedir __publicationlist $(__getval "" --author -A))
}
function _urls()
{
    local -r CURWORD="${words[CURRENT]}"
    if [[ ${CURWORD} =~ '/' ]] ; then
        local PREFIX=${${CURWORD%%/*}##*=}
        local -a Ws
        set -A Ws ${PREFIX}/${^$(__inacachedir __publicationlist ${PREFIX})}
        compadd $CAOPTS -a Ws
    else
        __inacachedir __sectionlist 1
    fi
}
function __anames()
{
    setopt extendedglob
    setopt nullglob
    unsetopt rematchpcre
    setopt rcquotes
    local line
    local IFS=
    local -a R
    grep -m1 -sh '^author: ' [0-9a-z_\-]*(.cache|)(.) | iconv -c -f UTF-8 | while read -r line ; do
        if [[ "$line" =~ '^author:\s+(.*)' ]] ; then
            local str=${match[1]}
            if [[ "$str" =~ "^\\s*'([^']|'')*'\\s*$" ]] ; then
                eval "R+=( $str )"
            elif [[ "$str" =~ "^\\s*\"([^\"\\]|\\.)\"\\s*$" ]] ; then
                eval "R+=( ${(q)${(Q)str}} )"
            elif ! [[ "$str" =~ "^\\s*[\"']" ]] ; then
                eval "R+=( ${(q)str} )"
            fi
        fi
    done
    compadd -a R
}
function _anames()
{
    __inacachedir __anames
}
#TODO:
#   - fix _urls (/)
#   - fix __anames (unicode/corrupted double-linked list)
#   - fix addillustration
#   - --user
#   - post
#   - sectionset
#   - set
#   - add
#   - setfriends
#   - savesection
_arguments -S \
    --prefercache'[Try to load information from cache if available]' \
    --no-prefercache \
    --root"=[Root URL in place of samlib.ru]:libru libraries:($_LIBRULIBS)" \
    -R"-:libru libraries:($_LIBRULIBS)" \
    '(--url -U)--author=[Author (section) part of url]:sections:_authors' \
    '(--url -U)-A-:sections:_authors' \
    '(--url -U)--publication=[Publication part of url]:publications:_publications' \
    '(--url -U)-P-:publications:_publications' \
    '(--author -A --publication -P)--url=[Author, publication and root URL]:urls:_urls' \
    '(--author -A --publication -P)-U-:urls:_urls' \
    --agent'=[UserAgent]:user agents:( )' \
    --user'=[Your *lib.ru login]:login:( )' \
    -u'-:login:( )' \
    --password'=[Your *lib.ru password]::( )' \
    --optdir'=[Options directory]:directories:_directories' \
    -o'-:directories:_directories' \
    --watches'=[Settings file]:files:_files' \
    -w'-:files:_files' \
    --pdir'=[Directory where publications will be saved to (relative to optdir)]:directories:_pdirectories' \
    -p'-:directories:_pdirectories' \
    --acache'=[Directory where caches will be saved to (relative to optdir)]:directories:_pdirectories' \
    -a'-:directories:_pdirectories' \
    --dest'=[Destination file]:files:_files' \
    -O'-:files:_files' \
    --minescq'=:floating point number:(0.0 0.5 1.1)' \
    - '(check)' \
        ':actions:((check\:Check\ for\ updates))' \
    - '(print)' \
        ':actions:((print\:Print\ author\ information))' \
        ':properties:((publications\:List\ of\ publications subscriptions friends\:List\ of\ friends))' \
    - '(post)' \
        ':actions:((post\:Post\ a\ comment))' \
    - '(vote)' \
        ':actions:((vote\:Vote\ for\ publication))' \
        ':marks:(0 1 2 3 4 5 6 7 8 9 10)' \
    - '(addillustration)' \
        ':actions:((addillustration\:Upload\ illustration\ to\ publication))' \
        ':images and mp3 files:_files' \
        ':descriptions:( )' \
        ':authors:_anames' \
    - '(sectionset)' \
        ':actions:((sectionset\:Set\ author\\\x27s\ section\ properties))' \
        '*:section properties:(author)' \
    - '(set)' \
        ':actions:((set\:Set\ publication\\\x27s\ properties))' \
    - '(add)' \
        ':actions:((add\:Add\ publication))' \
        ':files:html files:_files' \
    - '(setfriends)' \
        ':actions:((setfriends\:Change\ friend\ list))' \
    - '(delete)' \
        ':actions:((delete\:Delete\ publication))' \
    - '(savehtml)' \
        ':actions:((savehtml\:Save\ publication\ to\ a\ HTML\ file))' \
    - '(savefb2)' \
        ':actions:((savefb2\:Save\ publication\ to\ a\ FB2\ file))' \
    - '(savecomments)' \
        ':actions:((savecomments\:Save\ comments\ to\ a\ publication))' \
    - '(savesection)' \
        ':actions:((savesection\:Save\ section\ to\ a\ zip\ archive))'

#vim: ts=4:tw=80:ft=zsh
