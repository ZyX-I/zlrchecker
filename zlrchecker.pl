#!/usr/bin/perl
# vim: fdm=marker fmr=▶,▲ tw=80 ts=4 sts=4 et
#▶1 ZLR::StufF
package ZLR::Stuff;
use fields qw(mergeHashes populatePossible getSimilar);
#▶2 mergeHashes
# (hash, hash) -> hash
sub mergeHashes($$) {
    my $hash1 = shift;
    my $hash2 = shift;
    return $hash2 unless defined $hash1;
    return $hash1 unless defined $hash2 and
                         ref $hash2 eq "HASH";
    my $mergedopts={};
    my @keys=keys %$hash1;
    push @keys, (keys %$hash2);
    foreach (@keys) {
        if(not defined $hash2->{$_} or ref $hash2->{$_}) {
            $mergedopts->{$_}=$hash1->{$_}; }
        else {
            $mergedopts->{$_}=$hash2->{$_}; } }
    return $mergedopts;
}
#▶2 populatePossible
# (ZLR::ReturnProp, name, HTML::Element (select)) -> hash
# (Object, Field name, <select>) -> Variant list
# Заполняет список возможных вариантов, используя для этого содержимое тёга 
# <select>.
sub populatePossible($$$) {
    my $where=shift;
    my $what="i_possible".lc(shift);
    return $where->$what if(defined $where->$what);
    my $sel=shift;
    $where->$what({});
    for my $o ($sel->find("option")) {
        my $value=$o->attr("value");
        next unless defined $value;
        $where->$what->{$value}=$o->as_text();
    }
    return $where->$what;
}
#▶2 getSimilar
# (name, string, ZLR::ReturnProp) -> {number => string}
# (Field name, Search string, Object) -> Search result
# Пытается найти str в списке возможных вариантов, созданном функцией 
# populatePossible
sub getSimilar($$$) {
    my $what    = "i_possible".lc(shift);
    my $value   = shift;
    my $where   = (shift)->$what;
    local $_;
    return {$value => $where->{$value}} if(defined $where->{$value});
    my %rev=reverse %{$where};
    return {$rev{$value} => $value} if(defined $rev{$value});
    $value=lc($value);
    for my $key (keys %$where) {
        $_=lc($where->{$key});
        return {$key => $where->{$key}} if(/\Q$value/);
    }
    warn "Invalid value: $value.\n";
    warn "Possible values:\n";
    print STDERR ::YAML::dump($where);
    return undef;
}
#▶1 ZLR::UA
package ZLR::UA;
#▶2 BEGIN
use strict;
use warnings;
use utf8;
use fields qw(get shortGet getLines authorize setUA setRoot postue postfd
              nullCookies restoreCookies);
use vars qw($o_maxtries $o_pmaxtries $o_retryafter $zlr $ua $o_maxsize $user
            $password $aucookies $o_timeout $o_useragent);
use LWP::UserAgent;
use URI::URL;
use Encode;
use Term::ReadPassword;
$o_timeout      = 180;                           # Максимальная длительность 
                                                 # ожидания активности 
                                                 # соединения (в секундах)
$o_maxtries     = 10;                            # Максимальное количество 
                                                 # попыток соединения
$o_pmaxtries    = 3;                             # Максимальное количество 
                                                 # попыток ввода пароля
$o_retryafter   = 5;                             # Количество секунд до 
                                                 # следующей попытки
$o_maxsize      = 2048;                          # Дата обновления должна 
                                                 # быть указана до байта 
                                                 # $o_maxsize
$zlr            = "http://samlib.ru";            # Имя сервера самиздата
$ua             = LWP::UserAgent->new();         # UA, занимается скачиванием
$ua->env_proxy;                                  ##Загрузить настройки proxy 
                                                 ##из переменных окружения
$ua->cookie_jar({});
$ua->timeout($o_timeout);
#▶2 postue
# (URL, array ([str => str])[, string]) -> HTTP::Response
# (URL, fields[, referer]) -> response
# Выполнить закодированный для использования в URL POST запрос, используя array 
# в качестве списка полей и string в качестве referer (если указан).
sub postue($$;$) {
    #▶3 Объявление переменных
    my $url=shift;
    my $fields=shift;
    #▶4 $referer
    my $referer=shift;
    if(defined $referer and $referer eq "" and defined $fields->{"DIR"}) {
        $referer=absURL($url."?DIR=".$fields->{"DIR"}); }
    else {
        $referer=absURL($referer); }
    #▶3 Создание запроса
    my $request=HTTP::Request->new(POST => absURL($url));
    $request->content_type('application/x-www-form-urlencoded');
    $request->content_encoding("UTF-8");
    $request->referer($referer) if(defined $referer);
    $ua->prepare_request($request);
    my $content="";
    #▶4 Заполнение полей
    for my $k (keys %$fields) {
        $content.="&" if($content ne "");
        $k=Encode::decode_utf8($k) unless Encode::is_utf8($k);
        $k=Encode::encode("cp1251", $k);
        $k=URI::Escape::uri_escape($k);
        my $c=$fields->{$k};
        if(not ref $c) {
            $c=Encode::decode_utf8($c) unless Encode::is_utf8($c);
            $c=Encode::encode("cp1251", $c, Encode::FB_HTMLCREF);
            $c=URI::Escape::uri_escape($c);
        }
        elsif(ref $c eq "URI::URL") {
            $c=$c->canonical();
            $c=URI::Escape::uri_escape($c);
        }
        $content.="$k=$c";
    }
    $request->content($content);
    #▶3 Отправка запроса
    my $i=0;
    print STDERR "Doing POST request to url $url".
        (($::o_verbose>2)?(" with fields:\n".
                ::YAML::dump($fields)):("\n"))
        if($::o_verbose>1);
  REQUEST:
    my $response=$ua->simple_request($request);
    $i++;
    my $code=$response->code;
    #▶4 Обработка ошибки 503
    if($i<=$o_maxtries and 500<=$code and $code<600) {
        print STDERR "Failed to request $url with code $code... retrying\n"
            if($::o_verbose>2);
        sleep $o_retryafter;
        goto REQUEST;
    }
    #▲3
    return $response;
}
#▶2 postfd
# (URL, array ([str => Either str array])[, string]) -> HTTP::Response
# (URL, fields[, referer]) -> response
# Выполнить POST запрос формата multipart/form-data указанного адреса. Второй 
# аргумент определяет список полей, третий (необязательный) — referer
sub postfd($$;$) {
    #▶3 Объявление переменных
    my $url=absURL(shift);
    my $content=shift;
    my $referer=shift;
    $referer=absURL($referer) if(defined $referer);
    my $i=0;
    #▶3 verbose message
    print STDERR "Doing POST request (form-data) to url $url".
        (($::o_verbose>2)?(" with fields:\n".
                ::YAML::dump($content)):("\n"))
        if($::o_verbose>1);
    #▶3 Перекодирование запроса
    my $newcontent=[];
    while(my ($f, $c)=splice @$content, 0, 2) {
        if(not ref $c) {
            $c=Encode::decode_utf8($c) unless Encode::is_utf8($c);
            $c=Encode::encode("cp1251", $c, Encode::FB_HTMLCREF);
        }
        push @$newcontent, $f, $c;
    }
    #▶3 Отправка запроса
  POST:
    my $response=$ua->post($url, $newcontent,
                           Content_type => "form-data",
                           ((defined $referer)?(referer => $referer):()));
    $i++;
    my $code=$response->code;
    #▶4 Обработка ошибки 503
    if($i<=$o_maxtries and 500<=$code and $code<600) {
        print STDERR "Failed to download $url with code $code... retrying\n"
            if($::o_verbose>2);
        sleep $o_retryafter;
        goto POST;
    }
    #▲3
    return $response;
}
#▶2 authorize
# ([string[, string]]) -> Bool
# ([user[, password]]) -> Bool
# Попытаться авторизоваться, используя указанное (или сохранённое) имя 
# пользователя и пароль (если он не указан и отсутствует сохранённый пароль, то 
# пароль будет запрошен у пользователя).
sub authorize(;$$) {
    #▶3 Объявление переменных
       $user     = shift unless defined $user;
       $password = shift unless defined $password;
    my $tries    = 0;
    #▶3 Запрос авторизации
  PASSWD:
    $tries++;
    #▶4 Запрос пароля у пользователя
    unless(defined $password) {
        $password=Term::ReadPassword::read_password("Password for $user\: "); }
    #▲4
    my $response=postue("/cgi-bin/login", {
            OPERATION => "login",
            BACK      => absURL("/cgi-bin/login")."",
            DATA0     => $user,
            DATA1     => $password,
        }, "/cgi-bin/login");
    #▶3 Проверка успешности
    if($response->is_success) {
        unless($response->decoded_content=~
                        /\s+<font size="\+2">Д<\/font>обро пожаловать\s+/)
        {
            return undef if($tries>=$o_pmaxtries);
            undef $password;
            goto PASSWD;
        }
    }
    #▶3 Ошибка 404
    elsif($response->code == 404) {
        warn "Failed to authorize with error code 404 (Not Found).\n";
        warn "Maybe you should change your User-Agent.\n";
        return 0;
    }
    #▲3
    else { return 0; }
    $aucookies=$ua->cookie_jar;
    return 1;
}
#▶2 get: загрузить URL
# (URL[, URL][, array ([str => str])]) -> string
# (URL[, URL][, headers]) -> Page contents
# Получить содержимое страницы, находящейся по адресу, указанному в первом URL 
# (второй URL служит для указания текущей страницы). Можно задать заголовки, 
# используемые в запросе.
sub get($;$$) {
    my @fields;
    if(ref $_[-1] eq "ARRAY") {
        @fields=@{pop @_}; }
    my $url=&absURL(@_);
    return undef unless defined $url;
    local $\="";
    #▶3 Загрузка с несколькими попытками
    my $i=0;
  GET:
    print STDERR "Downloading $url\n" if($::o_verbose>1);
    my $response = $ua->get($url, @fields);
    my $code     = $response->code;
    if($i<$o_maxtries and 500<=$code and $code<600) {
        $i++;
        print STDERR "Failed to download $url with code $code... retrying\n"
            if($::o_verbose>2);
        sleep $o_retryafter;
        goto GET;
    }
    #▶3 Все попытки провалились
    unless($response->is_success) {
        warn "Failed to download $url with code $code\n";
        return undef;
    }
    #▲3
    return $response->decoded_content;
}
#▶2 shortGet: Загрузить не более $o_maxsize байт
# То же, что и get(), но с ограничением на размер страницы
sub shortGet($;$$) {
    $ua->max_size($o_maxsize);                    # Нам не нужно всё 
                                                  # произведение
    print STDERR "Partially " if($::o_verbose>1);
    my $result=&get(@_);
    $ua->max_size(undef);
    return $result;
}
#▶2 getLines
# То же, что и get(), но возвращает массив с линиями, а не строку с содержимым 
# страницы.
sub getLines($;@) {
    my $text=&get(@_);
    return () unless defined $text;
    return split /\r?\n/, $text;
}
#▶2 absURL
# (URL[, URL]) -> URL
# Вернуть абсолютный адрес, если дан относительный и текущая страница (по 
# умолчанию — $zlr).
sub absURL($;$) {
    my $url=shift;
    return undef unless defined $url;
    my $current=shift;
    $current=$zlr unless defined $current;
    return URI::URL->new($url, $current)->abs();
}
#▶2 setUA
# (string) -> _
# Сменить UserAgent
sub setUA($) {
    my $newua=shift;
    $newua="Mozilla/5.0 (zlrchecker.pl/$::VERSION, ".$ua->_agent.")"
        unless defined $newua;
    $o_useragent=$newua;
    $ua->agent($o_useragent);
    print STDERR "Changed User-Agent to “$o_useragent”\n"
        if($::o_verbose>2);
    return undef;
}
#▶2 setRoot
# (string) -> _
# Сменить корень ($zlr)
sub setRoot($) {
    $zlr=shift;
    return undef;
}
#▶2 nullCookies
# () -> HTTP::Cookies
# Удаляет все Cookies, возвращая объект с ними
sub nullCookies() {
    my $oldcj=$ua->cookie_jar;
    $ua->cookie_jar({});
    return $oldcj;
}
#▶2 restoreCookies
# ([cookies]) -> _
# Устанавливает Cookies, использует Cookies, сохранённые после успешной 
# авторизации, если в качестве аргумента ничего не указано
sub restoreCookies(;$) {
    my $oldcj = shift || $aucookies || {};
    $ua->cookie_jar($oldcj);
}
#▶1 ZLR::ReturnProp
package ZLR::ReturnProp;
#▶2 BEGIN
use strict;
use warnings;
use fields qw(data save);
#▶2 AUTOLOAD
sub AUTOLOAD {
    return undef unless(our $AUTOLOAD =~ /::([oipc]_)?([a-z]+)$/);
    undef $AUTOLOAD;
    my $field = $2;
    my $where;
    if(not defined $1) { $where="data";     }
    elsif($1 =~ /^o/o) { $where="options";  }
    elsif($1 =~ /^i/o) { $where="info";     }
    elsif($1 =~ /^p/o) { $where="parents";  }
    elsif($1 =~ /^c/o) { $where="children"; }
    my $self  = shift;
    $self->{$where}={} if not defined $self->{$where} and scalar @_;
    return $self->{$where}{$field}=(shift) if(scalar @_);
    return $self->{$where}{$field};
}
#▶2 data
# (ZLR::ReturnProp) -> hash
# Возвращает ссылку на словарь с именем «data» своего объекта
sub data($) {
    my $self=shift;
    return undef unless defined $self;
    return $self->{"data"};
}
#▶2 Pblessed
# (ZLR::ReturnProp | hash | array | scalar) -> (hash | array | scalar)
# Подготавливает объект к сохранению: заменяет все объекты на свои словари 
# «data»
sub Pblessed($);
sub Pblessed($) {
    my $w=shift;
    local $_;
    my $r;
    if(ref $w eq "HASH") {
        $r={};
        foreach (keys %$w) {
            $r->{$_}=Pblessed($w->{$_}); }
    }
    elsif(ref $w eq "ARRAY") {
        $r=[];
        foreach (@$w) {
            push @$r, $_; }
    }
    elsif(not ref $w) {
        return $w; }
    else {
        return Pblessed($w->data); }
    return $r;
}
#▶2 save
# (ZLR::ReturnProp) -> string
# Сохраняет значимые данные объекта в строку
sub save($;@) {
    my $self=shift;
    return undef unless defined $self;
    my $data=$self->data;
    return ::YAML::dump(Pblessed($data), @_);
}
#▶1 ZLR::Comments
package ZLR::Comments;
#▶2 BEGIN
use strict;
use warnings;
use utf8;
use vars qw($cstart $cend @ISA);
use fields qw(getCommentsPage getAllComments);
$cstart=
'<!--/banner-->$';
# '^<center><A\s+HREF="/cgi-bin/comment[^"]*"';
$cend=
'^<center>';
# '^<center><b>Страниц\s+\(\d+\):</b>';
@ISA=('ZLR::ReturnProp');
#▶2 getCommentsPage
# (ZLR::Comments[, Maybe FH[, Maybe natural[, natural]]]) -> _
# (self[, Maybe destination file (self if not specified)[,
#  Maybe page number (first page if not specified)[, archive number]]])
sub getCommentsPage($;$$$) {
    #▶3 Объявление переменных
    my $self      = shift;
    my $FH        = shift;
    my $page      = shift;
    my $arch      = shift;
    my $curl      = $self->p_publication->url("comments");
       $curl      = "$curl.".($arch+0)      if(defined $arch);
       $curl      = "$curl?PAGE=".($page+0) if(defined $page);
    my $indent    = ((defined $arch)?"  ":"");
    my $r         = ((defined $page) + 2*(defined $arch));
    local $\="\n";
    binmode $FH, ":utf8" if(defined $FH);
    #▶4 data
    my @data      = ZLR::UA::getLines($curl);
    return undef unless scalar @data;
    print STDERR "Processing comments page $curl" if($::o_verbose);
    print $FH "---" if(not defined $page and not defined $arch and defined $FH);
    #▶4 Переменные для цикла
    my $inext = 0; # Состояние получения общей информации
    my $next  = 0; # Состояние получения общей ссылки «ответить» 
                   # и количества страниц и архивов
    my $n     = 0; # Состояние получения комментария:
                   #    0 — получение ещё не началось или завершено
                   #    1 — получаем replylink (ссылки «ответить») и MSGID
                   #    2 — получаем первую строку текста комментария
                   #    3 — получаем оставшийся текст комментария
    my $got=$r;    # Обозначает, что мы закончили получать 
                   # информацию о страницах и архивах
    my @comments=();
    local $_;
    #▶3 Основной цикл
    foreach (@data) {
        #▶4 Информация со страницы
        #▶5 «Ответить», страницы и архивы
        if(not($got)) {
            #▶6 Общая ссылка «ответить»
            if(not $next and /<center><A\s+HREF="(\/cgi-bin\/comment[^"]*)"/)
            {
                my $url=ZLR::UA::absURL($1, $curl);
                if($r==0) {
                    if(defined $FH) {
                        print $FH "replylink: ".::YAML::enclose($url); }
                    else {
                        $self->replylink("$url"); }
                }
                $next=3;
            }
            #▶6 Количество страниц и архивов
            elsif($next==1) {
                if($r==0 and /<b>Страниц\s+\((\d+)\):<\/b>/) {
                    my $pagecount=$1+0;
                    if(defined $FH) {
                        print $FH $indent."pagecount: $pagecount"; }
                    $self->pagecount($pagecount);
                }
                if($r==0 and /<b>Архивы\s+\((\d+)\):<\/b>/) {
                    my $archcount=$1+0;
                    if(defined $FH) {
                        print $FH "archcount: $archcount"; }
                    $self->archcount($archcount);
                }
                $got=1;
            }
            #▶6 $next
            # Количество страниц и архивов следует через строку после ссылки 
            # «ответить»
            if($next) {
                $next--;
                $got=1 unless $next;
            }
            #▲6
        }
        #▶5 Общая информация
        if(not($got) and $r==0 and $inext>=0) {
            if($inext==0 and
               /^<li>&copy;\s+Copyright\s+<a\s+href="([^">]*)">(.*?)<\/a>/)
            {
                my $alink=ZLR::UA::absURL($1, $curl);
                $inext=1;
                if(defined $FH) {
                    print $FH "alink: ".::YAML::enclose($alink);
                    print $FH "author: ".::YAML::enclose($2);
                }
                else {
                    $self->alink("$alink");
                    $self->author($2);
                }
            }
            elsif($inext==1 and not /^<li>/) {
                $inext=-1; }
        }
        elsif(not($got) and $r==2) {
            $inext=-1; }
        #▶4 Комментарии
        if(/$cstart/ .. /$cend/) {
            #▶5 Заголовок
            if(s/^(<i>\s*)?<small>(\d+)\.<\/small>\s*//) {
                if($got and not defined $page and ($inext==0 or $inext==-1)) {
                    print $FH $indent."comments:" if(defined $FH);
                    $inext=-2;
                }
                if(defined $FH) {
                    print $FH "$indent-" }
                else {
                    push @comments, {}; }
                #▶6 id
                if(defined $FH) {
                    print $FH "$indent  id: $2";
                    print $FH "$indent  blocked: 1" if(defined $1);
                }
                else {
                    $comments[-1]->{"id"}=$2;
                    $comments[-1]->{"blocked"}=1 if(defined $1);
                }
                #▶6 Комментарий удалён
                if(/^<small><i>Удалено\s([^.]+)\.\s+
                    (\d{4}\/\d{2}\/\d{2}\s\d{2}:\d{2})/x)
                {
                    if(defined $FH) {
                        print $FH "$indent  deleted: ".::YAML::enclose($1);
                        print $FH "$indent  datetime: $2";
                    }
                    else {
                        $comments[-1]->{"deleted"}=$1;
                        $comments[-1]->{"datetime"}=$2;
                    }
                }
                #▶6 не удалён
                else {
                    $n=1;
                    #▶7 Автор комментария не указал свою страницу
                    if(s/^<b>(<font\scolor=red>)?([^<]*)(<\/font>)?<\/b>\s*//) {
                        if(defined $FH) {
                            print $FH "$indent  self: 1" if(defined $1);
                            print $FH "$indent  author: ".::YAML::enclose($2);
                        }
                        else {
                            $comments[-1]->{"self"}=1 if(defined $1);
                            $comments[-1]->{"author"}=$2;
                        }
                    }
                    #▶7 указал
                    elsif(s/^<b>(\*)?
                                <noindex>
                                    <a\shref="([^"]*)"\srel="?nofollow"?>
                                        (<font\scolor=red>)?
                                            ([^<]*)
                                        (<\/font>)?
                                    <\/a>
                                <\/noindex>(<\/font>)?
                            <\/b>\s*//x)
                    {
                        if(defined $FH) {
                            print $FH "$indent  asterisk: 1" if(defined $1);
                            print $FH "$indent  alink: ".::YAML::enclose($2);
                            print $FH "$indent  self: 1" if(defined $3);
                            print $FH "$indent  author: ".::YAML::enclose($4);
                        }
                        else {
                            $comments[-1]->{"asterisk"} = 1 if(defined $1);
                            $comments[-1]->{"alink"}    = $2;
                            $comments[-1]->{"self"}     = 1 if(defined $3);
                            $comments[-1]->{"author"}   = $4;
                        }
                    }
                    #▶7 email автора
                    if(s/^\(<u>([^<]*)<\/u>\)\s*//) {
                        if(defined $FH) {
                            print $FH "$indent  email: ".
                                ::YAML::enclose(::Convert::zlrEmail($1)); }
                        else {
                            $comments[-1]->{"email"}=::Convert::zlrEmail($1); }
                    }
                    #▶7 Время написания комментария
                    if(s/^<small><i>\s*(\d{4}\/\d{2}\/\d{2}\s\d{2}:\d{2})//) {
                        if(defined $FH) {
                            print $FH "$indent  datetime: $1"; }
                        else {
                            $comments[-1]->{"datetime"}=$1; }
                    }
                    #▶7 Комментарии сейчас заблокированы,
                    #     но комментарий уже был оставлен
                    $n=2 if(/<\/small><\/i>\s*$/o);
                    #▲7
                }
            }
            #▶6 Ссылка «ответить»
            elsif($n==1) {
                if(/\[<a\shref="(\/comment\/[^?">]*\?
                                OPERATION=reply&
                                MSGID=(\d+))"/ox)
                {
                    $n=2;
                    if(defined $FH) {
                        print $FH "$indent  referer: ".::YAML::enclose($curl);
                        print $FH "$indent  replylink: ".
                               ::YAML::enclose(ZLR::UA::absURL($1, $curl)."");
                        print $FH "$indent  MSGID: $2";
                        print $FH "$indent  text:";
                    }
                    else {
                        $comments[-1]->{"referer"}   = $curl;
                        $comments[-1]->{"replylink"} =
                                            "".ZLR::UA::absURL($1, $curl);
                        $comments[-1]->{"MSGID"}     = $2;
                        $comments[-1]->{"text"}=[];
                    }
                }
                elsif(/<\/small><\/i>\s*$/o) {
                    $n=2; }
            }
            #▶5 Текст комментария
            #▶6 Первая строка
            elsif($n==2) {
                if(/^<br>&nbsp;&nbsp;(.*?)(<br>\s*)?$/) {
                    $n=defined $2 ? 3 : 0;
                    if(defined $FH) {
                        print $FH "$indent  - ".::YAML::enclose($1); }
                    else {
                        push @{$comments[-1]->{"text"}}, $1; }
                }
            }
            #▶6 Остальные строки
            elsif($n==3) {
                if(/^&nbsp;&nbsp;(.*?)(<br>\s*)?$/) {
                    $n=0 unless defined $2;
                    if(defined $FH) {
                        print $FH "$indent  - ".::YAML::enclose($1); }
                    else {
                        push @{$comments[-1]->{"text"}}, $1; }
                }
                elsif(/^<hr noshade>/) {
                    $n=0; }
            }
            #▲5
        }
        #▲4
    }
    push @{$self->comments}, @comments unless(defined $FH or defined $arch);
    push @{$self->archives->[$arch]{"comments"}}, @comments
                unless(defined $FH or not defined $arch);
    return $self;
}
#▶2 getAllComments
# (ZLR::Comments[, FH]) -> _
# (self[, destination file]) -> _
# Сохраняет все страницы комментариев в формате YAML в указанном файле. Если 
# файл не указан, то сохраняет в self
sub getAllComments($;$) {
    my $self = shift;
    my $FH   = shift;
    $self->comments([]);
    $self->getCommentsPage($FH);
    #▶3 Получение других страниц
    my $pagecount=$self->pagecount || 1;
    if($pagecount > 1) {
        for(my $i=2; $i <= $pagecount; $i++) {
            print $FH "# PAGE $i" if(defined $FH);
            $self->getCommentsPage($FH, $i);
        }
    }
    #▶3 Получение архивов
    my $archcount=$self->archcount || 0;
    if($archcount > 0) {
        print $FH "archives:" if(defined $FH);
        $self->archives([]);
        for(my $i=1; $i <= $archcount; $i++) {
            if(defined $FH) {
                print $FH "\n# ARCHIVE $i\n";
                print $FH "-\n";
            }
            $self->archives->[$i]{"comments"}=[];
            $self->getCommentsPage($FH, undef, $i);
            for(my $j=1; $j <= 26; $j++) {
                print $FH "\n  # PAGE $i\n" if(defined $FH);
                $self->getCommentsPage($FH, $j, $i);
            }
        }
    }
    #▲3
    return $self;
}
#▶2 postComment
# (ZLR::Comments[, Either string FH]) -> Bool
# (self[, comment text]) -> Bool
sub postComment($$) {
    my $self    = shift;
    my $text    = shift;
    if(not defined $text) {
        $text=join "", <STDIN>; }
    if(ref \$text ne "SCALAR") {
        $text=<$text>; }
    my $pubinfo = $self->p_publication;
    my $url     = ZLR::UA::absURL($pubinfo->url("reply"));
    $text=::Convert::escape($text);
    my $ob="&#".ord("[").";";
    my $cb="&#".ord("]").";";
    $text=~s/^(\s*(?:>\s*)+)\Q$ob\E(.*)\Q$cb\E/$1\[$2\]/gm;
    $text=~s/(?<=[^\n])>/&gt;/gs;
    $text=~s/((?!(ht|f)tps?)\b\w+):/$1&#58;/gs;
    $text=~s/</&lt;/;
    $text=~s/\r?\n/\r\n/gs;
    # Получаем Cookies
    ZLR::UA::shortGet(ZLR::UA::absURL($pubinfo->url("comments")));
    my $response=ZLR::UA::postue("/cgi-bin/comment"."", {
            FILE       => "/".$pubinfo->url("fl"),
            MSGID      => "",
            OPERATION  => "store_new",
            NAME       => $self->o_name,
            EMAIL      => $self->o_email,
            URL        => $self->o_url,
            TEXT       => $text,
            COOK_CHECK => 1,
        }, $pubinfo->url("comments"));
    if($response->code!=302) {
        my $text=$response->decoded_content;
        warn "Failed to post comment\n";
        if($text=~/<TABLE\s+BORDER=0\s+CELLPADDING=3\s+WIDTH=350>\s+
                        <TR>\s+
                            <TD\s+ALIGN=CENTER>\s+
                                <BR>\s+
                                <B>(.*?)<\/B>/osx)
        {
            warn "Error message: $1\n";
        }
        if($text =~ /^<center><b><font color=red>(.*?)<\/font>/om) {
            warn "Error message: $1\n";
        }
        return 0;
    }
    return 1;
}
#▶2 new
# (invocant, ZLR::PubInfo) -> ZLR::Comments
sub new {
    my $invocant = shift;
    my $class    = ref $invocant || $invocant;
    my $pubinfo  = shift or return undef;
    my $self     = bless {}, $class;
    $self->p_publication($pubinfo);
    $self->p_author($pubinfo->p_author);
    my $author   = $self->p_author;
    $self->o_url($author->o_defaulturl);
    $self->o_name($author->o_defaultname);
    $self->o_email($author->o_defaultemail);
    $pubinfo->c_comments($self);
    return $self;
}
#▶1 ZLR::PubInfo
package ZLR::PubInfo;
#▶2 BEGIN
use strict;
use warnings;
use utf8;
use fields qw(getDate getInfo getEditInfo setEditInfo url savePublication
              getIllustrations vote addIllustration rawsave);
use vars qw($imgstart $addstart @ISA %einfo %eselinfo);
use HTML::TreeBuilder;
use HTML::Entities;
use POSIX;
use Encode;
use File::Basename;
$imgstart='<hr align="CENTER" size="2" noshade><h3>Иллюстрации:</h3>';
$addstart='<hr align="CENTER" size="2" noshade><h3>Приложения:</h3>';
@ISA=('ZLR::ReturnProp');
#▶3 %einfo
%einfo=(qw(
    AUTHOR     author
    TRANSLATOR translator
    TITLE      title
    DATE       rawdate
));
#▶3 %eselinfo
%eselinfo=(qw(
    TYPE        format
    JANR        genre
    ADD_TYPE    section
    NO_COMMENT  cpermissions
    READ_LEVEL  readpermissions
    WRITE_LEVEL writepermissions
));
#▶2 new
# (invocant, ZLR::Author, string, @) -> ZLR::PubInfo
# (invocant, publication author, source, @) -> ZLR::PubInfo
# source     @           Описание
# ~          _           Новое произведение с минимумом заполненных полей.
# new        string      Новое произведение с полями, заполненными значениями
#                        по умолчанию. Дополнительный аргумент определяет имя 
#                        файла произведения.
# info       hash        Новое произведение с полями, заполненными из хэша.
# line       text        Новое произведение с полями, заполненными из строчки
#                        со страницы со списком произведений
# text       text        Новое произведение с полями, заполненными из страницы
#                        с текстом произведения (дан текст страницы)
# URL        URL         Новое произведение с полями, заполненными из страницы
#                        с текстом произведения (дан адрес страницы)
sub new {
    my $invocant = shift;
    my $class    = ref $invocant || $invocant;
    my $author   = shift;
    my $source   = shift;
    my $self;
    if(not defined $source)  {
        $self=bless {}, $class;
        $self->rawdate(POSIX::strftime("%d/%m/%Y", localtime(time)));
    }
    elsif($source eq "new")  { $self=__PACKAGE__->New(@_);                }
    elsif($source eq "info") { $self=__PACKAGE__->NewFromHASH(@_);        }
    elsif($source eq "line") { $self=__PACKAGE__->NewFromLine(@_);        }
    elsif($source eq "text") { $self=__PACKAGE__->NewFromPublication(@_); }
    elsif($source eq "URL")  { $self=__PACKAGE__->NewFromURL(@_);         }
    $self->p_author($author) if(defined $self);
    return $self;
}
#▶2 New
# (invocant, string) -> ZLR::PubInfo
# (invocant, filename of new publication) -> ZLR::PubInfo
# См. new/new
sub New($$) {
    my $invocant = shift;
    my $class    = ref $invocant || $invocant;
    my $filename = shift;
    my $self     = __PACKAGE__->new();
    my @time     = localtime(time);
    $self->filename($filename);
    $self->annotation("Uploaded on ".POSIX::strftime("%a, %d %b %Y %T %z",
                                                     @time).
                      " by <a href='http://zlrchecker.sourceforge.net/'>".
                      "ZLRChecker.pl</a>");
    $self->title("Added by ZLRChecker.pl / Untitled");
    $self->rawdate(POSIX::strftime("%d/%m/%Y", @time));
    $self->rawreadpermissions({2 => ""});
    $self->rawwritepermissions({4 => ""});
    $self->canvote(1);
    $self->rawcpermissions({0 => ""});
    $self->i_new(1);
    return $self;
}
#▶2 NewFromLine
# (invocant, string) -> ZLR::PubInfo
# Получить информацию о произведении из строчки с произведением на странице со 
# списком произведений
sub NewFromLine($$) {
    my $invocant = shift;
    my $class    = ref $invocant || $invocant;
    local $_=shift;
    /<DL><DT><li>\s*                                    # Начало
     (?<hash><font\s+color=red>\#<\/font>\s*)?          # Красный #
     (?:<font\s+size="-2">
      \[<a\shref=(?<editprop>[^>]*)>Edit<\/a>           # «Редактировать раздел»
      \|<a\shref=(?<edittext>[^>]*)>                    # «Редактировать текст»
        Textedit<\/a>\]<\/font>\s*)?
     (?:<font\s+color=[^>]*?>(?:New|Upd)<\/font>\s*)?   # Новинка?
     (?:<b>\s*(?<authors>[^<]*?)\s*<\/b>)?\s*           # Указаны автор(ы)
     <A\s*HREF=(?<fnamebase>[^>]*?)\.shtml>             # Ссылка на произведение
     <b>(?<title>.*?)<\/b><\/A>\s*\&nbsp;\s*            # Имя произведения
     <b>(?<size>\d+)k<\/b>\s*\&nbsp;\s*<small>          # Размер
     (?:Оценка:<b>\d{1,2}\.\d{2}\*\d+<\/b>              # Оценка
        \s*&nbsp;\s*)?
     \s*(?:"(?<section>@?.*?)")?\s*                     # @Раздел или формат
     \s*(?<genres>[^<]*?)\s*                            # Жанры
     (?:<A\s+HREF="(?<comurl>\/comment\/[^">]*)">       # Ссылка на комментарии, 
         Комментарии:\s*(?<comcnt>\d+)\s*               # их количество 
                    \((?<comdate>\d{2}\/\d{2}\/\d{4})\) # и дата последнего
      <\/A>\s*)?<\/small>\s*
     (?:<br>(?:<DD><font\s+color="\#555555">
        (?<annotation>.*)                               # Аннотация
     <\/font>)?
     (?:<DD><small><a\s+href=(?<imgurl>\/img\/[^>]*)>   # Ссылка на иллюстрации
        Иллюстрации\/приложения:\s*
        (?<imgcnt>\d+)\s*шт\.\s*                        # Количество иллюстраций
      <\/a><\/small>)?<\/DL>)?
    /ox;
    my %data = (
        hash         => ((defined $+{"hash"})? 1 : undef),
        editprop     => $+{"editprop"},
        edittext     => $+{"edittext"},
        author       => $+{"authors"},
        filename     => $+{"fnamebase"},
        title        => $+{"title"},
        size         => $+{"size"}+0,
        section      => $+{"section"},
        genrelist    => $+{"genres"},
        commentslink => $+{"comurl"},
        comments     => ((defined $+{"comcnt"})?($+{"comcnt"}+0):(undef)),
        lastcomment  => $+{"comdate"},
        annotation   => $+{"annotation"},
        imglink      => $+{"imgurl"},
        imgcount     => ((defined $+{"imgcnt"})?($+{"imgcnt"}+0):(undef)),
    );
    unless(defined $data{"section"} and $data{"section"} =~ /^@/) {
        $data{"format"}=$data{"section"};
        delete $data{"section"};
    }
    else {
        $data{"section"} =~ s/^@//; }
    # Удалим лишние (неопределённые) ключи
    foreach (keys %data) {
        delete $data{$_} if(not defined $data{$_}); }
    return bless {data => \%data}, $class;
}
#▶2 NewFromPublication
# (invocant, string) -> ZLR::PubInfo
# (invocant, page source) -> ZLR::PubInfo
# См. new/text
sub NewFromPublication($$) {
    my $invocant = shift;
    my $class    = ref $invocant || $invocant;
    my $self     = __PACKAGE__->new();
    return $self->getInfo(shift);
}
#▶2 NewFromHASH
# (invocant, hash) -> ZLR::PubInfo
# См. new/info
sub NewFromHASH($$) {
    my $invocant = shift;
    my $class    = ref $invocant || $invocant;
    my $hash     = shift;
    return undef unless ref $hash;
    my $self     = __PACKAGE__->new();
    local $_;
    foreach (keys %$hash) {
        my $p=$hash->{$_};
        $self->$_($p);
    }
    return $self;
}
#▶2 NewFromURL
# (invocant, URL) -> ZLR::PubInfo
# См. new/URL
sub NewFromURL($$) {
    my $invocant = shift;
    my $class    = ref $invocant || $invocant;
    my $url      = shift;
    return undef unless defined $url;
    print STDERR "Obtaining publication information from $url\n"
        if($::o_verbose>1);
    my $self=__PACKAGE__->new();
    $self->getInfo(ZLR::UA::shortGet($url));
    $self->source("$url");
    return $self;
}
#▶2 getDate: Получить дату обновления произведения
#              из страницы с самим произведением
# (ZLR::PubInfo) -> string
# (self) -> date
sub getDate($) {
    my $self   = shift;
    my $purl=$self->url;
    return undef unless defined $purl;
    print STDERR "Obtaining publication information from $purl\n"
        if($::o_verbose>1);
    $self->getInfo(ZLR::UA::shortGet($purl));
    $self->source("$purl");
    return $self->update;
}
#▶2 getInfo
# (ZLR::PubInfo, string) -> _
# (self, page source) -> _
# Получить информацию о произведении из текста страницы произведения
sub getInfo($$) {
    #▶3 Объявление переменных
    my    $self = shift;
    local $_    = shift;
    $_=$$_ if(ref $_);
    return undef unless defined $self;
    #▶3 Название произведения
    if(
       /<body\sbgcolor="\#E9E9E9">\s*
        <div\salign="?right"?><h3>\s*
        (.*?):\s<small>
          <a\shref=\/.\/([^>]*)>
            другие\sпроизведения\.
          <\/a>
        <\/small>\s*
        <\/h3><\/div>\s*
        <center><h2>(.*?)<\/h2>/xs)
    {
        $self->authorname("$1");
        $self->author    ("$2");
        $self->title     ("$3");
    }
    #▲3
    #▶3 Объявление переменных для цикла
    my @data = split /\r?\n/, $_;
    my $n=0;
    my $p=0;
    #▲3
    foreach (@data) {
        next if(/<!--/o .. /-->/o);
        $n=1 if(not $n and /^<li>/);
        if($n==1) {
            #▶3 Переводчик и email автора
            if($p) {
                if($p==1 and /^\s*\(перевод:\s+(.*?)\)/o) {
                    $self->translator("$1");
                    $p=2;
                    next;
                }
                elsif(/^\s*\(<u>([^<]*)<\/u>\)/o) {
                    $self->email(::Convert::zlrEmail($1));
                    $p=0;
                    next;
                }
                else { $p=0; }
            }
            $n=2 unless(/^<li>/);
            #▶3 Комментарии
            if(/^<li><A\sHREF="(\/comment\/[^">]*)">
                    Комментарии:\s+(\d+),\s*
                    последний\s+от\s+(\d{2}\/\d{2}\/\d{4})\.
                 <\/A>/x)
            {
                $self->commentslink("$1");
                $self->comments    ($2+0);
                $self->lastcomment ("$3");
            }
            elsif(/^<li><A\sHREF="(\/cgi-bin\/comment[^">]*)">
                        Оставить\s+комментарий
                    <\/A>/x) {
                $self->replylink("$1");
            }
            #▶3 Имя (имена) автора
            elsif(/^<li>&copy;\s+Copyright\s+
                    <a\s+href=\/[^>]*>([^<]*)<\/a>\s*
                    (\(<u>([^<]*)<\/u>\))?/xs)
            {
                $self->author("$1");
                $self->email (::Convert::zlrEmail($3)) if(defined $3);
                $p=1 unless defined $3;
            }
            #▶3 Дата создания и изменения
            elsif(/^<li>Размещен:\s*(\d{2}\/\d{2}\/\d{4}),\s*
                        изменен: \s*(\d{2}\/\d{2}\/\d{4})/x)
            {
                $self->cdate ("$1");
                $self->update("$2");
            }
            elsif(/^<li>Обновлено:\s*(\d{2}\/\d{2}\/\d{4})/) {
                $self->update("$1"); }
            #▶3 Форма и жанры произведения
            elsif(/^<li><a\s+href=\/type\/(index_type[^>]*)\.shtml>
                        ([^<]*)
                    <\/a>:?(.*)/x)
            {
                $self->formatlink("$1");
                $self->format    ("$2");
                my %genres=reverse(
                    $3=~/<a href="(\/janr\/[^>"]*)">([^<]*)<\/a>/g);
                $self->genres    (\%genres);
            }
            #▶3 Раздел, в котором произведение находится
            elsif(/^<li>\s*<a\s+href=(index[^>]*?)>(.*?)<\/a>\s*
                    <\/ul><\/small>\s*<\/td>\s*/x)
            {
                $self->sectionlink("$1");
                $self->section    ("$2");
            }
            #▶3 Наличие иллюстраций
            elsif(/^<li><a\s+href=(\/img\/[^>]*)>
                    Иллюстрации\/приложения:\s+(\d+)\s+шт\.<\/a>/x)
            {
                $self->imglink ("$1");
                $self->imgcount($2+0);
            }
            #▲3
        }
        #▶3 Аннотация и возможность оценивания
        # Как правило, лежит за пределами скачанной части
        elsif($n==2) {
            if(/<ul><small><li><\/small>
                <b>Аннотация:<\/b>\s*<br>\s*
                <font\scolor=\#555555>?<i>\s*(.*?)\s*
                <\/i><\/font><\/ul>\s*<\/td><\/tr><\/table>/x)
            {
                $self->annotation("$1");
                last;
            }
            elsif(/^&nbsp;<font size="-1">Ваша оценка: <select name=BALL><br>/o)
            {
                $self->canvote(1);
            }
        }
        #▲3
    }
    return $self;
}
#▶2 getEditInfo
# (self) -> _
# Получить дополнительную информацию о произведении из страницы редактирования 
# произведения
sub getEditInfo($) {
    #▶3 Объявление переменных
    my $self=shift;
    my $url=$self->url("edit");
    my $text=ZLR::UA::get($self->url("edit"));
    return undef unless defined $text;
    my $tree=HTML::TreeBuilder->new_from_content($text);
    return undef unless defined $tree;
    print STDERR "Processing author-only information from $url\n"
        if($::o_verbose);
    my $form=$tree->find("form");
    return undef unless defined $form;
    #▶3 inputs: автор, переводчик, заглавие, возможность голосования, дата
    for my $key (keys %einfo) {
        my $input=$tree->look_down(name => $key);
        next unless defined $input;
        my $rn=$einfo{$key};
        $self->$rn($input->attr("value"));
    }
    my $input=$tree->look_down(name => "NO_VOTE");
    $self->canvote(0+(not $input->attr("checked"))) if(defined $input);
    #▶3 selects: формат, жанры, раздел, возможность комментирования
    my @selects=($tree->find("select"));
    # for my $sel (@selects) {
    for my $key (keys %eselinfo) {
        my $sel=$tree->look_down(name => $key);
        next unless defined $sel;
        my $rn=$eselinfo{$key};
        ZLR::Stuff::populatePossible($self->p_author, $rn."s", $sel);
        $rn="raw$rn";
        my @os=($sel->look_down(selected => "selected"));
        next unless scalar @os;
        $self->$rn({});
        for my $o (@os) {
            $self->$rn->{$o->attr("value")}=$o->as_text(); }
    }
    #▶3 textareas: аннотация
    my @areas=($tree->find("textarea"));
    for my $a (@areas) {
        my $name=$a->attr("name");
        next unless defined $name;
        if($name eq "ANNOT") {
            my $text=$a->as_text();
            $self->annotation($text) if($text ne "");
        }
    }
    #▲3
    return $self;
}
#▶2 getIllustrations
# (self) -> _
# Получить список иллюстраций и приложений, их описание и автора.
sub getIllustrations($) {
    my $self=shift;
    return undef unless defined $self->imglink;
    my @data=ZLR::UA::getLines($self->imglink);
    return undef unless scalar @data;
    print STDERR "Getting illustration info for ".$self->filename."\n"
        if($::o_verbose);
    my $n=0;
    my $cur="";
    local $_;
    foreach (@data) {
        if(not $n and /^\Q$imgstart/o) {
            $self->illustrations({});
            $n=1;
        }
        elsif((not $n or $n==1) and /^\Q$addstart/o) {
            $self->additions({});
            $n=4;
        }
        if($n==1 and /<table width=640 align=center>/o) {
            $n=2; }
        elsif($n==2) {
            if(/<img src=(\S+) width=(\d+) height=(\d+)/o) {
                $n=3;
                $self->illustrations->{$1}={
                    width  => $2+0,
                    height => $3+0,
                };
                $cur=$1;
            }
            else {
                $n=1; }
        }
        elsif($n==3) {
            $self->illustrations->{$cur}->{"raw"}=$_;
            if(/^\s*<b>(.*)<\/b>\ <i>(.*)<\/i><br>$/o) {
                $self->illustrations->{$cur}->{"description"}=$1;
                $self->illustrations->{$cur}->{"author"}=$2;
            }
            $n=1;
        }
        elsif($n==4 and /<li><a href="([^">]+)">(.*?)<\/a> \((\d+)k\)/o) {
            $n=5;
            $self->additions->{$1}={
                filename => $2,
                size     => $3+0,
            };
            $cur=$1;
        }
        elsif($n==5) {
            $self->additions->{$cur}->{"raw"}=$_;
            if(/^\s*<b>(.*)<\/b>\ <i>(.*)<\/i><br>$/o) {
                $self->additions->{$cur}->{"description"}=$1;
                $self->additions->{$cur}->{"author"}=$2;
            }
            $n=4;
        }
    }
}
#▶2 setEditInfo
# (self[, string[, FH[, Bool]]]) -> _
# (self[, operation[, FH[, doclose]]]) -> _
# Редактировать произведение, создать новое или даже удалить
sub setEditInfo($;$$$) {
    my $self=shift;
    my $author=$self->p_author;
    my $operation=shift || "Store";
    my $FH=shift;
    my $about=1 if($self->filename eq "about");
    $operation="Сохранить! (Store body)" if($about);
    my $text;
    if(defined $FH) {
        $text=::Convert::escape((join "", <$FH>), 1);
        close $FH unless shift;
    }
    my $fields=[
        OPERATION   => $operation,
        DIR         => $author->url("fl"),
        REPLACE     => ((defined $text)? "": "Yes"),
        OWNER       => $ZLR::UA::user,
        TMP_FILE    => "",
        REQUIRED    => "STORE_FILE,TITLE,DATE,JANR,AUTHOR",
        STORE_FILE  => $self->filename,
        TITLE       => ::Convert::escape($self->title),
        AUTHOR      => ((defined $self->author and $self->author ne "")?
                                ($self->author):
                                ($author->info->author)),
        TRANSLATOR  => $self->translator || "",
        (map { (JANR => $_) } (keys %{$self->rawgenre || {0=>undef}})),
        TYPE        => (keys %{$self->rawformat           || {0=>undef}})[0],
        READ_LEVEL  => (keys %{$self->rawreadpermissions  || {2=>undef}})[0],
        WRITE_LEVEL => (keys %{$self->rawwritepermissions || {4=>undef}})[0],
        ADD_TYPE    => (keys %{$self->rawsection          || {0=>undef}})[0],
        NO_COMMENT  => (keys %{$self->rawcpermissions     || {0=>undef}})[0],
        DATE        => $self->rawdate,
        ANNOT       => defined $self->annotation ?
                            ::Convert::escape($self->annotation, 1) : "",
        (($self->canvote)?():(
                NO_VOTE => "1")),
        ((not $about)?
            ((defined $text)?
                (
                    TEXT      => "",
                    CODESET   => "windows-1251",
                    FILE      => [undef, "text.html",
                                  Content      => $text,
                                  Content_type => "text/html"],
                    (($self->i_new)?():
                        (OWERWRITE => 1)),
                ):
                ()):
            (BODY => $text)),
    ];
    my $response=ZLR::UA::postfd("/cgi-bin/zhurnal", $fields);
    if($operation eq "Delete") {
        $fields={
            OPERATION => "delete_object",
            DIR       => $author->url("fl"),
            FILE      => $self->filename,
            del       => "Удалить! (Delete it!)",
        };
        $response=ZLR::UA::postue("/cgi-bin/zhurnal", $fields);
    }
    else {
        my $text=$response->decoded_content;
        unless($text =~ /^\Q<TITLE>302 Found<\/TITLE>\E\s*$/om) {
            warn "Failed to set edit information";
            if($text =~ /^<font color=red><center>(.*?)<br>/om) {
                warn "Error message: $1\n"; }
        }
    }
}
#▶2 addIllustration
# (self, Either string FH, string, string[, Bool[, string]]) -> _
# (self, Either filename FH, description, author name[,
#  doclose[, new filename]]) -> _
sub addIllustration($$$$;$$) {
    my $self        = shift;
    my $filename    = shift;
    my $description = shift;
    my $aname       = shift;
    my $donotclose  = shift;
    my $ext         = lc ".$1" if($filename =~ /\.(\w{3,4})$/o);
       $ext         = "" unless defined $ext;
       $description = "" unless defined $description;
       $aname       = "" unless defined $aname;
    my $FH=((ref $filename)?($filename):(::File::open($filename, 1)));
    my $targetfname = shift;
       $targetfname = (File::Basename::fileparse($filename))[0]
            unless defined $targetfname or ref $filename or $ext =~ /jpe?g|gif/;
    return undef unless defined $FH;
    my $fields=[
        OPERATION => "upload",
        DIR       => $self->p_author->url("fl"),
        FILE      => $self->filename,
        Submit    => "Сохранить! (Store)",
        DESCR     => ::Convert::escape($description, 1),
        AUTH      => ::Convert::escape($aname, 1),
        FILE_BODY => [undef, ((defined $targetfname)?
                                    ($targetfname):
                                    ("file$ext")),
            Content => (join "", <$FH>),
        ],
    ];
    close $FH unless $donotclose;
    my $response=ZLR::UA::postfd("/cgi-bin/img_manager", $fields,
                                            $self->url("editillustrations"));
    unless($response->code == 302) {
        warn "Failed to upload illustration";
        return undef;
    }
    return 1;
}
#▶2 savePublication
# (self, Maybe string) -> _
# (self, what changed) -> _
# Сохранить произведение
sub savePublication($$) {
    #▶3 Объявление переменных
    my $self         = shift;
    my $whatchanged  = shift;
    my $publication  = $self->filename;
    my $author       = $self->p_author;
    my $savedpubinfo = $author->savedinfo->publications->{$publication}
        if(defined $author);
    my ($oldvalue, $newvalue);
    return undef if($whatchanged eq "removed");
    unless($whatchanged eq "added") {
        $newvalue=$self->$whatchanged;
        $oldvalue=$savedpubinfo->$whatchanged
            if(defined $savedpubinfo);
    }
    my $purl        = ZLR::UA::absURL($self->url);
    my $pubdir      = $author->o_pubdir;
    my $maxfnamelen = $author->o_maxfnamelen;
    #▶4 $aname
    my       $aname = $author->o_pfilename;
             $aname = $author->subs($aname, $publication, $whatchanged);
             $aname =~tr/\///d;
             $aname = substr($aname, 0, $maxfnamelen) if($maxfnamelen>0);
             $aname = "$pubdir/$aname.zip";
    #▶4 $fname
    my       $fname = $author->o_pfilename;
             $fname = $author->subs($fname, $publication, $whatchanged);
             $fname =~tr/\///d;
             $fname = substr($fname, 0, $maxfnamelen) if($maxfnamelen>0);
    #▶3 Проверка каталога $pubdir
    unless(-d $pubdir or mkdir $pubdir) {
        warn "Unable to create directory $pubdir!";
        return undef;
    }
    unless(-w $pubdir) {
        warn "Directory $pubdir not writeable!";
        return undef;
    }
    #▶3 Создание архива
    my $arch=main::Archive->new($aname);
    return undef unless defined $arch;
    my $converse=$author->o_converse;
    if($converse =~ /\b(?:html|fb2)\b/) {
        my $html=::Convert::zlrPrepare($author, $self);
        if($converse =~ /\bhtml\b/) {
            $arch->add("$fname.html", $html); }
        if($converse =~ /\bfb2\b/) {
            my $imgarch;
            $imgarch=$arch unless($author->o_alwaysbinary);
            my $fb2=::Convert::toFB2($html, $imgarch, $self, $purl);
            $arch->add("$fname.fb2",  $fb2);
        }
    }
    if($converse =~ /\bnone\b|^(?!.*\b(?:html|fb2)\b)/) {
        $arch->add("$fname.shtml",
                   "<html><body>".$self->rawsave."</body></html>"); }
    $arch->add("$fname.yaml", $self->save);
    close $arch or do { warn "Failed to close $aname archive!"; return undef; };
    $self->lastsave($aname);
    return $aname;
}
#▶2 rawsave
# (self) -> string
# Получить необработанный текст произведения
sub rawsave($) {
    my $self=shift;
    my $text=ZLR::UA::get($self->url);
    $text=~s/$::Convert::pstart/<PublicationContents>/;
    $text=~s/\s*<hr\ssize=2\snoshade>\s*
             <center>\s*
                 <table\sborder=0\scellpadding=0\scellspacing=0>.*?(?=<\/body>)/
              <\/PublicationContents>
             <\/div>/sx;
    $text=~s/.*<PublicationContents>//s;
    $text=~s/<\/PublicationContents>.*//s;
    return $text;
}
#▶2 vote
# (self, unsigned) -> _
# (self, mark) -> _
# Проголосовать за произведение
sub vote($$) {
    my $self=shift;
    my $mark=(shift)+0;
    my $author=$self->p_author;
    my $response=ZLR::UA::postue("/cgi-bin/votecounter", {
            OK   => "OK",
            DIR  => $author->url("fl"),
            FILE => $self->filename,
            BALL => $mark,
        }, $self->url);
    my $tree=HTML::TreeBuilder->new_from_content($response->decoded_content);
    my $ref=$tree->look_down('http-equiv' => 'refresh');
    unless(defined $ref) {
        warn "Failed to vote";
        return undef;
    }
    my $rurl=$ref->attr("content");
    $rurl=~s/^\s*0\s*;\s*url=//o;
    ZLR::UA::shortGet($rurl);
    return $self;
}
#▶2 url
# (self[, string]) -> URL
# (self[, type]) -> URL
# Получить URL какой-либо страницы, связанной с произведением
# type      Описание
# ~         URL страницы с произведением
# fl        a/author/publication
# comments  URL страницы комментариев
# reply     URL ссылки «ответить»
# edit      URL страницы редактирования произведения
# edittext  URL страницы редактирования текста произведения
# editillustrations  URL страницы редактирования иллюстраций и приложений
sub url($;$) {
    my $self        = shift;
    my $what        = shift;
    my $publication = $self->filename;
    my $aurl        = $self->p_author->url("fl");
    return undef unless defined $aurl;
    my $url         = "$aurl/$publication";
    return "/$url.shtml"   unless defined $what;
    return   $url          if($what eq "fl");
    return "/comment/$url" if($what eq "comments");
    return $self->replylink || "/cgi-bin/comment?COMMENT=$url"
                           if($what eq "reply");
    return "/cgi-bin/img_manager?DIR=$aurl&FILE=$publication"
                           if($what eq "editillustrations");
    my $zurl="/cgi-bin/zhurnal";
    return $self->editprop ||
           "$zurl?OPERATION=edit_book&FILE=$publication&DIR=$aurl"
                           if($what eq "edit");
    return $self->edittext ||
           "$zurl?OPERATION=(Text+edit)&DIR=$aurl&STORE_FILE=$publication"
                           if($what eq "edittext");
}
#▶1 ZLR::Info
package ZLR::Info;
#▶2 BEGIN
use strict;
use warnings;
use utf8;
use vars qw($ustart $uend $istart $aend @ISA %einfo %eselinfo);
use fields qw(save getBibliography getTop getSeeAlso getFriends getStartWithList
              getEditInfo setEditInfo setFriends setStartWith setPasswd
              setImage setTop setSeeAlso getSubSections setSubSections
              getSubscriptions setSubscriptions getImages getPossibleSections);
use HTML::TreeBuilder;
use Image::Info;
@ISA=('ZLR::ReturnProp');
$ustart =                                        # Начало списка произведений
'<!-------- вместо <body> вставятся ссылки на произведения! ------>';
$uend =                                          # Конец списка произведений
'<!--------- Подножие ------------------------------->';
$istart='<!----   Блок шапки (сведения об авторе) ----------->';
$aend=                                           # Конец аннотации
'<!------------------- Блок управления разделом -------------------------->';
#▶3 einfo
%einfo=(qw(
    AUTHOR     author
    TITLE      title
    EMAIL      email
    SUBS_EMAIL subscriptionemail
    SUBS_ALL   commentssubscribe
    HOME_LINK  www
    ADD_TYPE   rawsectionlist
    DATE       updated
    TOWN       rawtown
    BD         rawbirthdate
    LAST_ADD   rawlastadd
));
#▶3 eselinfo
%eselinfo=(qw(
    READ_LEVEL   readpermissions
    WRITE_LEVEL  writepermissions
    READ_DEFAULT booksreadpermissions
));
#▶2 new: Получить список произведений
# (invocant[, string, ZLR::Author, False | 1 | 2]) -> ZLR::Info
# (invocant[, string "author", author, source]) -> ZLR::Info
# source  Описание
# False   Получить информацию со страницы со списком произведений
#   1     Получить информацию из кэша, в котором сохраняется информация
#         с предыдущей проверки
#   2     Получить информацию из кэша, в котором сохраняется информация
#         с предыдущего запуска
sub new {
    my $invocant = shift;
    my $class    = ref $invocant || $invocant;
    my $source   = shift;
    return bless {
        data => {
            annotations  => {},            # Аннотации к подразделам
            publications => {},            #%Список произведений
            sections     => [],            #%//Список подразделов (не исп.)
            ordered      => [],            #%Список произведений в порядке
        }                                  # появления на странице
    }, $class
                    unless defined $source;
    return __PACKAGE__->NewFromAuthor(@_) if($source eq "author");
    return undef;
}
#▶2 NewFromAuthor
# (invocant, ZLR::Author[, False | 1 | 2]) -> ZLR::Info
# (invocant, author[, source]) -> ZLR::Info
# См. new
sub NewFromAuthor($$;$) {
    my $invocant = shift;
    my $class    = ref $invocant || $invocant;
    my $author   = shift;
    my $what     = shift;
    my $self=__PACKAGE__->new();
    $self->p_author($author);
    if($what) {
        my $fname=$author->file;
        $fname.=".cache" if($what == 2);
        if(-r $fname) {
            $self->FromHASH(::YAML::load($fname)) or return undef;
            $author->i_isold(1);
        }
        elsif($what==2) {
            $self->FromURL($author->url("dateindex")) or return undef; }
    }
    else {
        $self->FromURL($author->url("dateindex")) or return undef; }
    return $self;
}
#▶2 FromHASH
# (ZLR::Info, hash) -> _
# (self, hash) -> _
# Заполнить поля из хэша
sub FromHASH($$) {
    my $self=shift;
    my $hash=shift;
    return undef unless defined $hash and ref $hash;
    local $_;
    if(not defined $hash->{"publications"}) {
        warn "No publications in HASH";
        return undef;
    }
    foreach (keys %{$hash->{"publications"}}) {
        my $p=$hash->{"publications"}{$_};
        $self->publications->{$_}=
            ((ref $p eq "ZLR::PubInfo")?
                ($p):
                (ZLR::PubInfo->new($self->p_author, info => $p)));
    }
    foreach (keys %$hash) {
        next if($_ eq "publications");
        next unless /^[a-z]+$/;
        $self->$_($hash->{$_});
    }
    return $self;
}
#▶2 FromURL
# (ZLR::Info, URL) -> _
# (self, URL) -> _
# Заполнить плоля, используя информацию с указанной страницы
sub FromURL($$) {
    #▶3 Объявление переменных
    my $self = shift;
    my $url  = shift;
    my $r    = $self->ParseMain($url);
    return undef unless defined $r;
    $self->srcurl($url);
    #▶3 //Получить произведения в разделах
    # Не используется: на странице indexdate нет разделов
    if(scalar @{$self->sections}) {
        for my $section (@{$self->sections}) {
            my $info2=$self->ParseMain("$url/index_$section.shtml",
                                            $section);
            next unless defined $info2;
            for my $publication (keys %{$info2->{"publications"}}) {
                $self->publications->{$publication}=
                    $info2->{"publications"}{$publication};
            }
        }
    }
    #▲3
    return $self;
}
#▶2 ParseMain: Получить список произведений и другую информацию
#                из страницы со списком произведений
# (ZLR::Info, URL[, string]) -> _
# (self, URL[, current section]) -> _
sub ParseMain($;$$) {
    #▶3 Объявление переменных
    my $self           = shift;
    my $url            = shift;
    my $currentsection = shift;         #//Текущий подраздел (не исп.)
    $url=~m@(http://[^/]*)?/(.)/([^/]+)@;
    my $au="/$2/$3/";                   # Часть URL, используется для нахождения 
                                        # имени автора на странице раздела
    #▶4 page/data
    print STDERR "Obtaining information from $url\n" if($::o_verbose);
    my @data=ZLR::UA::getLines($url);
    return undef unless scalar @data;
    my $n=0;
    my $a="";
    my $aa="";
    #▶3 Построчное прочтение страницы
    foreach (@data) {
        #▶4 Информация о произведении
        # Глобальные $ustar и $uend определяют две строчки, с одной начинается 
        # список произведений, а на другой он заканчивается.
        if(/\Q$ustart/o .. /\Q$uend/o) {
            $n=-1;
            # Довольно общее регулярное выражение, позволяющее определить, не 
            # содержит ли данная строчка ссылку на произведение. За исключением 
            # имени файла, данные достаются с помощью функции 
            # GetPublicationData.
            if(/^<DL><DT><li>.*?<A HREF=(.*?)\.shtml>.*?<b>(\d+)k<\/b>/o) {
                $self->publications->{$1} = ZLR::PubInfo->new($self->p_author,
                                                              line => $_);
                $self->publications->{$1}->rsection($currentsection)
                            if(defined $currentsection);
                $self->publications->{$1}->author($self->author)
                            unless defined $self->publications->{$1}->author;
                push @{$self->ordered}, $1;
            }
            #▶5 Список лучших и библиография
            elsif(/^<h3>(Список лучших|Библиография):<\/h3>/o) {
                if(/<h3>Список лучших:<\/h3>/o) {
                    $self->hastop(1); }
                if(/<h3>Библиография:<\/h3>/o) {
                    $self->hasbibl(1); }
            }
            #▶5 См также.
            elsif(/^<h3>Смотрите\ также:<\/h3>/o .. /^<\/div>\s*$/o) {
                $self->seealso([]) unless(defined $self->seealso);
                if(/<DL><DT><li\ TYPE=square><b><a\ href="([^">]*)">
                    <font\ color="\#555555">(.*)<\/font><\/a>
                    \ <i>(.*)<\/i><\/b><\/DL>$/ox)
                {
                    push @{$self->seealso}, {
                        $1 => {
                            title       => $2,
                            description => $3
                        }
                    };
                    delete $self->seealso->[-1]{$1}{"description"}
                        if($3 eq "");
                }
            }
            #//▶5 Раздел
            # Здесь мы определяем раздел. Реально не используется, так как на 
            # странице indexdate нет разделов.
            #▶6 Раздел на данной странице
            # Раздел, список произведений которого находится здесь же, на 
            # странице
            elsif(/^<\/small><p><font size=\+1>.*?<gr0>/o) {
                if(/<a href=\/type\/index_([^>]*?)\.shtml/o) {
                    $currentsection="inline-type-$1/";
                }
                elsif(/<a name=gr([1-9]\d*)>/o) {
                    $currentsection="inline-$1/";
                }
            }
            #▶6 Раздел на отдельной странице
            elsif(/<a name=gr(\d+)><a href=index_\1\.shtml>/o) {
                push @{$self->sections}, $1; }
            #▲5
        }
        elsif($n==-1) { last; }
        #▶4 Информация о разделе
        # Но, тем не менее, информация о разделе лежит выше строчки, с которой 
        # начинается список произведений. Нас интересует время обновления 
        # и автор (впрочем, сейчас время обновления не используется).
        elsif(/\Q$istart/o .. /<\/td><\/tr><\/table>/o) {
            #▶5 WWW
            if(/<li><b>WWW:<\/b>\s+<noindex><a\s+href="([^">]*)">/o) {
                $self->www("$1"); }
            #▶5 Email
            elsif(/<li><b>Aдpeс:<\/b>\s*<u>([^<]*)<\/u>/o) {
                $self->email(::Convert::zlrEmail($1)); }
            #▶5 Страна и город
            elsif(/<font\s+color=\#393939>Живет:<\/font><\/a><\/b>\s*
                   (.*?)(,(.*))?$/xo)
            {
                $self->country("$1");
                $self->city("$3") if(defined $3);
            }
            #▶5 Дата рождения
            elsif(/<font\s+color=\#393939>Родился:<\/font><\/a><\/b>\s*(.*)/o) {
                $self->birthday("$1"); }
            #▶5 Время обновления
            elsif(/<font color=\#393939>Обновлялось:<\/font><\/a><\/b>\s*(.*)/o)
            {
                $self->updated("$1");
            }
        }
        #▶5 Автор
        elsif((/^(.*?):\s
                <small>
                    <a\shref=\Q$au\E>другие\sпроизведения\.<\/a>
                <\/small>/xo or /^<h3>(.*?):<br>$/o) and
              (not defined $self->author or $self->authorfromtitle))
        {
            $self->author("$1");
            $self->authorfromtitle(0);
            $n=1;
        }
        elsif(/^\s+<title>(.*)\.\s*(.*?)\.\s*(.*?)\s*<\/title>\s*$/) {
            $self->authorfromtitle(1);
            $self->librutitle("$1");
            $self->author("$2");
            $self->title("$3");
        }
        #▶5 Заголовок раздела
        elsif($n==1) {
            if(/^<font color="\#c5c5c5">(.*)<\/font><\/h3>$/o) {
                $self->title("$1"); }
            else { $n=2; }
        }
        #▶5 about
        elsif(/^<a href=about\.shtml>/o) {
            $n=3;
            $self->hasabout(1);
        }
        elsif($n==3) {
            $n=4;
            $self->aimage(ZLR::UA::absURL($1, $url)."") if(/<img.*?src=(\S*)/);
        }
        #▶5 Начните знакомство с
        elsif(/^<br><b>Начните знакомство с/) {
            $self->startwith  ([ /<a href=([^>]*)\.shtml>/g ]); }
        #▶5 Аннотация к разделу
        elsif(/^<br><b>
                <font\s+color=\#393939>
                    Аннотация\s+к\s+разделу:
                <\/font><\/b><i>/xo .. /$aend/o)
        {
            $a.="$_\n";
        }
        #▶5 Аннотация к подразделу
        elsif(/^<small><b>Аннотация<\/b>:/o .. /^<ul>$/o) {
            $aa.="$_\n"; }
        #▲4
    }
    #▲3
    return undef unless $n==-1;
    #▶3 Аннотация
    if($a ne "") {
        $a=~s/^.*?<i>(.*)<\/i>.*?$/$1/s;
        $self->annotation($a);
    }
    if($aa ne "") {
        $aa=~s/^.*?<i>(.*)<\/i>.*?$/$1/s;
        $self->annotations->{$currentsection}=$aa;
    }
    #▲3
    return $self;
}
#▶2 getBibliography
# (ZLR::Info) -> _
# (self) -> _
# Получить библиографию автора
sub getBibliography($) {
    #▶3 $self->hasbibl
    my $self = shift;
    return undef unless $self->hasbibl;
    #▶3 Переменные для цикла
    my $bibliography=[];
    $self->bibliography($bibliography);
    my @data=ZLR::UA::getLines($self->url("bibliography"));
    return undef unless scalar @data;
    print STDERR "Obtaining bibliography information for ".$self->author."\n"
                if($::o_verbose);
    my $n=0;
    my $c=0;
    #▶3 Цикл
    foreach (@data) {
        if(/^<hr\ align="CENTER"\ size="2"\ noshade>\s*$/o .. /^<center>/o) {
            $n=1;
            #▶4 Первая строчка — никакой информации
            if($c==0 and /^<DL><DT><li>/o) {
                $c=1; }
            #▶4 Вторая строчка — ссылка на произведение
            elsif($c==1) {
                if(/<b><a href=([^>]*)\.shtml>/o) {
                    push @$bibliography, $1;
                    $c=2;
                }
                else { $c=0; }
            }
            #▶4 Третья строчка — остальная полезная информация
            elsif($c==2) {
                $c=0;
                if(/(<i>(.*?)<\/i>:)?                        # Издательство
                    \ <font\ color=\#555555>([^<]*)<\/font>  # Тип публикации
                    \ "(.*?)"<\/b><small>                    # Название
                    (\ (.*?))?,                              # Номер в сборнике
                    (\s+<i>(\d{2}\/\d{2}\/\d{4})<\/i>,?)?    # Дата публикации
                    (\s+тир\.<b><i>(\d+)<\/i><\/b>,?)?       # Тираж
                    (\s+ISBN:<b><i>(.*?)<\/i><\/b>,?)?       # ISBN
                    (\s*\[<a\ href=([^>]*)>купить\.\.\.)?    # Ссылка на магазин
                    /ox)
                {
                    my $publication=$bibliography->[-1];
                    $bibliography->[-1]={
                        $publication => {
                            publisher => $2,
                            type      => $3,
                            name      => $4,
                            num       => $6,
                            date      => $8,
                            copynum   => (defined $10 ? $10+0 : undef),
                            isbn      => $12,
                            buy       => $14,
                        }
                    };
                    for my $key (keys %{$bibliography->[-1]{$publication}}) {
                        delete $bibliography->[-1]{$publication}{$key}
                            unless
                                defined $bibliography->[-1]{$publication}{$key};
                    }
                }
            }
            #▲4
        }
        elsif($n) { last; }
    }
    #▶3 rawbibliography
    my $text=ZLR::UA::get($self->url("bibledit"));
    return $self unless defined $text;
    my $tree=HTML::TreeBuilder->new_from_content($text);
    my @selects=$tree->find("select");
    my @inputs=$tree->find("input");
    $self->rawbibliography([]);
    for my $s (@selects, @inputs) {
        my $name=$s->attr("name");
        next unless $name =~ s/(\d+)$//o;
        my $n=$1;
        my $value;
        if($s->tag eq "select") {
            ZLR::Stuff::populatePossible($self->p_author, lc "b$name", $s);
            $value=$s->look_down(selected => "selected");
            $value=$value->attr("value") if(defined $value);
            if($name eq "URL") {
                next if($value eq 0);
                $self->rawbibliography->[$n-1]={};
            }
        }
        else {
            $value=$s->attr("value"); }
        next unless defined $value;
        next unless defined $self->rawbibliography->[$n-1];
        $self->rawbibliography->[$n-1]->{$name}=$value;
    }
    #▲3
    return $self;
}
#▶2 getTop
# (ZLR::Info) -> _
# (self) -> _
# Получить «список лучших»
sub getTop($) {
    my $self = shift;
    return undef unless $self->hastop;
    my @data=ZLR::UA::getLines($self->url("top"));
    return undef unless scalar @data;
    print STDERR "Obtaining top information for ".$self->author."\n"
            if($::o_verbose);
    my $n=0;
    my $top=[];
    $self->top($top);
    foreach (@data) {
        if(/^<hr align="CENTER" size="2" noshade><DL><DT>/o ..
           /<hr align="CENTER" size="2" noshade>\s*$/o)
        {
            $n=1;
            if(/<DL><DT><li><b>
                <a\ href=(\/cgi-bin\/best_show[^>]*)>
                    (\d+)                              # Оценка
                <\/a>\ :\ <\/b>
                <a\ href=\/.\/([^\/>]*)\/>             # Ссылка на раздел автора
                    <font\ color=\#555555>
                        (.*?)                          # Имя автора
                    <\/font>
                <\/a>:\s
                <a\ href=[^>]*?\/([^\/>]*)\.shtml>     # Ссылка на произведение
                    <b>(.*?)<\/b>                      # Название произведения
                <\/a>
               /x)
            {
                push @$top, {
                    mark        => $2+0,
                    authorlink  => $3,
                    author      => $4,
                    publication => $5,
                    title       => $6,
                };
            }
            if(/<DD><font color=\#555555><i>(.*)<\/i><\/font>\s*<\/DL>/) {
                $top->[-1]{"annotation"}=$1;
            }
        }
        elsif($n) { last; }
    }
    return $self;
}
#▶2 getSeeAlso
# (ZLR::Info) -> _
# (self) -> _
# Получить список ссылок «см. также»
sub getSeeAlso($) {
    my $self = shift;
    return undef unless defined $self->seealso and scalar @{$self->seealso};
    my @data=ZLR::UA::getLines($self->url("seealso"));
    return undef unless scalar @data;
    print STDERR "Obtaining seealso information for ".$self->author."\n"
            if($::o_verbose);
    my $n=0;
    my $seealso=[];
    $self->seealso($seealso);
    foreach (@data) {
        if(/^<hr align="CENTER" size="2" noshade><DL><DT>/o ..
           /<hr align="CENTER" size="2" noshade>\s*$/o)
        {
            $n=1 unless $n;
            if($n==1 and
               /<DL><DT><li\ TYPE=square><b>
                <a\ href="([^">]*)">
                    <font\ color="\#555555">(.*)
                <\/a><\/b>\s*$
               /xo)
            {
                $n=2;
                push @$seealso, [
                    $1 => $2
                ];
            }
            elsif($n==2 and
                  /^<i>(.*)<\/i><\/font><\/a><\/b><\/DL>\s*$/o)
            {
                $n=1;
                my $link=$seealso->[-1][0];
                $seealso->[-1]={
                    $link => {
                        title       => $seealso->[-1][1],
                        description => $1,
                    }
                };
                delete $seealso->[-1]{$link}{"description"} if($1 eq "");
            }
        }
        elsif($n) { last; }
    }
    return $self;
}
#▶2 getFriends
# (ZLR::Info) -> _
# (self) -> _
# Получить список объявленных дружескими разделов
sub getFriends($) {
    my $self=shift;
    my @data=ZLR::UA::getLines($self->url("friends"));
    return undef unless scalar @data;
    print STDERR "Obtaining friend list for ".$self->author."\n"
            if($::o_verbose);
    my $n=0;
    my $friends=[];
    $self->friends($friends);
    foreach (@data) {
        if(/^<table><tr><td><b>FRIENDS: \((\d+)\) &gt; <\/b><\/td><\/tr>/o) {
            $n=1;
            $self->friendsnumber($1+0);
            next;
        }
        elsif(/^<table><tr><td><b>Friends&FriendOf: &lt; \((\d+)\) &gt;<\/b>/o){
            $n=1;
            $self->bothfriends($1+0);
            next;
        }
        elsif(/^<\/table><br>\s*$/) {
            next; }
        next unless $n;
        if(/^<tr><td>(<b>)?<a href=\/(.)\/(\2[^>]*)\/>/o) {
            push @$friends, $3; }
        else {
            last; }
    }
    my $text=ZLR::UA::get($self->url("friendssetpage"));
    return $self unless defined $text;
    $self->frsubscribe(1)
        if($text=~/<input\s+type=checkbox\s+name=subs_fr\s+checked/o);
    return $self;
}
#▶2 getStartWithList
# (ZLR::Info) -> _
# (self) -> _
# Получить список произведений «начните знакомство с»
sub getStartWithList($) {
    my $self=shift;
    my $url=$self->url("startwithedit");
    my $text=ZLR::UA::get($url);
    return undef unless defined $text;
    print STDERR "Obtaining “start with” list from $url\n"
        if($::o_verbose);
    my $tree=HTML::TreeBuilder->new_from_content($text);
    my $form=$tree->find("form");
    return undef unless defined $form;
    my @selects=($form->find("select"));
    ZLR::Stuff::populatePossible($self->p_author, "publications", $selects[0]);
    $self->rawstartwith([]);
    for my $sel (@selects) {
        my $swN=$sel->look_down(selected => "selected");
        next unless defined $swN;
        push @{$self->rawstartwith}, $swN->attr("value");
    }
}
#▶2 getSecretEmail
# (ZLR::Info) -> _
# (self) -> _
# Получить адрес электронной почты, использующийся для восстановления пароля
sub getSecretEmail($) {
    my $self = shift;
    my $url  = $self->url("passwd");
    my $text=ZLR::UA::get($url, [
            referer => ZLR::UA::absURL($self->url("editor")),
        ]
    );
    return undef unless defined $text;
    print STDERR "Obtaining secret email from $url\n"
        if($::o_verbose);
    my $tree=HTML::TreeBuilder->new_from_content($text);
    my $input=$tree->look_down(name => "DT2");
    return undef unless defined $input;
    $self->secretemail($input->attr("value"));
    return $self;
}
#▶2 getEditInfo
# (ZLR::Info) -> _
# (self) -> _
# Получить дополнительную информацию о разделе со страницы редактирования 
# свойств раздела
sub getEditInfo($) {
    #▶3 Объявление переменных
    my $self=shift;
    my $url =$self->url("editprop");
    my $text=ZLR::UA::get($url, [
            referer => ZLR::UA::absURL($self->url("editor")),
        ]
    );
    return undef unless defined $text;
    print STDERR "Obtaining section edit information from $url\n"
        if($::o_verbose);
    my $tree=HTML::TreeBuilder->new_from_content($text);
    #▶3 inputs
    for my $key (keys %einfo) {
        my $input=$tree->look_down(name => $key);
        next unless defined $input;
        my $rn=$einfo{$key};
        $self->$rn($input->attr("value"));
    }
    #▶3 selects
    for my $key (keys %eselinfo) {
        my $sel=$tree->look_down(name => $key);
        next unless defined $sel;
        my $rn=$eselinfo{$key};
        ZLR::Stuff::populatePossible($self->p_author, $rn, $sel);
        my $o=$sel->look_down(selected => "selected");
        $rn="raw$rn";
        $self->$rn({$o->attr("value") => $o->as_text()});
    }
    #▶3 Аннотация
    my $textarea=$tree->look_down(name => "ANNOT");
    return undef unless defined $textarea;
    $self->annotation($textarea->as_text());
    #▲3
    return $self;
}
#▶2 getPossibleSections
# (ZLR::Info) -> _
# (self) -> _
# Получить список подразделов, который можно использовать при создании 
# нового/изменении произведения
sub getPossibleSections($) {
    my $self=shift;
    my $url=$self->url("newbook");
    my $text=ZLR::UA::get($url);
    my $tree=HTML::TreeBuilder->new_from_content($text);
    my $sel=$tree->look_down(name => "ADD_TYPE");
    return ZLR::Stuff::populatePossible($self->p_author, "sections", $sel);
}
#▶2 getSubSections
# (ZLR::Info) -> _
# (self) -> _
# Получить список подразделов и аннотаций к ним
sub getSubSections($) {
    my $self=shift;
    my $url=$self->url("subsectionsedit");
    my $text=ZLR::UA::get($url);
    return undef unless defined $text;
    my $tree=HTML::TreeBuilder->new_from_content($text);
    return undef unless defined $tree;
    $self->rawsubsections([]);
    my @inputs=$tree->find("input");
    my @textareas=$tree->find("textarea");
    return undef unless scalar @inputs;
    for my $t (@inputs, @textareas) {
        my $name=$t->attr("name");
        next unless $name =~ s/(\d+)$//o;
        my $n=$1;
        my $value;
        if($name eq "ANNOT")   { $value=$t->as_text();     }
        elsif($name eq "TITL") { $value=$t->attr("value"); }
        else { next; }
        if($name eq "TITL") {
            next if($value eq "");
            $self->rawsubsections->[$n-1]={};
        }
        next unless defined $value;
        next unless defined $self->rawsubsections->[$n-1];
        $self->rawsubsections->[$n-1]->{$name}=$value;
    }
    return $self;
}
#▶2 getSubscriptions
# (ZLR::Info) -> _
# (self) -> _
# Получить список подписок на комментарии
sub getSubscriptions($) {
    my $self=shift;
    my $url=$self->url("subscriptionedit");
    my $text=ZLR::UA::get($url);
    return undef unless defined $text;
    my $tree=HTML::TreeBuilder->new_from_content($text);
    return undef unless defined $tree;
    return undef if($tree->look_down(name => "DATA1"));
    $self->rawsubscriptions([]);
    my @inputs=$tree->find("input");
    for my $input (@inputs) {
        my $name=$input->attr("name");
        next unless defined $name and $name =~ /^URL/;
        my $value=$input->attr("value");
        last if($value eq "");
        push @{$self->rawsubscriptions}, $value;
    }
    return $self;
}
#▶2 getImages
# (ZLR::Info) -> _
# (self) -> _
# Получить список иллюстраций к подразделу
sub getImages($) {
    my $self=shift;
    my $url=$self->url("imageedit");
    my $text=ZLR::UA::get($url);
    return undef unless defined $text;
    my $tree=HTML::TreeBuilder->new_from_content($text);
    return undef unless defined $tree;
    my @images=($tree->find("img"));
    return undef unless scalar @images;
    local $_;
    $self->sectionimages([map {$_->attr("src")} @images]);
    return $self;
}
#▶2 setSubscriptions
# (ZLR::Info) -> _
# (self) -> _
# Установить список рассылок новых комментариев
sub setSubscriptions($) {
    my $self=shift;
    my $fields={
        OPERATION => "replace",
        Store     => "Store (Сохранить)",
        DIR       => $self->url("fl"),
        NUM       => (scalar(@{$self->rawsubscriptions})),
    };
    my $i=0;
    for my $p (@{$self->rawsubscriptions}) {
        $i++;
        $fields->{"URL$i"}=$p;
    }
    my $response=ZLR::UA::postue("/cgi-bin/subsedit", $fields, "");
    unless($response->code == 302) {
        my $text=$response->decoded_content;
        warn "Failed to set subscription list\n";
        if($text=~/<center><font color=red>(.*?)<\/font>:?<\/center>/o) {
            warn "Error message: $1\n"; }
        return undef;
    }
    return 1;
}
#▶2 setBibliography
# (ZLR::Info) -> _
# (self) -> _
# Установить библиографию
sub setBibliography($) {
    my $self=shift;
    return undef unless defined $self->rawbibliography;
    my $fields={
        Store     => "Store (Сохранить)",
        DIR       => $self->url("fl"),
        OPERATION => "replace",
        NUM       => scalar @{$self->rawbibliography},
    };
    my $i=0;
    for my $h (@{$self->rawbibliography}) {
        $i++;
        for my $k (keys %$h) { $fields->{"$k$i"}=$h->{$k}; }
    }
    my $response=ZLR::UA::postue("/cgi-bin/publish", $fields, "");
    unless($response->code == 302) {
        warn "Failed to set bibliography\n";
        return undef;
    }
    return 1;
}
#▶2 setSubSections
# (ZLR::Info) -> _
# (self) -> _
# Установить список подразделов и аннотации к ним
sub setSubSections($) {
    my $self=shift;
    my $fields={
        Store     => "Store (Сохранить)",
        DIR       => $self->url("fl")."/",
        OPERATION => "replace",
        NUM       => scalar @{$self->rawsubsections},
    };
    my $i=0;
    for my $h (@{$self->rawsubsections}) {
        $i++;
        for my $k (keys %$h) { $fields->{"$k$i"}=$h->{$k}; }
    }
    my $response=ZLR::UA::postue("/cgi-bin/groupedit", $fields, "");
    unless($response->code == 302) {
        warn "Failed to set subsections\n";
        return undef;
    }
}
#▶2 setStartWith
# (ZLR::Info) -> _
# (self) -> _
# Установить список произведений «см. также»
sub setStartWith($) {
    my $self=shift;
    my $fields={
        DIR        => $self->url("fl"),
        OPERATION  => "Store",
        ((defined $self->rawstartwith->[0])?
            (BEGIN_TXT1 => $self->rawstartwith->[0]):
            ()),
        ((defined $self->rawstartwith->[1])?
            (BEGIN_TXT2 => $self->rawstartwith->[1]):
            ()),
    };
    my $response=ZLR::UA::postue("/cgi-bin/beginedit", $fields, "");
    my $text=$response->decoded_content;
    unless($text =~ /^\Q<TITLE>302 Found<\/TITLE>\E\s*$/om) {
        warn "Failed to set start with list";
        if($text =~ /^<font color=red><center>(.*?)<br>/om) {
            warn "Error message: $1\n"; }
    }
}
#▶2 setFriends
# (ZLR::Info) -> _
# (self) -> _
# Объявить дружеские разделы
sub setFriends($) {
    my $self=shift;
    local $_;
    my @friends=@{($self->friends)};
    my $fields={
        Store     => "Store (Сохранить)",
        NUM       => scalar @friends,
        OPERATION => "replace",
        $self->frsubscribe ? (subs_fr => "on") : (),
    };
    my $i=1;
    foreach (@friends) {
        /^(.)/o;
        $fields->{"URL$i"}="$1/$_";
        $i++;
    }
    my $response=ZLR::UA::postue("/cgi-bin/friends", $fields, "");
}
#▶2 setPasswd
# (ZLR::Info[, string]) -> _
# (self[, new password]) -> _
# Изменить пароль или скрытый адрес электронной почты
sub setPasswd($;$) {
    my $self    = shift;
    my $newpass = shift;
    $newpass=$ZLR::UA::password if(not defined $newpass);
    my $fields  = {
        OPERATION  => "replace_pwd",
        BACK       => ZLR::UA::absURL($self->url("editor")),
        DT0        => $ZLR::UA::user,
        DT1        => $ZLR::UA::password,
        DP1        => $newpass,
        DP2        => $newpass,
        "Сменить!" => "Сменить!",
        DT2        => $self->secretemail,
    };
    my $response=ZLR::UA::postue("/cgi-bin/login", $fields,
                                  $self->url("passwd"));
    if($response->is_success) {
        unless($response->decoded_content=~
                        /\s+<font size="\+2">Д<\/font>обро пожаловать\s+/) {
            warn "Failed to change password"; } }
}
#▶2 setEditInfo
# (ZLR::Info) -> _
# (self) -> _
# Редактировать свойства раздела
sub setEditInfo($) {
    my $self=shift;
    my $operation=((shift) ? "Удалить! (Delete)" : "Сохранить! (Store dir)");
    my $fields={
        OPERATION    => $operation,
        REPLACE      => "Yes",
        REQUIRED     => "AUTHOR,TITLE,DATE",
        OWNER        => $ZLR::UA::user,
        DIR          => $self->url("fl"),
        TOWN         => $self->rawtown,
        BD           => $self->rawbirthdate,
        LAST_ADD     => $self->rawlastadd,
        AUTHOR       => $self->author,
        TITLE        => $self->title,
        DATE         => $self->updated,
        EMAIL        => $self->email,
        SUBS_EMAIL   => $self->subscriptionemail,
        HOME_LINK    => $self->www,
        ADD_TYPE     => $self->rawsectionlist,
        ANNOT        => ::Convert::escape($self->annotation, 1),
        READ_LEVEL   => (keys %{$self->rawreadpermissions  ||   {2=>undef}})[0],
        READ_DEFAULT => (keys %{$self->rawbooksreadpermissions||{2=>undef}})[0],
        WRITE_LEVEL  => (keys %{$self->rawwritepermissions ||   {4=>undef}})[0],
        ((defined $self->rawstartwith)?(
            ((defined $self->rawstartwith->[0])?
                (BEGIN_TXT1 => $self->rawstartwith->[0]):
                ()),
            ((defined $self->rawstartwith->[1])?
                (BEGIN_TXT2 => $self->rawstartwith->[1]):
                ())):()),
        (($self->commentssubscribe)?
            (SUBS_ALL   => 1):
            ()),
    };
    my $response=ZLR::UA::postue("/cgi-bin/zhurnal", $fields);
    my $text=$response->decoded_content;
    unless($text =~ /^\Q<TITLE>302 Found<\/TITLE>\E\s*$/om) {
        warn "Failed to set section information";
        if($text=~/(\r?\n)<font color=red><center>(.*?)<\/center><\/font>/os) {
            my $emsg=$2;
            $emsg=~s/\s{2,}/ /s;
            warn "Error message: $emsg\n"; }
    }
}
#▶2 setTop
# (ZLR::Info) -> _
# (self) -> _
# Установить список лучших
sub setTop($) {
    my $self=shift;
    my $fields={
        OPERATION => "save",
        DIR       => $self->url("fl"),
        TYPE      => "rating1",
        Ok        => "Ok",
    };
    my @top=@{$self->top};
    my $m=scalar @top;
    my $i=1;
    while($i<=30) {
        if($i>$m) {
            $fields->{"url$i"}="";
            $fields->{"ocenka$i"}=10;
            $fields->{"comment$i"}="";
        }
        else {
            my $t=$top[$i-1];
            my $plink=$t->{"authorlink"};
            $plink="$ZLR::UA::zlr/$1/$plink/" if($plink =~ /^(.)/);
            $plink.=$t->{"publication"}.".shtml";
            $fields->{"url$i"}=$plink;
            $fields->{"ocenka$i"}=10-($t->{"mark"});
            $fields->{"comment$i"}=$t->{"annotation"};
        }
        $i++;
    }
    my $response=ZLR::UA::postue("/cgi-bin/ratedit", $fields, "");
    if($response->code != 301) {
        my $text=$response->decoded_content;
        warn "Failed to set top30\n";
        if($text=~/<center><font color=red>(.*?)<\/font><\/center>/o) {
            warn "Error message: $1\n"; }
        return undef;
    }
    return 1;
}
#▶2 setSeeAlso
# Установить список «см. также»
sub setSeeAlso {
    my $self=shift;
    return undef unless defined $self->seealso;
    my $fields={
        Store     => "Store (Сохранить)",
        DIR       => $self->url("fl")."/",
        OPERATION => "replace",
    };
    my $i=0;
    for my $hash (@{$self->seealso}) {
        $i++;
        my $key=(keys %$hash)[0];
        my $v=$hash->{$key};
        $fields->{"URL$i"}=$key;
        $fields->{"TITLE$i"}=$v->{"title"};
        $fields->{"BODY$i"}=$v->{"description"};
    }
    $fields->{"NUM"}=$i;
    my $response=ZLR::UA::postue("/cgi-bin/linkedit", $fields, "");
    if($response->code!=302) {
        warn "Failed to set SeeAlso";
        return undef;
    }
    return 1;
}
#▶2 setImage
# (ZLR::Info, Either filename FH, 1 | 2) -> _
# (self, image, 1 | 2) -> _
# Изменить изображение с указанным номером
sub setImage($$$) {
    my $self=shift;
    my $filename=shift;
    my $imgn=shift;
    my $operation=(defined $filename) ? "Сохранить! (Store)" :
                                        "Удалить! (Delete)";
    my ($image, $imginfo);
    if(defined $filename) {
        my $FH=((ref $filename)?($filename):(::File::open($filename, 1)));
        return undef unless defined $FH;
        sysread($FH, $image, 102400, 0);
        close $FH unless ref $filename;
        $imginfo=eval{Image::Info::image_info(\$image)};
        if($@ or $imginfo->{"error"}) {
            warn "Invalid image: ".($@ || $imginfo->{"error"});
            return undef;
        }
    }
    my $fields=[
        DIR       => $self->url("fl"),
        OPERATION => $operation,
        FILE_NAME => ".photo$imgn",
        (defined $filename) ? (
            FILE      => [undef, "image.".$imginfo->{"file_ext"},
                Content_Type => $imginfo->{"file_media_type"},
                Content      => $image,
            ]) : (),
    ];
    my $response=ZLR::UA::postfd("/cgi-bin/file_manager", $fields);
    my $text=$response->decoded_content;
    unless($text =~ /^\Q<TITLE>302 Found<\/TITLE>\E\s*$/om) {
        warn "Failed to upload image";
        if($text=~/<TABLE\s+BORDER=0\s+CELLPADDING=3\s+WIDTH=350>\s+
                        <TR>\s+
                            <TD\s+ALIGN=CENTER>\s+
                                <BR>\s+
                                <B>(.*?)<\/B>/osx)
        {
            warn "Error message: $1\n";
        }
    }
}
#▶2 url
# см. ZLR::Author::url
sub url {
    return (shift)->p_author->url(@_);
}
#▶1 ZLR::Author
package ZLR::Author;
#▶2 BEGIN
use strict;
use warnings;
use utf8;
use URI::URL ();
use HTML::Entities;
use IO::Uncompress::Unzip;
use fields (qw(mergeOptions setDefaults inProfile format check notify subs url));
use vars (qw(%defaultoptions %checks $notref $isint $isreg $bool @ISA
             $replaces %pcmatch %complexpcmatch %testfunctions @ndprop @pndprop
             $flagregex $modregex));
@ISA=('ZLR::ReturnProp');
@ndprop=(qw(author title www email subscriptionemail secretemail rawtown
            rawbirthdate updated annotation));
@pndprop=(qw(filename title author translator rawdate annotation canvote));
$flagregex='\{[@_]?\w+\}|\'(?:\\.|[^\'])*\'|"(?:\\.|[^"])*"|.';
$modregex='[|?\\\\!&\[\]]|(?:-\d+|\d*)(?:,\d+)?';
#▶3 Настройки по умолчанию
%defaultoptions=(
    pubdir  => "publications",                # Сохранённые произведения
    rssfile => "rss.xml",                     # Файл с RSS лентой
    rssmax  => 99,                            # Максимальное количество 
                                              # новостей
    rssopts => {                                        # Настройки RSS
        title       => "ZLRchecker.pl’s RSS feed",      ##Заголовок
        description =>
            "Here you get notification about updates",  ##Описание
        # "link" => undef,                              ##Ссылка
        itemtitle       =>                              ##Заголовок новости
            "%?f(%?F(Обновилось%)(Новое%) ".
            "произведение «%]t» ".
            "в разделе «%.»)".
            "(Произведение «%]T» ".
            "было удалено из раздела «%.»)",
        itemdescription =>                              ##Сама новость
            "Произведение: %t<br>".
            "Автор(ы): %^<br>".
            "Размер: %s<br>".
            "%?>/added|removed()(".
                "%?>=size(Изменился размер: %S → %s<br>%)(%)".
                "%??t=%T(%)(Изменилось название: %][T → %][t<br>%)".
                "%??i=%I(%)(Произведение было перемещено ".
                   "%?I(из подраздела «%][I»%%%)(%%%) в подраздел «%][i»<br>%)".
                "%??o=%O(%)(%?O(Изменился формат произведения: %O →%%%)".
                              "(Формат произведения:%%%) %o<br>%)".
                "%??g=%G(%)(Изменлись жанр(ы%%%) произведения: ".
                    "«%G» → «%g»<br>%)".
                "%??p=%P(%)(Изменилось количество иллюстраций: ".
                    "%p → %P<br>%)".
            ")".
            "%?l(Сохранено в файле %l)(Не сохранялось)<br>".
            "%?a(Аннотация: <blockquote>%a</blockquote>)()",
    },
    outformat    =>
        "%?f(%?>(%-9>%)(new      %))(removed  ) %,\n",# Что будет выводится 
                                                      # в STDOUT
    alwaysbinary => 0,                             # Всегда включать изображения 
                                                   # в виде тёга <binary>
    minescq      => 0.5,
    maxfnamelen  => 127,                           # Максимальная длина файла 
                                                   # (симв., не байт!)
    qreg         => "[\"']",                       # Что считаем кавычками
    defaultorder => "ExcludeInclude",
    pfilename    => '%=-%@-%f-%!\t',               # Шаблон имени архива
    pafilename   => '%@-%f:%t',                    # Шаблон имени файла
                                                   # в архиве
    acache       => "authors",                     # Каталог со старой 
                                                   # информацией
    shownew      => 0,
    coutformat   => "c %4# %;\n",
    crssfile     => "comments.xml",                # Файл для сохранения rss 
                                                   # ленты комментариев
    crssopts     => {
        title       =>
            "ZLRCheckers.pl’s comment RSS feed",
        description     =>
            "Here you get notifications ".
            "about new comments",
        itemtitle       =>
            "Новые комментарии у произведения «%][t» ".
            "в разделе «%.»: %#",
        itemdescription => "", # TODO
        itemlink        => "%;",
    },
    defaultname   => "ZLRChecker",
    defaulturl    => "http://zlrchecker.sourceforge.net",
    defaultemail  => "",
    converse      => "none",
    detectheaders => 1,
);
#▶3 %checks
#▶4 $notref: проверка проходит, если аргумент — не ссылка
$notref = sub {
    local $_=shift;
    return undef if(ref);
    return $_;
};
#▶4 $isint: проверка проходит, если аргумент — целое число, большее -1
$isint = sub {
    local $_=shift;
    return undef if(ref);
    $_=int($_);
    return undef unless $_ >= -1;
    return $_;
};
#▶4 $bool: применяет not(not) к аргументу
$bool = sub {
    return(not(not(shift)));
};
#▲4
%checks=(
    #▶4 minescq: любое число с плавающей точкой
    minescq => sub {
        local $_=shift;
        return undef if(ref);
        return $_ + 0.0;
    },
    #▶4 qreg: любая строка, не являющаяся регулярным выражением
    qreg => sub {
        local $_=shift;
        return undef if(ref);
        eval {"" =~ /$_/};
        return undef if($@);
        return $_;
    },
    #▶4 defaultorder: строка, ExcludeInclude или IncludeExclude
    defaultorder => sub {
        local $_=shift;
        return undef unless $_ eq "ExcludeInclude" or
                            $_ eq "IncludeExclude";
        return $_;
    },
    #▶4 defaulturl: URL
    defaulturl   => sub {
        local $_=shift;
        return undef if(ref);
        return (URI::URL->new($_)->abs()->canonical())."";
    },
    #▶4 converse
    converse     => sub {
        local $_=shift;
        return undef if(ref);
        $_=lc $_;
        return undef unless $_ =~ /^(html|fb2|none)(\+(html|fb2|none)){0,2}$/;
        return $_;
    },
    #▶4 Другие настройки с более простыми проверками
    shownew      => $bool,
    alwaysbinary => $bool,
    pubdir       => $notref,
    outformat    => $notref,
    coutformat   => $notref,
    pfilename    => $notref,
    pafilename   => $notref,
    rssfile      => $notref,
    crssfile     => $notref,
    defaultname  => $notref,
    defaultemail => $notref,
    rssmax       => $isint,
    crssmax      => $isint,
    maxfnamelen  => $isint,
    #▲4
);
#▶3 $replaces
$replaces={
    #▶4 WIN: экранирование символов, недопустимых в имени файла
    #          в системе Windows
    WIN   => [
        "%"  => "%%",
        ":"  => "%(..)",
        "*"  => "%.",
        "/"  => "%s",
        "\\" => "%b",
        "?"  => "%q",
        "|"  => "%p",
        "<"  => "%l",
        ">"  => "%g",
        "\"" => "%('')",
        "\0" => "%0",
    ],
    #▶4 TRANS: перевести символы Unicode в ASCII
    TRANS => [
        "№" => "#",
        "’" => "'",
        "∲" => "\$oc",
        "≡" => "=-",
        "ё" => "ox",
        "⨂" => "\$*)n",
        "⇒" => "=>",
        "э" => "ex",
        "∬" => "\$ii",
        " " => "_",
        "́"  => "`",
        "а" => "a",
        "б" => "b",
        "ц" => "c",
        "д" => "d",
        "е" => "e",
        "ф" => "f",
        "г" => "g",
        "х" => "h",
        "и" => "i",
        "й" => "j",
        "к" => "k",
        "л" => "l",
        "м" => "m",
        "н" => "n",
        "о" => "o",
        "п" => "p",
        "я" => "q",
        "р" => "r",
        "с" => "s",
        "т" => "t",
        "у" => "u",
        "в" => "v",
        "ш" => "w",
        "ь" => "x",
        "ы" => "y",
        "з" => "z",
        "⋃" => "\$Un",
        " " => "~",
        "‛" => "\$`!",
        "ж" => "zh",
        "↣" => ">->",
        "α" => "|a",
        "∳" => "\$occ",
        "≥" => ">=",
        "»" => ">>",
        "⇍" => "/<=",
        "‟" => "\$``",
        "⨁" => "\$+)n",
        "↢" => "<-<",
        "↔" => "<->",
        "⊄" => "/<c",
        "∉" => "/<e",
        "ъ" => "x'",
        "≢" => "/=-",
        "↤" => "<-|",
        "⋂" => "\$un",
        "©" => "(c)",
        "±" => "+-",
        "⇏" => "/=>",
        "⟨" => "|<",
        "⟩" => "|>",
        "÷" => "\$/-",
        "∕" => "\$//",
        "⁰" => "^0",
        "¹" => "^1",
        "²" => "^2",
        "³" => "^3",
        "⁴" => "^4",
        "⁵" => "^5",
        "⁶" => "^6",
        "⁷" => "^7",
        "⁸" => "^8",
        "⁹" => "^9",
        "δ" => "|d",
        "ε" => "|e",
        "φ" => "|f",
        "γ" => "|g",
        "ς" => "|h",
        "ι" => "|i",
        "τ" => "|j",
        "κ" => "|k",
        "λ" => "|l",
        "μ" => "|m",
        "η" => "|n",
        "ο" => "|o",
        "π" => "|p",
        "ρ" => "|r",
        "σ" => "|s",
        "θ" => "|t",
        "υ" => "|u",
        "ν" => "|v",
        "ω" => "|w",
        "ξ" => "|x",
        "∰" => "\$ooo",
        "ζ" => "|z",
        "∠" => "\$/_",
        "∄" => "\$/e",
        "„" => ",,",
        " " => "_.",
        "₀" => "_0",
        "₁" => "_1",
        "₲" => "_2",
        "₳" => "_3",
        "₄" => "_4",
        "⊅" => "/>c",
        "₆" => "_6",
        "₇" => "_7",
        "₈" => "_8",
        "₉" => "_9",
        "ш" => "sh",
        " " => "_d",
        " " => "_m",
        " " => "_o",
        "∓" => "-+",
        "–" => "--",
        " " => "_t",
        "→" => "->",
        "∁" => "\$co",
        "≤" => "<=",
        "₅" => "_5",
        "∌" => "/>e",
        "“" => "``",
        "℗" => "(p)",
        "¬" => "\$!",
        "″" => "\$\"",
        "‰" => "\$%",
        "‘" => "\$'",
        "∨" => "\$*",
        "∧" => "\$+",
        "‚" => "\$,",
        "∖" => "\$-",
        "∙" => "\$.",
        "⁄" => "\$/",
        "°" => "\$0",
        "≪" => "\$<<",
        "⇐" => "\$<=",
        "‹" => "\$<",
        "›" => "\$>",
        "Å" => "\$A",
        "℃" => "\$C",
        "ℰ" => "\$E",
        "℉" => "\$F",
        "∞" => "\$I",
        "K" => "\$K",
        "ℕ" => "\$N",
        "Ω" => "\$O",
        "℔" => "\$P",
        "ℚ" => "\$Q",
        "ℝ" => "\$R",
        "∑" => "\$S",
        "∪" => "\$U",
        "ℤ" => "\$Z",
        "√" => "\$\\",
        "⊂" => "\$<c",
        "ʼ" => "\$`",
        "∀" => "\$a",
        "ℂ" => "\$c",
        "∂" => "\$d",
        "ℯ" => "\$e",
        "ℎ" => "\$h",
        "∫" => "\$i",
        "′" => "\$m",
        "∅" => "\$n",
        "∮" => "\$o",
        "∏" => "\$p",
        "″" => "\$s",
        "∩" => "\$u",
        "×" => "\$x",
        "≈" => "\$~",
        "∯" => "\$oo",
        "≠" => "/=",
        "∃" => "\$ex",
        "∝" => "\$=c",
        "∛" => "\$\\3",
        "∜" => "\$\\4",
        "®" => "(r)",
        "—" => "---",
        "щ" => "w'",
        "↦" => "|->",
        "⇔" => "<=>",
        "⊽" => "\$*!",
        "≫" => "\$>>",
        "⊗" => "\$*)",
        "…" => "...",
        "щ" => "wh",
        "⊃" => "\$>c",
        "∋" => "\$>e",
        "ψ" => "|y",
        "ч" => "ch",
        "℠" => "^sm",
        "∈" => "\$<e",
        "⋁" => "\$*n",
        "ч" => "cx",
        " " => "_q",
        "”" => "''",
        "⊻" => "\$*x",
        "⊼" => "\$+!",
        "⊕" => "\$+)",
        "ℏ" => "\$h-",
        "∈" => "\$<e",
        "ю" => "ux",
        "⊥" => "\$|_",
        "ъ" => "xx",
        "β" => "|b",
        "←" => "<-",
        "χ" => "|c",
        "™" => "^tm",
        "«" => "<<",
        "⋀" => "\$+n",
        "∭" => "\$iii",
        "∥" => "\$||",
        "⇎" => "/<=>",
    ],
    #▲4
};
#▶3 %pcmatch
%pcmatch = (
    s => "size",
    a => "annotation",
    t => "title",
    i => "section",
    o => "format",
    g => "genrelist",
    u => "update",
    f => "filename",
    l => "lastsave",
    c => "comments",
    k => "lastcomment",
    p => "imgcount",
);
#▶3 %complexpcmatch
%complexpcmatch = (
    #▶4 %>, %v, %V, %=, %@, %<, %+, %.
    ">" => sub {return $_[2]}, # Что изменилось
     V  => sub {return $_[3]}, # Старое значение того, что изменилось
     v  => sub {return $_[4]}, # Новое значение того, что изменилось
    "=" => sub {return(time)}, # Текущее время
    "@" => sub {return (shift)->name;         }, # Часть URL с автором
    "+" => sub {return (shift)->info->updated;}, # Дата обновления
    "." => sub {return (shift)->info->author; }, # Имя автора
    "*" => sub {return (shift)->info->www;    }, # Адрес домашней страницы
    # Полный URL раздела
    "<" => sub {return ZLR::UA::absURL((shift)->info->srcurl); },
    #▶4 %,: полный URL произведения
    "," => sub {
        my $self=shift;
        my $publication=shift;
        return undef unless defined $publication;
        my $pubinfo=$self->info->publications->{$publication};
           $pubinfo=$self->savedinfo->publications->{$publication}
                unless defined $pubinfo;
        return undef unless defined $pubinfo;
        return ZLR::UA::absURL($pubinfo->url);
    },
    #▶4 %;: полный URL страницы комментариев
    ";" => sub {
        my $self        = shift;
        my $publication = shift;
        return undef unless defined $publication;
        my $pubinfo=$self->info->publications->{$publication};
           $pubinfo=$self->savedinfo->publications->{$publication}
                unless defined $pubinfo;
        return undef unless defined $pubinfo;
        return ZLR::UA::absURL($pubinfo->url("comments"));
    },
    #▶4 %^: имя автора произведения или имя автора раздела
    "^" => sub {
        my $self        = shift;
        my $publication = shift;
        my $info        = $self->info;
        my $pubinfo     = $info->publications->{$publication}
                    if(defined $publication);
        my $auname;
        $auname=$pubinfo->author if(defined $pubinfo);
        $auname=$info->author unless defined $auname;
        return $auname;
    },
    #▶4 %#: количество новых комментариев
    "#" => sub {
        my $self        = shift;
        my $publication = shift;
        return undef unless defined $publication;
        my $pubinfo     = $self->info->publications->{$publication};
        my $savedinfo   = $self->savedinfo;
        my $savedpubinfo= $savedinfo->publications->{$publication}
            if(defined $savedinfo);
        return (
            ((defined $pubinfo)?
                ($pubinfo->comments || 0) : 0) -
            ((defined $savedpubinfo)?
                ($savedpubinfo->comments || 0) : 0));
    },
    #▲4
);
#▶3 %testfunctions
# (ZLR::Author, a, string) -> Bool
# (self, test, publication filename) -> Bool
%testfunctions=(
    #▶4 size
    size => sub {
        #▶5 Объявление переменных
        my $self        = shift;
        my $test        = shift;
        my $publication = shift;
        my $pubinfo     = $self->info->publications->{$publication};
        my $size        = ((defined $pubinfo)?($pubinfo->size):(0));
        #▶5 Некоторые проверки
        return 0 unless defined $size;
        return 0 if(ref $test);
        return 0 unless $test=~/^([=-])/;
        #▶5 Вычитание старого размера
        if($1 eq "-") {
            my $savedpubinfo=$self->savedinfo->publications->{$publication};
            my $savedsize;
            $savedsize=$savedpubinfo->size if(defined $savedpubinfo);
            $size-=$savedsize if defined $savedsize;
        }
        #▶5 Проверка размера
        my @tests=($test=~/[><]=?\d+/g);
        for $_ (@tests) {
            return 0 unless eval "$size$_"; }
        return 1;
        #▲5
    },
    #▶4 section, name, title
    section => sub {
        my $self=shift;
        my $test=shift;
        return TestExactOrReg($self, $test, "section", shift);
    },
    name => sub {
        my $self=shift;
        my $test=shift;
        return TestExactOrReg($self, $test, "filename", shift);
    },
    title => sub {
        my $self=shift;
        my $test=shift;
        return TestExactOrReg($self, $test, "title", shift);
    },
    #▶4 match
    match => sub {
        my $self        = shift;
        my $test        = shift;
        my $publication = shift;
        my $author      = $self->p_author;
        return 0 unless ref $test eq "ARRAY";
        my $t=$self->subs($test->[0], $publication, @_);
        my $i=1;
        while($i < scalar @$test) {
            my $r=$test->[$i];
            my $res=0;
            eval { $res=($t=~/$r/); };
            warn "Failed test “$r” for “match=/".$test->[0]."/” ".
                                                            "with message “$@”"
                if($@);
            return 0 unless $res;
            $i++;
        }
        return 1;
    },
    #▲4
);
#▶2 new
# (invocant, string, Bool, %) -> ZLR::Author
# (invocant, author name, prefercache, %) -> ZLR::Author
# Вместо % в настоящий момент можно использовать только "options" => hash
sub new {
    #▶3 Объявление переменных
    my $invocant    = shift;
    my $class       = ref $invocant || $invocant;
    my $aname       = shift;
    my $prefercache = shift;
    my %args        = @_;
    my $minimal     = 0;
    my $self        = bless {}, $class;
    my ($aopts);
    local $_;
    foreach (keys %args) {
        if($_ eq "options") {
            $aopts=$args{$_}; } }
    if(not defined $aname) {
        $minimal=1;
        goto OPTS;
    }
    return undef if(ref $aname);
    $aname=lc $aname;
    return undef unless $aname=~/^[a-z0-9_]+$/o;
    #▶3 acache
    my $acache=$aopts->{"acache"} || $defaultoptions{"acache"};
    mkdir $acache or die "Unable to create directory $acache"
                  unless -d $acache;
    -w $acache or die "Unable to write to directory $acache";
    my $afname="$acache/$aname";
    $self->file($afname);
    $self->name($aname);
    #▶3 Текущие данные
    $self->info(ZLR::Info->new(author => $self, ($prefercache ? 2 : 0)));
    return undef unless defined $self->info;
    #▶3 Старые данные
    $self->savedinfo(ZLR::Info->new(author => $self, 1));
    #▶3 Настройки
  OPTS:
    my $defaults=\%defaultoptions;
    $self->{"options"}=mergeOptions($defaults, $aopts);
    return $self if($minimal);
    #▶3 rss, format, check
    $self->{"rss"}    = (main::RSS->new($self));
    $self->{"crss"}   = (main::RSS->new($self, $self->o_crssopts,
                                               $self->o_crssmax,
                                               $self->o_crssfile));
    #▲3
    return $self;
}
#▶2 mergeOptions
# (hash, hash) -> hash
sub mergeOptions($$) {
    my $oldoptions=shift;
    my $newoptions=shift;
    return $newoptions unless defined $oldoptions;
    return $oldoptions unless defined $newoptions and
                              ref $newoptions eq "HASH";
    my $mergedopts={};
    my @keys=keys %$oldoptions;
    push @keys, keys %$newoptions;
    for my $key (@keys) {
        if(defined $newoptions->{$key}) {
            if($key eq "rssopts" or $key eq "crssopts" or $key eq "profiles") {
                $mergedopts->{$key}=ZLR::Stuff::mergeHashes(
                                      $oldoptions->{$key}, $newoptions->{$key});
            }
            elsif(defined $checks{$key}) {
                my $new=$checks{$key}->($newoptions->{$key});
                my $old=$oldoptions->{$key};
                if(defined $new) {
                    $mergedopts->{$key}=$new; }
                elsif(defined $old) {
                    $mergedopts->{$key}=$old; }
            }
            else {
                $mergedopts->{$key}=$newoptions->{$key}; } }
        else {
            $mergedopts->{$key}=$oldoptions->{$key}; } }
    return $mergedopts;
}
#▶2 setDefaults
# hash -> _
# Устанавливает настройки по умолчанию
sub setDefaults($) {
    my $oldoptions=\%defaultoptions;
    my $newoptions=shift;
    %defaultoptions=%{mergeOptions($oldoptions, $newoptions)};
    return undef;
}
#▶2 TestExactOrReg
# (ZLR::Author, string, string, string) -> Bool
# (self, test (=string or /regex), key, publication filename) -> Bool
# Проверить, соответствует ли значение, соответствующее ключу «key», другому 
# значению (string) или же регулярному выражению (regex).
sub TestExactOrReg($$$$) {
    my $self        = shift;
    my $test        = shift;
    my $key         = shift;
    my $publication = shift;
    return 0 if(ref $test);
    my $pubinfo      = $self->info->publications->{$publication};
    my $savedpubinfo = $self->savedinfo->publications->{$publication};
    my $t            = defined $pubinfo ? $pubinfo->$key : $savedpubinfo->$key;
    $t="" unless defined $t;
    return 0 unless $test=~/^([\/=])(.*)$/;
    my $ttype=$1;
        $test=$2;
    my $res  =0;
    if($ttype eq "=") {
        return($t eq $test);}
    else {
        my $res=0;
        eval { $res = $t=~/$test/; };
        if($@) {
            warn "Failed test “$test” for “$key” with message “$@”"; }
        return $res;
    }
    return 0;
}
#▶2 MatchesTests
# (ZLR::Author, hash, @) -> Bool
# (self, profile, @) -> Bool
# @ — см. %testfunctions (без первых двух аргументов)
sub MatchesTests($$;@) {
    my $self        = shift;
    my $profile     = shift;
    for my $tsttype (keys %$profile) {
        my $tests=$profile->{$tsttype};
        if(not ref $tests) {
            $tests=[$tests]; }
        elsif(ref $tests eq "HASH" and $tsttype eq "match") {
            my $newtests=[];
            for my $s (keys %$tests) {
                my $r=$tests->{$s};;
                push @$newtests, [$s, ((ref $r eq "ARRAY")?(@$r):($r))];
            }
            $tests=$newtests;
        }
        elsif(ref $tests ne "ARRAY") {
            warn "Not an array reference: $tsttype";
            print STDERR ::YAML::dump($tests);
            return 0;
        }
        my $tfunc=$testfunctions{$tsttype};
        unless(defined $tfunc) {
            warn "Unknown test: $tsttype";
            return 0;
        }
        for my $test (@$tests) {
            return 0 unless(&$tfunc($self, $test, @_));}
    }
    return 1;
}
#▶2 inProfile: Проверка на соответствие профилю
# (ZLR::Author, hash, @) -> Bool
# (self, profile, @) -> Bool
# @ — см. MatchesTests (без первых двух аргументов)
sub inProfile($$;@) {
    my $self        = shift;
    my $prof        = shift;
    my $profile;
    if(ref $prof eq "HASH") {
        $profile=$prof; }
    elsif(not ref $prof) {
        return 1 if($prof eq "all");
        return 0 if($prof eq "none");
        $profile=$self->o_profiles->{$prof};
    }
    else {
        warn "Expected profilename or a HASH, instead got ".(ref $prof);
        print STDERR ::YAML::dump($prof);
    }
    return 0 unless defined $profile;
    return $self->MatchesTests($profile, @_);
}
#▶2 check
# (ZLR::Author, string, string, string) -> Bool
# (self, check option, publication filename, whatchanged) -> Bool
# Проверить, соответствует ли произведение некоей настройке.
sub check($$$$) {
    my $self        = shift;
    my $where       = shift;
    my $publication = shift;
    my $whatchanged = shift;
    my $def         = ($where eq "check" or $where eq "download");
       $where       = "o_$where";
       $where       = $self->$where;
    return $def unless defined $where;
    my $order       = $where->{"order"};
    $order=$self->o_defaultorder if not defined $order or
                                    (($order ne "ExcludeInclude") and
                                     ($order ne "IncludeExclude"));
    my %has=(
        exclude => defined $where->{"exclude"},
        include => defined $where->{"include"},
    );
    my %excinc=(
        exclude => 0,
        include => 0,
    );
    my $default=$where->{"default"};
    $excinc{$default}=1 if(defined $default and
                           ($default eq "exclude" or
                            $default eq "include"));
    EI: for my $key (keys %has) {
        next EI unless $has{$key};
        for my $prof (@{$where->{$key}}) {
            if($self->inProfile($prof, $publication, $whatchanged)) {
                $excinc{$key}=1;
                next EI;
            }
        }
        $excinc{$key}=0;
    }
    if($order eq "IncludeExclude") {
        return($excinc{"include"} and (not $excinc{"exclude"})); }
    else {
        return($excinc{"include"} or  (not $excinc{"exclude"})); }
}
#▶2 ReplaceAll
# (string, array) -> string
# Прогнать строку через серию замен одних фиксированных подстрок на другие.
sub ReplaceAll($$) {
    my $r=shift;
    my @replaces=@$r;
    my $str=shift;
    my $i=0;
    my $len=(scalar @replaces)/2;
    while($i<$len) {
        my ($lhs, $rhs)=@replaces[$i*2, ($i*2)+1];
        $str=~s/\Q$lhs\E/$rhs/g;
        $i++;
    }
    return $str;
}
#▶2 ModSubstr
# (string, string) -> string
# (modifier string, string being modified) -> string
# Изменить строку в соответствии с данными модификаторами
sub ModSubstr($$) {
    my $mod=shift;
    my $str=shift;
    $str=""     unless defined $str;
    return $str unless defined $mod and $mod ne "";
    for my $m (($mod=~/$modregex/g)) {
        if   ($m eq "!") {
            $str=ReplaceAll($replaces->{"TRANS"}, lc $str); }
        elsif($m eq "\\") {
            $str=ReplaceAll($replaces->{"WIN"}, $str); }
        elsif($m eq "|") {
            $str=~s/([\\\[\]*?+\^\${}().\|])/\\$1/g; }
        elsif($m eq "&") {
            $str=URI::Escape::uri_escape_utf8($str); }
        elsif($m eq "[") {
            $str=HTML::Entities::encode_entities($str); }
        elsif($m eq "]") {
            $str=HTML::Entities::decode_entities($str); }
        elsif($m =~ /(-?\d+)?(,(\d+))?/) {
            if(defined $3) {
                $str=~s/^(.{$3}).*$/$1/; }
            $str=sprintf("%$1s", $str) if(defined $1);
        }
    }
    return $str;
}
#▶2 GetSubstr
# (ZLR::Author, string, string, string) -> string
# (self, format flag, publication filename, whatchanged) -> string
# Получить подстроку, соответствующую указанному «format flag».
sub GetSubstr($$$$) {
    #▶3 Объявление переменных
    my $self        = shift;
    my $k           = shift;
    my $publication = shift;
    my $whatchanged = shift;
    my $info        = $self->info;
    my $savedinfo   = $self->savedinfo;
    my $pubinfo     = $info->publications->{$publication}
        if(defined $publication);
    my $savedpubinfo= $savedinfo->publications->{$publication}
        if(defined $savedinfo and defined $publication);
    my ($oldvalue, $newvalue);
    #▶4 $whatchanged: added, removed, nothing
    unless(defined $whatchanged) {
        $whatchanged="added"   unless defined $savedpubinfo;
        $whatchanged="removed" unless defined $pubinfo;
        $whatchanged="nothing" unless defined $whatchanged;
    }
    #▶4 $newvalue and $oldvalue
    unless($whatchanged eq "added" or
           $whatchanged eq "removed" or
           $whatchanged eq "nothing")
    {
        $newvalue=$pubinfo->$whatchanged
            if(defined $pubinfo);
        $oldvalue=$savedpubinfo->$whatchanged
            if(defined $savedpubinfo);
    }
    #▶4 $author
    my $author;
    if(defined $info) {
        $author=$info->p_author; }
    elsif(defined $savedinfo) {
        $author=$savedinfo->p_author; }
    #▶3 %{publicationproperty}, %{@authorproperty}, %{_option}
    if($k=~s/^\{([_@])?(.*)\}$/$2/) {
        my $type=$1;
        if(not defined $type) {
            return ((defined $pubinfo)?
                        ($pubinfo->$k):
                        ((defined $savedpubinfo)?
                            ($savedpubinfo->$k):
                            (undef)));
        }
        return $author->info->$k if($type eq "@");
        $k="o_$k";
        return $author->$k;
    }
    #▶3 %'text'
    elsif($k=~s/^'(.*)'$/$1/o) {
        $k=~s/\\([\\'])/$1/go;
        return $k;
    }
    #▶3 %"text"
    elsif($k=~s/^"(.*)"$/$1/o) {
        $k=~s/\\(.)/$1/go;
        $k=$self->subs($k, $publication, $whatchanged);
        return $k;
    }
    #▲3
    my @args=($self, $publication, $whatchanged, $oldvalue, $newvalue);
    #▶3 Экранируемые символы
    return "$k"  if($k eq "%" or $k eq "(" or $k eq ")");
    #▶3 %complexpcmatch
    if(defined $complexpcmatch{$k}) {
        return $complexpcmatch{$k}->(@args); }
    #▶3 %pcmatch
    my $lck=lc $k;
    my $key=$pcmatch{$lck};
    return "%$k" unless defined $key;
    if($lck eq $k) {
        return undef unless defined $pubinfo;
        return $pubinfo->$key;
    }
    else {
        return undef unless defined $savedpubinfo;
        return $savedpubinfo->$key;
    }
    #▲3
}
#▶2 ReplaceConditionals
# (ZLR::Author, string, string, string, string, @) -> string
# (self, condition, value if true, value if false, string, @) -> string
# Заменить все условные выражения на их значения
# @ — см. GetSubstr (без первых двух аргументов)
sub ReplaceConditionals($$$$$;@) {
    #▶3 Объявление переменных
    my $self        = shift;
    my $condition   = shift;
    my $iftrue      = shift;
    my $iffalse     = shift;
    my $str         = shift;
    #▲3
    goto RECURSE unless defined $iftrue;
    #▶3 Проверка условий
    $condition=~s/%([%\(])/$1/g if(defined $condition);
    my $tf=0;
    if(not defined $condition) {
        $tf = (defined $str); }
    else {
        $str="" unless defined $str;
        if($condition=~/^=(.*)$/) {
            $tf = ($str eq $1); }
        elsif($condition=~/^\/(.*)$/) {
            eval {$tf = ($str=~/$1/)};
            warn "Invalid condition “/$1”: $@" if($@);
        }
    }
    #▶3 Замещение
    if($tf) {
        $str=$iftrue; }
    else {
        $str=$iffalse; }
    #▶3 Проверка вложенных условий
    $str=~s/%([%\)])/$1/g;
  RECURSE:
    $str=~s/
            %\?(\?)?($flagregex) # Что проверяем
            ([=\/]([^\(%]|%.)*)? # Условие (defined если нет)
            \((([^%]|%.)*?)\)    # Значение если истина
            \((([^%]|%.)*?)\)    # Значение если ложь
        /$self->ReplaceConditionals(((defined $1) ? $self->subs($3, @_) : $3),
                                    $5, $7, $self->GetSubstr($2, @_), @_)/gex;
    return $str;
}
#▶2 subs
# (ZLR::Author, string, @) -> string
# @ — см. GetSubstr (без первых двух аргументов)
sub subs($$;@) {
    my $self        = shift;
    my $str         = shift;
    $str=Encode::decode_utf8($str) unless Encode::is_utf8($str);
    $str=$self->ReplaceConditionals(undef, undef, undef, $str, @_);
    $str=~s/%(($modregex)+)?($flagregex)/
            ModSubstr($1, $self->GetSubstr($+, @_))/gex;
    return $str;
}
#▶2 notify
# (ZLR::Author, string, string) -> _
# (self, publication filename, whatchanged) -> _
# Извещает об обновлениях и запускает скачивание и конвертирование 
# произведения
sub notify($$$) {
    my $self         = shift;
    my $publication  = shift;
    my $whatchanged  = shift;
    return undef unless($self->i_isold or $self->o_shownew);
    my $pubinfo      = $self->info->publications->{$publication};
    my $savedpubinfo = $self->savedinfo->publications->{$publication};
    my ($oldvalue, $newvalue);
    if(defined $whatchanged) {
        $oldvalue     = ((defined $savedpubinfo)?
                             ($savedpubinfo->$whatchanged):
                             (undef));
        $newvalue     = ((defined $pubinfo)? ($pubinfo->$whatchanged): (undef));
    }
    else {
        if   (not defined $savedpubinfo){ $whatchanged="added";   }
        elsif(not defined $pubinfo)     { $whatchanged="removed"; }
    }
    my $c=($whatchanged ne "comments");
    return undef unless $self->check((($c)?("check"):("comments")),
                                     $publication, $whatchanged);
    (defined $pubinfo ? $pubinfo : $savedpubinfo)->savePublication($whatchanged)
        if $whatchanged ne "comments" and $self->check("download",
                                                       $publication,
                                                       $whatchanged);
    my $rss=$self->{(($c)?"rss":"crss")};
    $rss->add($self, $publication, $whatchanged) if(defined $rss);
    print $self->subs((($c)?($self->o_outformat):($self->o_coutformat)),
                      $publication, $whatchanged);
    return $pubinfo;
}
#▶2 backup
# (ZLR::Author, string) -> _
# (self, filename) -> _
sub backup($$) {
    #▶3 Объявление переменных
    my $self=shift;
    my $dest=shift;
    my $arch=main::Archive->new($dest);
    my $info=$self->info;
    my $i=0;
    local $_;
    return undef unless defined $arch;
    #▶3 Получение информации
    $info->getStartWithList();
    $info->getSecretEmail();
    $info->getEditInfo();
    $info->getTop();
    $info->getSeeAlso();
    $info->getSubSections();
    $info->getBibliography();
    $info->getSubscriptions();
    $info->getFriends();
    $info->getImages();
    $arch->add("INFO");
    #▶3 Сохранение информации
    #▶4 Разделы
    for my $s (@{$info->rawsubsections}) {
        print $arch $s->{"TITL"}."\0".$s->{"ANNOT"}."\0"; }
    print $arch "\0";
    #▶4 editInfo
    print $arch $info->$_."\0" foreach (@ndprop);
    print $arch ((keys %{$info->rawreadpermissions  ||   {2=>undef}})[0].
                 (keys %{$info->rawbooksreadpermissions||{2=>undef}})[0].
                 (keys %{$info->rawwritepermissions ||   {4=>undef}})[0]);
    #▶4 дружеские разделы
    print $arch ($info->frsubscribe ? 1 : 0);
    if(defined $info->friends) {
        print $arch "$_\0" foreach (@{$info->friends}); }
    print $arch "\0";
    #▶4 список лучших
    if(defined $info->top) {
        foreach (@{$info->top}) {
            my $mark=$_->{"mark"};
            $mark="+" if($mark == 10);
            print $arch "$mark";
            for my $k (qw(authorlink publication annotation)) {
                my $s=$_->{$k};
                   $s="" unless defined $s;
                print $arch "$s\0";
            }
        }
    }
    print $arch "\0";
    #▶4 См. также
    if(defined $info->seealso) {
        foreach (@{$info->seealso}) {
            my $key=(keys %$_)[0];
            my $v=$_->{$key};
            my $title=((defined $v and defined $v->{"title"})?
                ($v->{"title"}):(""));
            my $description=((defined $v and defined $v->{"description"})?
                ($v->{"description"}):(""));
            print $arch "$key\0$title\0$description\0";
        }
    }
    print $arch "\0".$info->secretemail."\0";
    #▶4 Изображения
    if(defined $info->sectionimages) {
        my $n=0;
        for my $imglink (@{$info->sectionimages}) {
            $n++;
            my $image=ZLR::UA::get($imglink);
            next unless defined $image;
            $imglink=~s/^.*\.//o;
            $arch->add("\@$n.$imglink", $image);
        }
    }
    #▶4 Публикации
    for my $publication (keys %{$info->publications}) {
        $i++;
        my $pubinfo=$info->publications->{$publication};
        $arch->add(">$i");
        $pubinfo->getEditInfo();
        print $arch ((defined $pubinfo->$_)?
                        ($pubinfo->$_):(""))."\0"
                    foreach (@pndprop);
        print $arch (
            (keys   %{$pubinfo->rawreadpermissions  || {2=>""}})[0].
            (keys   %{$pubinfo->rawwritepermissions || {4=>""}})[0].
            (keys   %{$pubinfo->rawcpermissions     || {0=>""}})[0]."\0".
            (keys   %{$pubinfo->rawformat           || {0=>""}})[0]."\0"
        );
        if(defined $pubinfo->rawsection) {
            print $arch ((values %{$pubinfo->rawsection})[0]); }
        elsif(defined $pubinfo->section) {
            print $arch $pubinfo->section; }
        $arch->add("=$i", $pubinfo->rawsave());
        my $n=0;
        if($pubinfo->imgcount) {
            $pubinfo->getIllustrations();
            for my $iref (keys %{$pubinfo->illustrations || {}},
                          keys %{$pubinfo->additions     || {} }) {
                $n++;
                my $image=ZLR::UA::get($iref);
                next unless defined $image;
                my $v=$pubinfo->illustrations->{$iref}
                    if(defined $pubinfo->illustrations);
                   $v=$pubinfo->additions->{$iref} unless defined $v;
                $iref=~s/^.*\///;
                $arch->add("\@>$i.$n");
                print $arch $v->{"description"}."\0".$v->{"author"}."\0$iref";
                $arch->add("\@$i.$n", $image);
            }
        }
        my $comments=ZLR::Comments->new($pubinfo);
        $comments->getAllComments();
        my @cmts=();
        if($comments->archives) {
            for my $a (reverse @{$comments->archives}) {
                push @cmts, reverse @{$a->{"comments"}};
            }
        }
        push @cmts, reverse @{$comments->comments};
        @cmts=grep {not($_->{"deleted"})} @cmts;
        @cmts=map {join "\0", ($_->{"author"}, $_->{"alink"}, $_->{"email"},
                               join "\n", @{$_->{"text"}})}
                  @cmts;
        $arch->add("+$i", join "\0", @cmts) if(scalar @cmts);
    }
    #▶4 «Начните знакомство с»
    #▲3
}
#▶2 restore
# (ZLR::Author, string) -> _
# (self, filename) -> _
sub restore($$) {
    #▶3 Объявление переменных
    my $self=shift;
    my $from=shift;
    my $arch=IO::Uncompress::Unzip->new($from);
    my $info=$self->info;
    local $_;
    #▶3 Первичные проверки
    return undef unless defined $arch;
    $arch->nextStream();
    return undef unless $arch->getHeaderInfo->{"Name"} eq "INFO";
    my $infotext=join "", <$arch>;
    #▶3 Разделы
    $info->rawsubsections([]);
    while($infotext=~s/^([^\0]+)\0([^\0]*)\0//o) {
        push @{$info->rawsubsections}, {
            TITL  => $1,
            ANNOT => $2,
        };
    }
    $info->setSubSections();
    $info->getSubSections();
    $info->getPossibleSections();
    #▶3 editInfo
    $info->getEditInfo();
    return undef unless $infotext =~ s/^\0//o;
    foreach (@ndprop) {
        return undef unless $infotext=~s/^([^\0]*)\0//o;
        $info->$_(Encode::decode_utf8("$1"));
    }
    foreach (qw(rawreadpermissions rawbooksreadpermissions rawwritepermissions))
    {
        return undef unless $infotext=~s/^(\d)//o;
        $info->$_({$1 => undef});
    }
    $info->setEditInfo();
    #▶3 дружеские разделы
    $info->frsubscribe("$1") if($infotext=~s/^([01])//o);
    $info->friends([]);
    push(@{$info->friends}, $1) while($infotext=~s/^([^\0]+)\0//o);
    return undef unless $infotext=~s/^\0//o;
    $info->setFriends();
    #▶3 top30
    $info->top([]);
    while($infotext=~s/^([0-9+])([^\0]*)\0([^\0]*)\0([^\0]*)\0//o) {
        push @{$info->top}, {
            mark        => (($1 eq "+")?(10):($1+0)),
            authorlink  => $2,
            publication => $3,
            annotation  => $4,
        };
    }
    return undef unless $infotext=~s/^\0//o;
    $info->setTop();
    #▶3 seealso
    $info->seealso([]);
    while($infotext=~s/^([^\0]+)\0([^\0]*)\0([^\0]*)\0//o) {
        push @{$info->seealso}, {
            $1 => {
                title       => $2,
                description => $3,
            },
        };
    }
    return undef unless $infotext=~s/^\0//o;
    $info->setSeeAlso();
    #▶3 secret email
    return undef unless $infotext=~s/^([^\0]*)\0$//o;
    $info->secretemail("$1");
    $info->setPasswd();
    #▶3 Произведения
    my $pubinfo;
    my $prevname="";
  STREAM:
    $arch->nextStream();
    my $name=$arch->getHeaderInfo->{"Name"};
    return 1 if($name eq $prevname);
    print STDERR "Restoring from $name\n"
        if($::o_verbose>1);
    $prevname=$name;
    if($name=~/^\@([12])\.(\w{3,4})$/) {
        $info->setImage($arch, $1);
        goto STREAM;
    }
    elsif($name=~/^>\d+$/o) {
        $pubinfo=ZLR::PubInfo->new($self, "new" => "0");
        my $infotext=join "", <$arch>;
        $arch->nextStream();
        foreach (@pndprop) {
            next unless $infotext=~s/^([^\0]*)\0//o;
            $pubinfo->$_("$1");
        }
        if(defined $info->publications->{$pubinfo->filename}) {
            $info->publications->{$pubinfo->filename}->setEditInfo("Delete"); }
        foreach (qw(rawreadpermissions rawwritepermissions)) {
            return undef unless $infotext=~s/^(\d)//o;
            $pubinfo->$_({$1 => undef});
        }
        foreach (qw(rawcpermissions rawformat)) {
            return undef unless $infotext=~s/^(\d+)\0//o;
            $pubinfo->$_({$1 => undef});
        }
        if($infotext=~s/^(.*)$//) {
            my $s=$1;
            $s=Encode::decode_utf8($s) unless Encode::is_utf8($s);
            $pubinfo->rawsection(ZLR::Stuff::getSimilar("sections", $s,
                                                        $info->p_author));
        }
        else {
            return undef; }
        $pubinfo->setEditInfo(undef, $arch, 1);
        $pubinfo->getEditInfo();
        goto STREAM;
    }
    elsif($name=~/^@>\d+\.\d+/o) {
        my $infotext=join "", <$arch>;
        $arch->nextStream();
        return undef unless $infotext=~s/^(.*)\0(.*)\0(.*)$//o;
        $pubinfo->addIllustration($arch, $1, $2, 1, $3);
        $pubinfo->getIllustrations();
        goto STREAM;
    }
    elsif($name=~/^\+\d+/o) {
        my $infotext=join "", <$arch>;
        my $n=2;
        my $c=0;
        my $comments=ZLR::Comments->new($pubinfo);
        while($infotext=~s/^([^\0]*)\0([^\0]*)\0([^\0]*)\0([^\0]*)(\0|$)//o) {
            if($n==2) {
                ZLR::UA::nullCookies();
                $n=0;
            }
            $n++;
            $c++;
            $comments->o_name("$1");
            $comments->o_url("$2");
            $comments->o_email("$3");
            $comments->postComment("Restored comment ($c):\n$4");
        }
        ZLR::UA::restoreCookies();
        $comments->o_name("ZLRChecker.pl restore");
        $comments->o_url("");
        $comments->o_email("");
        $comments->postComment("Restored $c comment".(($c>1)?("s"):("")).".");
        goto STREAM;
    }
    return undef;
}
#▶2 url
# (ZLR::Author[, string]) -> URL
# Получить URL какой-либо страницы, связанной с автором:
# string           Описание
# ~                URL главной страницы автора
# newbook          URL страницы создания нового произведения
# editprop         URL страницы изменения свойств раздела
# imageedit        URL страницы изменения изображений, связанных с разделом
# subsectionsedit  URL страницы изменения списка подразделов
# subscriptionedit URL страницы изменения подписок
# fl               a/author
# editor           URL главной страницы автора для самого автора
# friends          URL страницы со списком дружеских разделов
# friendssetpage   URL страницы изменения списка друзей
# startwithedit    URL страницы изменения списка «начните знакомство с»
# passwd           URL страницы изменения пароля и «секретного» email
# bibliography     URL страницы с библиографией
# bibl             URL страницы изменения библиографии
# top              URL страницы со списком лучших
# seealso          URL страницы со списком «см. также»
# dateindex        URL страницы со списком произведений, отсортированных по
#                  дате
sub url($;$) {
    my $self=shift;
    my $what=shift;
    my $name=$self->name;
    my $fl=$1 if($name =~ /^(.)/);
    my $url="$fl/$name";
    return "/$url" unless defined $what;
    return "/cgi-bin/zhurnal?OPERATION=new_book&DIR=$url"
                                               if($what eq "newbook");
    return "/cgi-bin/zhurnal?OPERATION=edit_dir&DIR=$url"
                                               if($what eq "editprop");
    return "/cgi-bin/file_manager?DIR=$url"    if($what eq "imageedit");
    return "/cgi-bin/groupedit?DIR=$url"       if($what eq "subsectionsedit");
    return "/cgi-bin/subsedit?DIR=$url"        if($what eq "subscriptionedit");
    return   $url                              if($what eq "fl");
    return "/editors/$url"                     if($what eq "editor");
    return "/cgi-bin/frlist?DIR=$url"          if($what eq "friends");
    return "/cgi-bin/friends?DIR=$url"         if($what eq "friendssetpage");
    return "/cgi-bin/beginedit?DIR=$url"       if($what eq "startwithedit");
    return "/cgi-bin/login?OPERATION=edit_pwd" if($what eq "passwd");
    return "/$url/publish.shtml"               if($what eq "bibliography");
    return "/cgi-bin/publish?DIR=$url"         if($what eq "bibledit");
    return "/$url/rating1.shtml"               if($what eq "top");
    return "/$url/linklist.shtml"              if($what eq "seealso");
    return "/$url/indexdate.shtml"             if($what eq "dateindex");
    return undef;
}
#▶1 main::File
package main::File;
#▶2 BEGIN
use strict;
use utf8;
use fields (qw(open changeDirectory));
use vars (qw($oldwd $optdir));
use Fcntl (qw(:DEFAULT :flock));
$oldwd=".";
$optdir=".";
#▶2 changeDirectory
sub changeDirectory(;$) {
    chdir ((shift)?($oldwd):$optdir);
}
#▶2 open
sub open($;$$$$) {
    my ($fname, $chdir, $trunc, $die, $message)=@_;
    changeDirectory($chdir);
    my $FH;
    my $msg;
    if(defined $trunc and $trunc==1) {
        if(not sysopen($FH, $fname, O_WRONLY | O_CREAT)) {
            $msg=$message || "Failed to open $fname for truncating"; }
        elsif(not flock($FH, LOCK_EX)) {
            warn $message if($message);
            $msg="Failed to obtain exclusive lock for $fname";
        }
        elsif(not truncate($FH, 0)) {
            warn $message if($message);
            $msg="Failed to truncate $fname";
        }
    }
    elsif(not $trunc) {
        if(not -r $fname) {
            $msg=$message || "File “$fname” does not exist"; }
        elsif(not open($FH, "<", $fname)) {
            my $msg=$message || "Failed to open $fname"; }
        elsif(not flock($FH, LOCK_SH)) {
            warn $message if($message);
            my $msg="Failed to obtain shared lock for $fname"; }
    }
    if(defined $msg) {
        if($die) { die $msg;  }
        else     { warn $msg; }
        return undef;
    }
    return $FH;
}
#▶1 main::YAML
package main::YAML;
#▶2 BEGIN
use strict;
use utf8;
use Encode;
use fields (qw(load dump enclose));
use vars (qw($implementation));
sub YLoadFile;
sub YDump;
BEGIN {
    my $found=0;
    # YAML::XS is known to dump ^G bytes as-is, so does YAML::Syck and
    # YAML::Tiny
    for my $mod (qw(YAML::Old YAML::Any YAML::Tiny YAML::Syck YAML::XS YAML)) {
        if(eval "require $mod; 1;") {
            for my $s (qw(LoadFile Dump)) {
                eval "sub Y$s { return \&$mod\::$s(\@_); }"; }
            $found=1;
            last;
        }
    }
    die "No YAML modules found" unless $found;
}
#▶2 load: Загрузить данные из файла
sub load($) {
    my $fname = shift;                             # Имя файла
    ::File::changeDirectory();
    -r $fname or return {};
    print STDERR "Loading data from file “$fname”\n"
        if($::o_verbose>2);
    my $data  = eval{YLoadFile($fname)};           # Загрузка
    if($@) {
        warn "Failed to load YAML file “$fname” with message $@";
        return undef; }
    return $data;
}
#▶2 dump: Сохранить данные
sub dump($;$) {
    my $data  = shift;                             # Данные для сохранения
    my $fname = shift;                             # Целевой файл
    my $text=YDump($data);
    $text=Encode::decode_utf8($text) unless Encode::is_utf8($text);
    #▶3 Указано имя файла — сохраняем туда
    if(defined $fname) {
        my $FH=::File::open($fname, 0, 1);         # Filehandler
        binmode $FH, ":utf8";
        return undef unless defined $FH;
        print STDERR "Dumping data to $fname\n"
            if($::o_verbose > 2);
        print $FH $text;                           # Сохранение
        close $FH;
        return 1;
    }
    #▲3
    # Имя файла не указано — возвращаем строку
    return $text;
}
#▶2 enclose
sub enclose($) {
    my $str=shift;
    $str=~s/'/''/g;
    return "'$str'";
}
#▶1 main::Archive
package main::Archive;
#▶2 BEGIN
use strict;
use warnings;
use utf8;
use fields ('add');
use vars ('@ISA');
use IO::Compress::Zip;
@ISA=('IO::Compress::Zip');
#▶2 new
sub new {
    my $invocant = shift;
    my $arch     = IO::Compress::Zip->new(@_);
    my $class    = ref $invocant || $invocant;
    return bless $arch, $class;
}
#▶2 add
sub add($$;$) {
    my ($self, $fname, $text)=@_;
    $self->newStream(Name => $fname) or return undef;
    print $self $text if(defined $text and not ref $text);
    return $self;
}
#▶1 main::XML
package main::XML;
#▶2 BEGIN
use strict;
use warnings;
use utf8;
use fields (qw(newDocument
               newNode
               addNodeWithText
               addNodeWithCDATA
               addText
               checkInside));
# use Carp qw/longmess cluck confess/;
#▶2 newDocument
sub newDocument($) {
    my $rootelement=shift;
    my $doc=XML::DOM::Document->new();
    my $xmldecl=$doc->createXMLDecl("1.0", "UTF-8", undef);
    $doc->setXMLDecl($xmldecl);
    my $element=$doc->createElement($rootelement);
    $doc->appendChild($element);
    return $element;
}
#▶2 newNode: новый тёг внутри указанного
sub newNode($$) {
    my ($element, $tagname)=@_;
    return $element unless $tagname;
    my $e=$element->getOwnerDocument->createElement($tagname);
    $element->appendChild($e);
    return $e;
}
#▶2 addNodeWithText: новый тёг с текстом внутри указанного
sub addNodeWithText($$$) {
    my ($element, $tagname, $text)=@_;
    return undef unless defined $text;
    my $e=::XML::newNode($element, $tagname);
    $e->addText($text);
    return $e;
}
#▶2 addNodeWithCDATA: новый тёг с CDATA внутри указанного
sub addNodeWithCDATA($$$) {
    my ($element, $tagname, $cdata)=@_;
    return undef unless defined $cdata;
    my $e=::XML::newNode($element, $tagname);
    my $ce=$e->getOwnerDocument->createCDATASection($cdata);
    $e->appendChild($ce);
    return $e;
}
#▶2 addText: добавить текст
sub addText($$) {
    my ($element, $text)=@_;
    $element->addText($text);
    return $element;
}
#▶2 checkInside: Проверка, находимся ли мы внутри тёга p
sub checkInside($;$) {
    my $element = shift;
    my $tn      = shift || "p";
    while(defined $element) {
        return $element if($element->getNodeName eq $tn);
        $element=$element->getParentNode;
    }
    return 0;
}
#▶1 main::RSS
package main::RSS;
#▶2 BEGIN
use strict;
use warnings;
use utf8;
use fields qw(save add);
use vars qw(%rss);
use POSIX qw(locale_h);
use XML::DOM;
use Cwd;
POSIX::setlocale(LC_TIME, "POSIX");
%rss=();
#▶2 new
sub new {
    #▶3 Объявление переменных
    my $invocant = shift;
    my $class    = ref $invocant || $invocant;
    my $author   = shift;
    my $rssopts  = shift || $author->o_rssopts;
    my $rssmax   = shift || $author->o_rssmax;
    my $rssfile  = Cwd::realpath(shift || $author->o_rssfile);
    my $erss     = $::RSS::rss{$rssfile};
    my $rss;
    #▶3 Файл уже используется
    if(defined $erss) {
        $rss=$erss;
        goto RET;
    }
    #▶3 Файл существует
    if(-r $rssfile) {
        eval{$rss=XML::DOM::Parser->new(KeepCDATA => 1)->parsefile($rssfile)->
                                                            getDocumentElement};
        if($@) {
            warn "Failed to parse $rssfile with error “$@”";
            goto NEW;
        }
        unless($rss->getNodeName eq "rss") {
            warn "Root tag is not <rss> in $rssfile";
            goto NEW;
        }
        unless($rss->hasChildNodes) {
            warn "<rss> tag has no child nodes in $rssfile";
            goto NEW;
        }
        $rss=$rss->getChildNodes->item(0);
        unless($rss->getNodeName eq "channel") {
            warn "<rss> tag must contain <channel> tag in $rssfile";
            goto NEW;
        }
        goto RET;
    }
    #▶3 Файл не существует
  NEW:
    $rss=::XML::newDocument("rss");
    $rss=::XML::newNode($rss, "channel");
    ::XML::addNodeWithText($rss, "title",       $rssopts->{"title"}      );
    ::XML::addNodeWithText($rss, "link",        $rssopts->{"link"}       );
    ::XML::addNodeWithText($rss, "description", $rssopts->{"description"});
    #▶3 В любом случае
  RET:
    my $r=bless {
        rss     => $rss,
        rssfile => $rssfile,
        rssopts => $rssopts,
        rssmax  => $rssmax,
    }, $class;
    $::RSS::rss{$rssfile}=$rss;
    return $r;
    #▲3
}
#▶2 add
sub add($$$;@) {
    my $self         = shift;
    my $author       = shift;
    my $publication  = shift;
    my $rssopts      = $self->{"rssopts"};
    my $rss          = $self->{"rss"};
    return 0 unless defined $rss;
    my $pubinfo      = $author->info->publications->{$publication};
    my $savedpubinfo = $author->savedinfo->publications->{$publication};
    my $link         =
        ((defined $rssopts->{"itemlink"})?
            ($author->subs($rssopts->{"itemlink"}, $publication, @_)):
            ((defined $pubinfo)?(ZLR::UA::absURL($pubinfo->url)):
                ((defined $savedpubinfo)?(ZLR::UA::absURL($savedpubinfo->url)):
                    (""))));
    my $time         = POSIX::strftime("%a, %d %b %Y %T %z", localtime(time));
    my $title        = $author->subs($rssopts->{"itemtitle"}, $publication, @_);
    my $description  = $author->subs($rssopts->{"itemdescription"},
                                     $publication, @_);
    my $item         = ::XML::newNode($rss, "item");
    ::XML::addNodeWithText ($item, "pubDate",       $time       );
    ::XML::addNodeWithText ($item, "link",          $link       );
    ::XML::addNodeWithText ($item, "title",         $title      );
    ::XML::addNodeWithCDATA($item, "description",   $description);
    return $self->save();
}
#▶2 save
sub save($) {
    #▶3 Объявление переменных
    my $self    = shift;
    my $rss     = $self->{"rss"};
    my $rssmax  = $self->{"rssmax"};
    my $rssfile = $self->{"rssfile"};
    return undef unless defined $rss;
    #▶3 Удаление лишних новостей
    goto SAVE if $rssmax == -1;
    my @nodes=$rss->getElementsByTagName("item", 0);
    if(scalar @nodes > $rssmax) {
        my $i=(scalar @nodes)-$rssmax;
        while($i--) {
            $rss->removeChild($nodes[$i]); }
    }
    #▶3 Сохранение ленты
  SAVE:
    my $rssfh=::File::open($rssfile, 0, 1);
    return undef unless(defined $rssfh);
    binmode $rssfh, ":utf8";
    $rss->getOwnerDocument->print($rssfh);
    close $rssfh;
    return $self;
    #▲3
}
#▶1 main::Convert
package main::Convert;
#▶2 BEGIN
use strict;
use warnings;
use utf8;
use vars qw(%ptags %noadjtags %inptags %noinptags %noempty $pstart %expectfw);
use HTML::TreeBuilder;
use HTML::Entities;
use MIME::Base64;
use Image::Info;
use Encode;
$pstart =                                                # Начало произведения
"<!----------- Собственно произведение --------------->";
#▶3 %ptags: соответствия между HTML и fb2 тёгами
%ptags=(
     i          => "emphasis",
     b          => "strong",
     em         => "emphasis",
     strong     => "strong",
     p          => "p",
     abbr       => ["style",    {name  => "abbr"   }],
     acronym    => ["style",    {name  => "acronym"}],
     address    => "emphasis",
     bdo        => ["style",    {name  => "bdo"    }],
     big        => ["style",    {name  => "big"    }],
     blockquote => "cite",
     caption    => ["style",    {name  => "caption"}],
     center     => ["style",    {name  => "center" }],
     cite       => ["style",    {name  => "cite"   }],
     code       => ["p",        {style => "code"   }],
     pre        => ["p",        {style => "pre"    }],
     dd         => ["p",        {style => "dd"     }],
     del        => "strikethrough",
     dfn        => "emphasis",
     dt         => ["p",        {style => "dt"     }],
     q          => "emphasis",
     s          => "strikethrough",
     strike     => "strikethrough",
     small      => ["style",    {name  => "small"  }],
     span       => "",
    "sub"       => "sub",
     sup        => "sup",
     dl         => "",
     ul         => "",
     ol         => "",
     li         => ["p",        {style => "li"     }],
     table      => "table",
     tr         => "tr",
     td         => "td",
     th         => ["td",       {style => "header" }],
     font       => "",
);
#▶3 %noadjtags
%noadjtags=qw(
    i      1
    b      1
    em     1
    strong 1
    q      1
    s      1
    sub    1
    sup    1
    small  1
    strike 1
    del    1
    big    1
);
#▶3 %inptags
%inptags=(
    qw(),
    %noadjtags
);
#▶3 %noempty
%noempty=(
    qw(
        font 1
    ),
    %noadjtags,
);
#▶3 %noinptags
%noinptags=qw(
    p          1
    ol         1
    ul         1
    div        1
    h1         1
    h2         1
    h3         1
    h4         1
    h5         1
    h6         1
    blockquote 1
    br         p
    dd         p
);
#▶3 %expectfw
%expectfw=qw(
    интерлюдия 5
    пролог     5
    эпилог     5
    глоссарий  5
    глава      4
    часть      3
    словарь    2
    раздел     2
    секция     1
);
#▶2 escape
sub escape($;$) {
    local $_=shift;
    my $htmlescape=shift;
    $_=Encode::decode_utf8($_) unless Encode::is_utf8($_);
    unless($htmlescape) {
        s/([^a-zA-Z0-9_\#*^=\s:\-\/\n><"@.])/"&#".(ord $1).";"/geo; }
    else {
        s/([^ -~\n])/"&#".(ord $1).";"/geo; }
    return $_;
}
#▶2 zlrEmail
sub zlrEmail($) {
    my $email=shift;
    $email=~s/&\#(\d+)/chr $1/gex;
    return $email;
}
#▶2 toFB2: Преобразовать в fb2
#▶3 AddImage: Добавить изображение
sub AddImage($$$$) {
    # $element — текущий HTML::Element
    # $doc     — текущий XML::DOM::Node
    # $imgarch — архив (или undef)
    # $source  — URL произведения
    my ($element, $doc, $imgarch, $source)=@_;

    my $src=$element->attr("src");          # Значение аттрибута src
    my $image=ZLR::UA::get($src, $source); # Само изображение

    return undef unless defined $image;

    my $imginfo=eval{Image::Info::image_info(\$image)};
    if($@ or $imginfo->{"error"}) {
        warn "Invalid image: ".($@ || $imginfo->{"error"});
        return undef;
    }

    my $e=::XML::newNode($doc, "image");    # Node с ссылкой на изображение

    my $imgname=(time)."-".(rand);          # Уникальное имя в архиве
    $imgname=~s/\.//g;
    $imgname.=".".$imginfo->{"file_ext"};
    if(defined $imgarch) {
        $imgname="images/$imgname";
        $imgarch->add($imgname, $image);        # Сохранение в архив
        $e->setAttribute("l:href", $imgname);   # l:href — аттрибут с ссылкой
    }
    else {
        my $b=::XML::addNodeWithText(
                $doc->getOwnerDocument->getDocumentElement,
                "binary", MIME::Base64::encode_base64($image));
        $b->setAttribute("id", $imgname);
        $b->setAttribute("content-type", $imginfo->{"file_media_type"});
        $e->setAttribute("l:href", "#$imgname");
    }

    my $id=GetID($element);                     # id/name
    $e->setAttribute("id", $id) if(defined $id);

    return $e;
}
#▶3 AddRef: Добавить ссылку
sub AddRef($$$$) {
    # $element — текущий HTML::Element
    # $doc     — текущий XML::DOM::Node
    # $imgarch — архив (или undef)
    # $source  — URL произведения
    my ($element, $doc, $imgarch, $source)=@_;
    my $href=$element->attr("href");       # Значение аттрибута href
    if(defined $href) {                    # Игнорирование javascript:... ссылок
        undef $href if($href=~/^\s*javascript:/i); }

    my $id=GetID($element);                # id/name
    my $e=$doc;
    if(defined $href or defined $id) {
        $e=::XML::newNode($doc, "a");
        $e->setAttribute("l:href", $href) if(defined $href);
        $e->setAttribute("id", $id)       if(defined $id);
    }
    ::Convert::IncludeInner($element, $e, $imgarch);
    return $e;
}
#▶3 IncludeInner
sub IncludeInner($$$$) {
    # $element — текущий HTML::Element
    # $doc     — текущий XML::DOM::Node
    # $imgarch — архив (или undef)
    # $source  — URL произведения
    my ($element, $doc, $imgarch, $source)=@_;
    foreach ($element->content_list()) {
        ::Convert::IncludeElement($_, [$doc], $imgarch); }
}
#▶3 GetID: получение значений аттрибутов id или name
sub GetID($) {
    my $element=shift;
    my @opts=();
    my $id=$element->attr("id");
    return $id if(defined $id);
    my $name=$element->attr("name");
    return $name if(defined $name);
    return undef;
}
#▶3 EnsureP
# Если мы находимся внутри <p> и нам нужно создать тёг с именем <p>, вернуть 
# родителя <p>. Если мы не находимся внутри <p> и нам надо создать не <p>, то 
# вернуть новый тёг <p>. В противных случаях вернуть первый аргумент.
sub EnsureP($$) {
    my ($doc, $tn)=@_;
    my $inside=::XML::checkInside($doc);
    if($tn eq "p") {
        if($inside) {
            $doc=$inside->getParentNode; } }
    elsif(not $inside) {
        $doc=::XML::newNode($doc, "p"); }
    return $doc;
}
#▶3 IncludeElement: Добавить HTML::Element
sub IncludeElement($$$$) {
    my ($element, $sections, $imgarch, $source)=@_;
    #▶4 Элемент — просто текст
    if(not ref $element) {
        ::XML::addText($sections->[-1], ::Convert::ampToUTF($element));
        return undef;
    }
    #▲4
    my $t = lc $element->tag(); # Имя HTML тёга
    #▶4 Соответствие между тёгом HTML и тёгом fb2 определено в %ptags
    if(defined $ptags{$t}) {
        my $tn=$ptags{$t};      # Имя fb2 тёга
        my $opts;
        if(ref $tn) {
            $tn   = $ptags{$t}->[0];
            $opts = $ptags{$t}->[1];
        }
        my $id=::Convert::GetID($element);
        my $doc=::XML::newNode(::Convert::EnsureP($sections->[-1], $tn), $tn);
        if(defined $id) {
            $doc->setAttribute("id", $id); }
        if(defined $opts) {
            foreach (keys %$opts) {
                $doc->setAttribute($_, $opts->{$_}) } }
        ::Convert::IncludeInner($element, $doc, $imgarch, $source);
    }
    #▶4 Заголовок
    elsif($t=~/h([1-6])/) {
        my $level=$element->attr("rel") || $1;
        $level=~s/^level-//;
        if($level<6) {
            pop @$sections
                while($level<(scalar @$sections));
            push @$sections, ::XML::newNode($sections->[-1], "section")
                while($level>=(scalar @$sections));
        }

        my $tn="title";
        my $opts;
        my $id=::Convert::GetID($element);
        my $doc=::XML::newNode($sections->[-1], $tn);
        if(defined $id) {
            $doc->setAttribute("id", $id); }
        ::Convert::IncludeInner($element, $doc, $imgarch, $source);
    }
    #▶4 Изображение
    elsif($t eq "img") {
        ::Convert::AddImage($element, $sections->[-1], $imgarch, $source); }
    #▶4 Ссылка
    elsif($t eq "a") {
        ::Convert::AddRef($element, $sections->[-1], $imgarch, $source); }
    #▶4 div
    # <div> превращается в <p>, если его стилевой класс не «empty». См. 
    # регулярные выражения в typo
    elsif($t eq "div") {
        my $class=$element->attr("class");
        if(defined $class and $class eq "empty") {
            my $inside=::XML::checkInside($sections->[-1]);
            my $doc;
            if($inside) {
                $doc=$inside->getParentNode(); }
            else {
                $doc=$sections->[-1]; }
            ::XML::newNode($doc, "empty-line");
        }
        else {
            my $id=::Convert::GetID($element);
            my $doc=::XML::newNode(::Convert::EnsureP($sections->[-1],"p"),"p");
            ::Convert::IncludeInner($element, $doc, $imgarch, $source);
        }
    }
}
#▶3 IncludeHTML
sub IncludeHTML($$$$) {
    my ($html, $doc, $imgarch, $source)=@_;
    my $tree=HTML::TreeBuilder->new_from_content($html);
    $tree=$tree->find('body');
    my $sections=[$doc];
    foreach ($tree->content_list()) {
        ::Convert::IncludeElement($_, $sections, $imgarch, $source); }
}
#▶3 toFB2
sub toFB2($$$$) {
    #▶4 Объявление переменных
    my ($text, $arch, $pubinfo, $source)=@_;
    print STDERR "Starting conversing “".$pubinfo->title."” to fb2\n"
        if($::o_verbose > 2);
    #▶4 Заголовок
    my $doc=::XML::newDocument("FictionBook");
    $doc->setAttribute("xmlns", "http://www.gribuser.ru/xml/fictionbook/2.0");
    $doc->setAttribute("xmlns:l", "http://www.w3.org/1999/xlink");
    #▶4 Описание произведения
    #▶5 Объявление переменных
    my $description  = ::XML::newNode($doc,         "description");
    my $titleInfo    = ::XML::newNode($description, "title-info");
    my $documentInfo = ::XML::newNode($description, "document-info");
    ::XML::addNodeWithText($documentInfo, "program-used",
                           "zlrchecker-$::VERSION");
    ::XML::addNodeWithText($documentInfo, "src-url", $source);
    ::XML::addNodeWithText(::XML::newNode($titleInfo, "author"),
                            "first-name", $pubinfo->author);
    ::XML::addNodeWithText($titleInfo, "book-title", $pubinfo->title);
    #▶5 Аннотация
    my $author=$pubinfo->p_author;
    if(defined $pubinfo->annotation) {
        my $element=::XML::newNode($titleInfo, "annotation");
        IncludeHTML(typo($pubinfo->annotation, $author),
                    $element, $arch, $pubinfo->source);
    }
    #▶5 Раздел
    if(defined $pubinfo->section) {
        ::XML::newNode($titleInfo, "sequence")->setAttribute("name",
                                                       $pubinfo->section);
    }
    #▶4 Текст
    $doc=::XML::newNode($doc, "body");
    IncludeHTML($text, $doc, $arch, $source);
    #▲4
    print STDERR "Finished conversing “".$pubinfo->title."” to fb2\n"
        if($::o_verbose > 2);
    return $doc->getOwnerDocument->toString;
}
#▶3 htmlToFB2
sub htmlToFB2($$$) {
    my ($text, $arch, $source)=@_;
    my $doc=::XML::newDocument("FictionBook");
    $doc->setAttribute("xmlns", "http://www.gribuser.ru/xml/fictionbook/2.0");
    $doc->setAttribute("xmlns:l", "http://www.w3.org/1999/xlink");
    my $description  = ::XML::newNode($doc,         "description");
    my $titleInfo    = ::XML::newNode($description, "title-info");
    my $documentInfo = ::XML::newNode($description, "document-info");
    ::XML::addNodeWithText($documentInfo, "program-used",
                           "zlrchecker-$::VERSION");
    $doc=::XML::newNode($doc, "body");
    # XXX probably replace with some code in normalizeTree?
    $text=~s/<br\s*\/?>/<p>/gis;
    IncludeHTML($text, $doc, $arch, $source);
    return $doc->getOwnerDocument->toString;
}
#▶2 typo: Типографирование
#▶3 ampToUTF
sub ampToUTF($) {
    local $_=shift;
    return HTML::Entities::decode_entities($_);
}
#▶3 normalizeTree
sub normalizeTree($) {
    #▶4 upmoveElements :: Element(parent[, children])
    # перемещает детей элемента к его родителю и удаляет элемент
    sub upmoveElements($) {
        my $e=shift;
        my @nodes=$e->detach_content();
        $e->postinsert(@nodes) if(scalar @nodes);
        my $parent=$e->parent;
        $e->delete();
    }
    #▶4 delIfEmptyAfterEmpty :: Element, Bool
    sub delIfEmptyAfterEmpty($$) {
        my $e=shift;
        my $prevempty=shift;
        my @nodes=$e->content_list;
        if(not scalar @nodes
                or (scalar @nodes == 1
                    and not ref $nodes[0]
                    and $nodes[0]=~/^\s*$/s))
        {
            if($prevempty) {
                $e->delete();
                return 2;
            }
            else {
                return 1;
            }
        }
        else {
            return 0;
        }
    }
    #▶4 normalizeParagraph :: Element(parent[, children])
    sub normalizeParagraph($;$$);
    sub normalizeParagraph($;$$) {
        #▶5 Объявление переменных
        my $e     = shift;
        my $p     = shift || $e;
        my $depth = shift || 0;
        #▶5 moveTo :: source:Element, target:Element([children])
        # Перемещает всех детей source к target и удаляет source
        sub moveTo($$) {
            my ($source, $target)=@_;
            my @nodes=$source->detach_content();
            $target->push_content(@nodes) if(scalar @nodes);
            $source->delete();
        }
        #▶5 $splitParagraph :: idx, Element (+ $p)
        # Перемещает ребёнка с индексом idx сразу после родителя элемента, 
        # создаёт p, в который помещает всех следующих детей элемента, размещая 
        # p следующим после перемещённого элемента
        my $splitParagraph=sub {
            my ($i, $e)=@_;
            #▶6 $postinsert :: Element, Element+ (+ $p, $e)
            my $postinsert=sub {
                if($p ne $e) {
                    my @addtags=();
                    my $cur=$e;
                    my $i=0;
                    while(ref $cur and $i<$depth) {
                        unshift @addtags, [$cur->tag, $cur->all_external_attr];
                        $cur=$cur->parent;
                        $i++;
                    }
                    undef $cur;
                    @addtags=map {$p->new(@$_)} @addtags;
                    my $parent=shift @addtags;
                    my $last=$parent;
                    while(scalar @addtags) {
                        my $cur=shift @addtags;
                        $last->push_content($cur);
                        $last=$cur;
                    }
                    for my $node (@_) {
                        my $dp=$node;
                        my @nodes=$node->detach_content();
                        $node->push_content($parent->clone());
                        while(scalar $node->content_list) {
                            $node=[$node->content_list]->[0];
                        }
                        $node->push_content(@nodes);
                        $node=$dp;
                    }
                }
                $p->postinsert(@_);
            };
            #▲6
            my ($oldp)=$e->splice_content($i, 1);
            $oldp->attr("align", $e->attr("align"))
                if(defined $e->attr("align")
                        and not defined $oldp->attr("align"));
            my @nodes=$e->splice_content($i);
            if(scalar @nodes) {
                my $par=$p->new($p->tag, $p->all_external_attr);
                $par->push_content(@nodes);
                $postinsert->($oldp, $par);
            }
            else {
                $postinsert->($oldp);
            }
        };
        #▶5 Удаление лишних пробелов
        $e->normalize_content();
        # $e->delete_ignorable_whitespace();
        my $child=[$e->content_list]->[0];
        if(defined $child and not ref $child and $child=~s/^\s+//s) {
            $e->splice_content(0, 1, $child);
        }
        $child=[$e->content_list]->[-1];
        if(defined $child and not ref $child and $child=~s/\s+$//s) {
            $e->splice_content(-1, 1, $child);
        }
        #▶5 Переименование тёга
        if(defined $e->tag) {
            my $tagname=$noinptags{$e->tag};
            if(defined $tagname and $tagname ne "1") {
                $e->tag($tagname);
            }
        }
        #▶5 Основной цикл
        my $i=0;
        while($i<(scalar $e->content_list)) {
            my $element=[$e->content_list]->[$i];
            if(ref $element) {
                my $tag=(lc $element->tag);
                if(defined $noadjtags{$tag}) {
                    while(1) {
                        my $el=[$e->content_list]->[$i+1];
                        if(ref $el) {
                            if($el->tag eq $tag) {
                                moveTo($el, $element);
                            }
                            else {
                                last;
                            }
                        }
                        elsif(defined $el and $el=~/^\s*$/o) {
                            my $el2=[$e->content_list]->[$i+2];
                            if(ref $el2 and $el2->tag eq $tag) {
                                $e->splice_content($i+2, 1);
                                $element->push_content($el);
                                moveTo($el2, $element);
                            }
                            else {
                                last;
                            }
                        }
                        else {
                            last;
                        }
                    }
                }
                elsif($tag eq "font") {
                    if(defined $element->attr("color")) {
                        $element->attr("color", undef)
                            if($element->attr("color")=~/^(?:#?0+|black)$/i)
                    }
                    upmoveElements($element)
                        unless scalar $element->all_external_attr_names;
                }
                elsif(defined $noinptags{$tag}) {
                    $splitParagraph->($i, $e);
                    $i++;
                    next;
                }
                normalizeParagraph($element, $p, $depth+1);
            }
            $i++;
        }
    }
    #▶4 newPar :: idx, Element(children)
    # Создаёт новый p, перемещая в него все элементы, которые должны быть в p
    sub newPar($$) {
        my $i=shift;
        my $tree=shift;
        my $p=$tree->new('p');
        my $j=0;
        while(1) {
            my $element=[$tree->content_list]->[$i];
            if(ref $element) {
                if(defined $inptags{$element->tag}) {
                    $element->detach();
                    $p->push_content($element);
                }
                else {
                    last; }
            }
            elsif(defined $element) {
                $tree->splice_content($i, 1);
                $p->push_content($element);
            }
            else {
                last; }
        }
        if($i==0) {
            $tree->unshift_content($p); }
        else {
            [$tree->content_list]->[$i-1]->postinsert($p); }
    }
    #▶4 Объявление переменных
    my $tree=shift;
    my $i=0;
    #▶4 Основной цикл
    my $prevempty=0;
    while($i<(scalar$tree->content_list)) {
        my $element=[$tree->content_list]->[$i];
        if(ref $element) {
            my $tag=(lc $element->tag);
            if($tag eq "div") {
                $prevempty=delIfEmptyAfterEmpty($element, $prevempty);
                next if($prevempty==2);
                my $align=lc($element->attr("align"))
                    if(defined $element->attr("align"));
                if(defined $align and $align eq "justify") {
                    upmoveElements($element);
                    next;
                }
                else {
                    normalizeParagraph($element);
                }
            }
            elsif($tag eq "dl") {
                $prevempty=0;
                upmoveElements($element);
                next;
            }
            elsif($tag eq "p" or $tag eq "dd") {
                $element->tag("p") unless $tag eq "p";
                normalizeParagraph($element);
                $prevempty=delIfEmptyAfterEmpty($element, $prevempty);
                next if($prevempty==2);
            }
            elsif(defined $inptags{$tag}) {
                $prevempty=0;
                newPar($i, $tree);
                next;
            }
            elsif(defined $noinptags{$tag}) {
                $prevempty=0;
                normalizeParagraph($element);
            }
        }
        else {
            newPar($i, $tree);
            next;
        }
        $i++;
    }
}
#▶3 normalizeHTML
sub normalizeHTML($$;$) {
    print STDERR "Normalizing HTML tree\n"
        if($::o_verbose>3);
    local $_          = shift;
    my $detectheaders = shift;
    my $tree=HTML::TreeBuilder->new_from_content($_);
    normalizeTree($tree->find('body'));
    detectSections($tree) if($detectheaders);
    if(shift) {
        my $r=$tree->find('body')->as_HTML('<&>', '', {});
        $r=~s/\s*<\/?body>\s*//gs;
        return $r;
    }
    return $tree->as_HTML('<>&', '  ', {});
}
#▶3 detectSections
sub detectSections($) {
    #▶4 Объявление переменных
    print STDERR "Trying to detect sections\n"
        if($::o_verbose>3);
    my $tree=shift;
    my @paragraphs=$tree->find('body')->content_list;
    my @foundparagraphs=();
    local $_;
    my %firstwords=();
    my @suggests=();
    my $prevempty;
    my $i=0;
    #▶4 Основной цикл
    for my $p (@paragraphs) {
        $_=$p->as_text;
        #▶4 *** (абзац из звёздочек)
        if(/^(\s*\*)+\s*$/) {
            push @suggests, ["aa", $p, $i];
            next;
        }
        #▶4 Остальные абзацы
        my $firstword=$1 if(/(\S+)/);
        #▶5 Абзац пуст?
        if(not defined $firstword) {
            $prevempty=1;
            next;
        }
        else {
            $prevempty=0; }
        #▶5 Объявление переменных
        my $lfirstword=lc $firstword; # первое слово в нижнем регистре
        my $weight=0;                 # оценка вероятности встречи заголовка
        my %components=(text => $_);  # компоненты этой оценки
        #▶5 Предыдущий абзац пуст
        if($prevempty) {
            $components{"prevempty"}=1;
            $weight+=1;
        }
        #▶5 Следующий абзац пуст
        if(defined $paragraphs[$i+1]
                and $paragraphs[$i+1]->as_text =~ /^\s*$/) {
            $components{"nextempty"}=1;
            $weight+=$prevempty;
        }
        #▶5 Первое слово начинается с цифр
        if($firstword=~/^(\d+)/) {
            $components{"startswithdigit"}=$1;
            $weight+=1;
        }
        #▶5 Первое слово состоит целиком из больших букв
        if($firstword=~/^\p{Upper}+([.?!]+|[:,])?$/) {
            $components{"uppercase"}=defined $1 ? $1 : "";
            $weight+=2;
        }
        #▶5 Первое слово начинается с большой буквы
        elsif($firstword=~/^\p{Upper}/) {
            $components{"uppercase"}=1;
            $weight+=1;
        }
        #▶5 Первое слово находится в списке слов, обычно начинающих раздел
        my $lfwonly=$lfirstword;
        $lfwonly=~s/^.*?(\pL+).*$/$1/;
        if(defined $expectfw{$lfwonly}) {
            $components{"knownword"}=$lfirstword;
            $weight+=$expectfw{$lfwonly};
        }
        #▶5 Первое слово — единственное в абзаце
        if(/^\s*\Q$firstword\E\s*$/) {
            $components{"oneword"}=$lfwonly;
            $weight+=1;
            if($firstword=~/([.?!]+|:)$/) {
                $components{"endswithpunct"}=$1;
                $weight+=1;
            }
            elsif($firstword=~/\pL$/) {
                $components{"endofline"}=1;
                $weight+=1;
            }
        }
        #▶5 Анализ текста после первого слова
        do {
            #▶6 Объявление переменных
            my $oldtext=$_;
            local $_=$oldtext;
            s/^\s*\Q$firstword\E\s*//; #Удаляем первое слово
            #▶6 За первым словом идёт число
            if(/^(\d+)([.,])?($)?/) {
                $components{"beforedigit"}=$1+0;
                $weight+=2;
                my $prevnum=0;
                if(defined $firstwords{$lfirstword}) {
                    my $prevstat;
                    my $fl=$firstwords{$lfirstword};
                    if(defined $fl and scalar @$fl) {
                        my $j=-1;
                        while(defined $fl->[$j]) {
                            if(defined $fl->[$j][4]{"beforedigit"}) {
                                $prevstat=$fl->[$j][2];
                                $prevnum=$fl->[$j][4]{"beforedigit"};
                                last;
                            }
                            $j--
                        }
                    }
                    $components{"prevnumber"}=$prevnum;
                    if($components{"beforedigit"}==($prevnum+1)) {
                        $components{"digitincremented"}=$prevstat;
                        $weight+=3;
                    }
                }
                elsif($components{"beforedigit"}==1) {
                    $components{"numberone"}=1;
                    $weight+=2;
                }
                if(defined $2) {
                    $components{"afterdigit"}=$2;
                    $weight+=(($2 eq '.') ? 2 : 1);
                }
                if(defined $3) {
                    $components{"beforeend"}=$1;
                    $components{"beforeend"}.=$2 if(defined $2);
                    $weight+=1;
                }
            }
            #▶6 За первым словом идёт одно или два, затем конец предложения
            elsif(/^(\S+)(?:\s+(\S+))?([.:])?$/) {
                $components{"beforeend"}=[$1];
                if(defined $2) {
                    push @{$components{"beforeend"}}, $2; }
                else {
                    $weight+=1; }
                if(defined $3) {
                    $components{"beforeend"}->[-1].=$3;
                    $weight+=1;
                }
            }
        };
        #▶5 Проверка длины строки
        my $slen=length $_;
        $components{"length"}=$slen;
           if($slen>64)  { $weight-=1; }
        elsif($slen>128) { $weight-=2; }
        elsif($slen>256) { $weight-=3; }
        #▶5 Запись результата
        my $result=["fw", $p, $i, $weight, \%components];
        push @suggests, $result if($weight>4);
        if(defined $firstwords{$lfirstword}) {
            push @{$firstwords{$lfirstword}}, $result; }
        else {
            $firstwords{$lfirstword}=[$result]; }
    }
    continue {
        $i++;
    }
    #▶4 Поиск ложных срабатываний и пропущенных глав
    # XXX dummy (no empirics, no subsections)
    @foundparagraphs=map {[$_->[0] eq "aa" ? 6 : 1, $_->[1]]} @suggests;
    #▶4 Превращение найденных абзацев в заголовки
    map {
        my ($l, $t)=@$_;
        my $h=$l;
        if($l>6) {
            # my @nodes=$t->detach_content();
            # if(scalar @nodes) {
                # my $b=$t->new('b');
                # $b->push_content(@nodes);
                # $t->push_content($b);
            # }
            $h=6;
        }
        $t->tag("h$h");
        $t->attr(rel => "level-$l");
    } @foundparagraphs;
    # while(my ($lword, $i)=each()) {
    # }
    # print STDERR ::YAML::dump(\@suggests);
    # print STDERR ::YAML::dump([map {[$_->[0], $_->[2], defined $_->[3] ? ($_->[3]) : ($_->[1]->as_text())]} @suggests]);
    # print STDERR ::YAML::dump([map {[$_->[0], $_->[2], defined $_->[3] ? ($_->[3], $_->[4]) : ($_)]} @suggests]);
}
#▶3 typo
sub typo($$) {
    #▶4 stripComments
    sub stripComments($) {
        print STDERR "Stripping comments\n"
            if($::o_verbose>3);
        local $_=shift;
        s/<!--.*?-->//gs;
        return $_;
    }
    #▶4 makeApostroph
    sub makeApostroph($$) {
        print STDERR "Creating apostrophs\n"
            if($::o_verbose>3);
        local $_=shift;
        my $minescq=shift;
        # Очень тормозит, экранирует одинарные штрихи на границах слов, если они 
        # часто встречаются на границах именно этих слов. Нужно на случай, если 
        # автор использует одинарные штрихи при образовании специфичных для 
        # произведения слов.
        my $wordrega='(?<![\pL\'])(\pL[\pL\']*\pL)\'(?![\'\pL])';
        my $wordregb='(?<![\pL\'])\'(\pL[\pL\']*\pL)(?![\pL\'])';
        my $wordreg="$wordrega|$wordregb";
        $wordreg=~s/(?<=\()(?=[^?])/?:/g;
        if($minescq <= 0) {
            s/$wordrega/$1’/g;
            s/$wordregb/’$1/g;
        }
        elsif($minescq <= 1) {
            my @words = sort map {lc} /$wordreg/g;
            my @uniqwords=();
            for my $word (@words) {
                if(not scalar @uniqwords or $word ne $uniqwords[-1]->[0]) {
                    my $nqword=$word;
                    $nqword=~s/^'|'$//g;
                    my @nqwords = /(?<!['\-\pL])'?\Q$nqword\E'?(?!['\-\pL])/ig;
                    push @uniqwords, [$word, scalar @nqwords, 1];
                }
                else {
                    $uniqwords[-1]->[2]++;
                }
            }
            for my $w (@uniqwords) {
                my ($word, $nqwc, $qwc) = @$w;
                if($nqwc>0 && $qwc/$nqwc >= $minescq) {
                    my $eword=$word;
                    $eword=~s/'/’/;
                    s/(?<!['\pL])\Q$word\E(?!['\pL])/$eword/g;
                }
            }
        }
        return $_;
    }
    #▶4 stripEmpty: Удаление пустых тёгов
    sub stripEmpty($) {
        print STDERR "Removing empty tags\n"
            if($::o_verbose>3);
        local $_=shift;
        s/<([abiu]|font|center|strong)(\s[^>]+)?>([[:blank:]]*)<\/\1>/$3/gsi;
        s/<\/([biu])>(\s*)<\1>/$2/gsi;
        s/<\/?xxx[^>]*>//gi;
        s/<p>\s*<\/p>|<p\s*\/>/<div class="empty"><\/div>/gsi;
        return $_;
    }
    #▶4 toUTF: Некоторые символы (ascii → Unicode, простые случаи)
    sub toUTF($) {
        print STDERR "Adding some UTF symbols\n"
            if($::o_verbose>3);
        local $_=shift;
        #Троеточие
        s/\.{3}/…/gs;
        #Copyright ©
        s/\([cCсС]\)/©/g;
        #®
        s/\(r\)/®/gi; # Ни разу не видел этот символ, но пусть будет
        #▶5 Тирэ
        # ---_ → ~—_
        s/-{2,3}\s/ — /gs;
        # [^ld-]-D → [^ld-]~—_D
        s/(?<![\pL[:digit:]:;()\-])-+(?![[:digit:]])/ — /g;
        s/(<p>(<\/?[abiuABIU]>|<\/?font[^>]*>|\s)*) +/$1/g;
        #▶5 Неразрывный пробел
        # Несколько неразрывных пробелов подряд не имеют смысла
        s/( )+/ /g;
        # Также не имеет смысла иметь обычный пробел перед неразрывным
        s/\s+(?= )//gs;
        #▶5 Номер
        # N \d → № d
        s/(?<=\s)N(\s*)(?=\d)/№$1/gs;
        #▲5
        return $_;
    }
    #▶4 makeQuotes: Заменить штрихи на «ёлочки» и „лапки“
    sub makeQuotes($$) {
        print STDERR "Creating quotes\n"
            if($::o_verbose>3);
        local $_=shift;
        my $qreg=shift;
        #▶5 Проверка $qreg
        eval{""=~/$qreg/};
        if($@) {
            warn "Invalid regex: qreg=/$qreg/";
            return $_;
        }
        #▶5 Штрихи внутри тёгов — устанавливаем защиту
        s/"(?=[^<>]*?>)/<!-- dq -->/gs;
        s/'(?=[^<>]*?>)/<!-- q -->/gs;
        s/\&quot;(?=[^<>]*?>)/<!-- quot -->/gs;
        #▲5
        s/\&quot;/"/g;
        # ."_ → .»_
        s/(?<=[!?.,…])$qreg(?=[\s )]|<\/p>)/»/g;
        # !". → !».
        s/(?<=[!?…])$qreg(?=[.<,;])/»/g;
        # :_" → :_«
        s/(?<=[:—])\s+$qreg/ «/g;
        # l"_ → l»_
        s/(?<=[\pL[:alnum:]])$qreg(?=[\p{P}\s]| )/»/gs;
        # )"_ → )»_
        s/(?<=\))$qreg(?=[\p{P}\s]| )/»/gs;
        # _"l → _«l
        s/(?<=[\s ])$qreg(?=[\pL[:alnum:]])/«/gs;
        # _"( → _«(
        s/(?<=[\s ])$qreg(?=\()/«/gs;
        # _" … → _«…
        s/(?<=[\s ])$qreg\s*(?=…)/«/g;
        # r."._ → r.»._
        s/(?<=\pL([.,!?…]))$qreg(?=[.,!?…][\s ])/»/g;
        # _.".r → _.».r
        s/(?<=[.,!?…][\s ])$qreg(?=\pL([.,!?…]))/«/g;
        # R"r → R«r
        s/(?<![\pL=])
            ((<\/?[abiuABIU]>|[.,?!…])*)
                $qreg
            ((<\/?[abiuABIU]>|[.,?!…])*)
        (?=\pL)/$1«$+/gx;
        # r"R → r«R
        s/(?<=\pL)
            ((<\/?[abiuABIU]>|[.,?!…])*)
                $qreg
            ((<\/?[abiuABIU]>|[.,?!…])*)
        (?!\pL)/$1»$+/gx;
        # »" → »»
        s/(?<=»)$qreg/»/g;
        # "« → ««
        s/$qreg(?=«)/«/g;
        # «..." → «...»
        s/(?<=«)
            (((?![«»])[^\n])*?)\s+
                ((<\/?font[^>]*>|<\/?[abiuABIU]>)*)
                    $qreg
                ((<\/?font[^>]*>|<\/?[abiuABIU]>)*)\s+/$1$3»$^N/gx;
        # "...» → «...»
        s/(?<=[>  ])$qreg(?:[  ]+)(((?![«»])[^\n])*?)(?=»)/«$1/g;
        # «_ → _«
        s/(?<!......[>(«])
            («)
          ((<\/?font[^>]*>|<\/?[abiuABIU]>|[.,?!…])*)\s*/\ $1$2/gx;
        # _» → »
        s/\s+((<\/?font[^>]*>|<\/?[abiuABIU]>|[.,?!…])*)(?=»)/$1/g;
        # # …»L → …«L
        # s/(?<=…)»(?![\x{c0}-\x{FF}\x{a3}\x{b8}[:alpha:]])/«/g;
        # Апостроф
        s/'/’/g;
        #▶5 Ёлочки внутри тёгов — снимаем защиту
        s/<!-- dq -->/"/g;
        s/<!-- q -->/'/g;
        s/<!-- quot -->/&quot;/g;
        #▶5 Лапки
        my @charpos=();
        # s/(?=([«»\n]))/push @charpos, [$1, 0+($-[0])]; "";/ge;
        # sort {$a->[1] <=> $b->[1]} @charpos;
        push @charpos, [$1, $-[0]] while /([«»\n])/g;
        my $level=0;
        my @replaces=();
        my $lineendpos=0;
        for my $cp (@charpos) {
            my ($char, $pos)=@$cp;
            if($char eq '«') {
                push @replaces, ['„', $pos, $level];
                $level++;
            }
            elsif($char eq '»') {
                $level--;
                push @replaces, ['“', $pos, $level];
                if($level==0) {
                    push @replaces, $pos;
                }
            }
            elsif($char eq "\n") {
                if($level>0) {
                    for my $c (reverse@replaces) {
                        last if not ref $c
                                or $c->[1]<=$lineendpos;
                        $c->[2]--;
                    }
                }
                $lineendpos=$pos;
                $level=0;
            }
        }
        for my $rp (@replaces) {
            substr($_, ($rp->[1]), 1)=$rp->[0] if ref $rp and $rp->[2]>0
                                                          and $rp->[2]%2; }
        # s/(?<=«)([^«»]*)«([^«»]*)»(?=([^«»]*|«[^«»]*»)*?»)/$1„$2“/g;
        # 1 while(s/(?<=“)([^«»]*)«([^«»]*)»(?=([^«»]*|«[^«»]*»)*?»)/$1„$2“/gx);
        s/(?<=\pL)'(?=\pL)/’/g;
        return $_;
    }
    #▶4 Объявление переменных
    my $text    = shift;
    my $author  = shift;
    my $minescq = $author->o_minescq;
    my $qreg    = $author->o_qreg;
    #▶4 Вызовы функций, объявленных ранее
    $text=stripComments($text);
    $text=normalizeHTML($text, $author->o_detectheaders, @_);
    $text=makeApostroph($text, $minescq);
    $text=stripEmpty($text);
    $text=toUTF($text);
    $text=makeQuotes($text, $qreg);
    #▲4
    return $text;
}
#▶3 zlrPrepare
sub zlrPrepare($$;$) {
    #▶4 Объявление переменных
    my $author  = shift;
    my $pubinfo = shift;
    my $text    = shift;
    $text=ZLR::UA::get($pubinfo->url) unless defined $text;
    $pubinfo->getInfo($text);
    #▶4 Начало и конец произведения: обрезаем лишнее
    print STDERR "Starting conversing ".
            ($pubinfo->filename || $pubinfo->title)."\n"
        if($::o_verbose > 2);
    $text=~s/$::Convert::pstart/<PublicationContents>/;
    $text=~s/\s*<hr\ssize=2\snoshade>\s*
             (<center>\s*)?
                 <table\sborder=0\scellpadding=0\scellspacing=0>.*?(?=<\/body>)/
              <\/PublicationContents>
             <\/div>/sx;
    $text=~s/.*<PublicationContents>/<html>/s;
    $text=~s/<\/PublicationContents>.*/<\/html>/s;
    $text=~s/[\r\n]/ /g;
    $text=~s/\s+/ /g;
    #▲4
    # FIXME WTF is going on?! This is how symbols are encoded in
    # http://samlib.ru/h/hitech_a/you_only_live_thrice_01.shtml.
    #
    # As you may see, this has nothing to do with unicode.
    $text=~s/&#0151;/—/g;
    $text=~s/&#0133;/…/g;
    $text=~s/&#0150;/–/g;
    $text=~s/&#0171;/«/g;
    $text=~s/&#0187;/»/g;
    $text=typo($text, $author);
    print STDERR "Successfully converted ".$pubinfo->filename."\n"
        if($::o_verbose>2);
    return $text;
}
#▶1 main
package main;
#▶2 BEGIN
use strict;
use warnings;

use utf8;
use Encode;

use POSIX;

use Cwd;
use Getopt::Long qw(:config gnu_getopt
                            no_ignore_case
                            auto_version
                            pass_through);
use vars qw($o_verbose $VERSION %propsets %proprenames $pass %canadd
            %sectionpropsets %sectionproprenames %sectioncanadd $datechk
            $o_maxcommentlen %sectiondefaultadd %sectionremoves %brns);
use Pod::Usage;
use URI::URL;
$o_maxcommentlen=1048576; # 1 MiB
$pass=sub { return shift; };
#▶3 %brns
%brns=qw(
    title      IZDATEL
    number     N_IZD
    publisher  PLACE
    date       D_IZD
    copynumber TIRAZ
    ISBN       ISBN
    buyref     BAY
);
#▶3 $datechk (->())
$datechk=sub {
    my $value=shift;
    return POSIX::strftime("%d/%m/%Y", localtime(time)) if($value eq "~");
    unless($value =~ /\d{2}\/\d{2}\/\d{4}/o) {
        warn "Date format: day/month/year (example: 01/01/1970)";
        return undef;
    }
    return $value;
},
#▶3 %sectionpropsets
%sectionpropsets=(
          secretemail => $pass,
    subscriptionemail => $pass,
                email => $pass,
    homepage          => $pass,
    passwd            => $pass,
    author            => $pass,
    title             => $pass,
    annotation        => $pass,
    birthplace        => $pass,
    date              => $datechk,
    commentssubscribe => sub { return 0+not(not(shift)); },
    startwith         => sub {
        my $value=shift;
        my $r=[];
        my @v=split /,/, $value;
        for my $p (@v) {
            push @$r,
                (keys %{&ZLR::Stuff::getSimilar("publications", $p, @_)})[0];
        }
        return $r;
    },
    denywrite         => sub { return {(2+(2*not(not(shift)))) => undef}; },
    denyread          => sub { return {(2+(2*not(not(shift)))) => undef}; },
    allowread         => sub { return {(2+(2*not(shift)))      => undef}; },
    denybooksread     => sub { return {(2+(2*not(not(shift)))) => undef}; },
    allowbooksread    => sub { return {(2+(2*not(shift)))      => undef}; },
    allowwrite        => sub {
        my $v=shift;
        return {5 => undef} if($v eq "2");
        return {(2+(2*not($v))) => undef};
    },
    top30 => sub {
        my $value=shift;
        if($value=~/^(10|[0-9])?:?([a-zA-Z0-9_]+):(.*?):(.*)$/) {
            return [{
                mark        => ((defined $1)? ($1+0): 0),
                authorlink  => $2,
                publication => $3,
                annotation  => $4,
            }];
        }
        warn "Top30 format: [mark:]author:publication:annotation\n";
        return undef;
    },
    seealso => sub {
        my $value=shift;
        if($value=~/^(.*?)%%(.*?)::(.*)$/) {
            return [{
                $1 => {
                    title       => $2,
                    description => $3,
                }
            }];
        }
        warn "SeeAlso format: URL%%title::description\n";
        return undef;
    },
    subsection => sub {
        my $value=shift;
        if($value=~/^(.+?)::(.*)$/o) {
            return [{
                TITL  => Encode::decode_utf8($1),
                ANNOT => Encode::decode_utf8($2),
            }];
        }
        warn "Subsection format: NAME::ANNOTATION\n";
        return undef;
    },
    bibliography => sub {
        my $value=shift;
        if($value=~s/^(.*?):(.*?)(:|$)//o) {
            my ($url, $itype)=($1, $2);
            my $r=eval{{
                URL     =>
                    (keys %{&ZLR::Stuff::getSimilar("bURL", $url, @_)})[0],
                IZDANIE =>
                    (keys %{&ZLR::Stuff::getSimilar("bIZDANIE", $itype, @_) ||
                                   {0=>undef}})[0],
            }};
            return undef unless defined $r;
            for my $k (keys %brns) {
                if($value=~s/\Q$k\E=(.*?)(:|$)//i) {
                    if($k eq "date") {
                        $r->{$brns{$k}}=$datechk->($1); }
                    else {
                        $r->{$brns{$k}}=$1; } }
                else {
                    $r->{$brns{$k}}=""; }
            }
            return [$r];
        }
        warn "Bibliography format: publication:type[:prop=value]\n";
        return undef;
    },
    subscription => sub {
        my $value=shift;
        if($value=~/^((.)[^\/]*)\/([^\/]*)\/?$/) {
            return ["$2/$1/$3"];
        }
        warn "Subscription format: author/publication\n";
        return undef;
    },
);
#▶3 %sectionproprenames
%sectionproprenames=qw(
    startwith       rawstartwith
    allowwrite      rawwritepermissions
    denywrite       rawwritepermissions
    allowread       rawreadpermissions
    denyread        rawreadpermissions
    allowbooksread  rawbooksreadpermissions
    denybooksread   rawbooksreadpermissions
    homepage        www
    date            updated
    birthplace      rawtown
    top30           top
    subsection      rawsubsections
    bibliography    rawbibliography
    subscription    rawsubscriptions
);
#▶3 %sectioncanadd
%sectioncanadd=qw(
    title         1
    author        1
    annotation    1
    top30         1
    seealso       1
    subsection    1
    bibliography  1
    subscription  1
);
#▶3 %sectionremoves
%sectionremoves=(
    top30 => sub {
        my ($value, $author, $cref)=@_;
        return undef unless defined $cref->top30;
        local $_;
        my @newval=grep {
                (($_->{"authorlink"}."/".$_->{"publication"})!~/\Q$value/)
            }
            @{$cref->top};
        $cref->top(\@newval);
    },
    seealso => sub {
        my ($value, $author, $cref)=@_;
        return undef unless defined $cref->seealso;
        local $_;
        my @newval=grep { (((keys %$_)[0])!~/\Q$value/) }
            @{$cref->seealso};
        $cref->seealso(\@newval);
    },
    subsection => sub {
        my ($value, $author, $cref)=@_;
        $value=Encode::decode_utf8($value);
        return undef unless defined $cref->rawsubsections;
        local $_;
        my @newval=grep { (($_->{"TITL"})!~/\Q$value/) }
            @{$cref->rawsubsections};
        $cref->rawsubsections(\@newval);
    },
    subscription => sub {
        my ($value, $author, $cref)=@_;
        return undef unless defined $cref->rawsubsections;
        local $_;
        my @newval=grep { (($_)!~/\Q$value/) } @{$cref->rawsubscriptions};
        $cref->rawsubscriptions(\@newval);
    },
);
#▶3 %sectiondefaultadd
%sectiondefaultadd=qw(
    top30         1
    seealso       1
    subsection    1
    bibliography  1
    subscription  1
);
#▶3 %propsets
%propsets=(
    title      => $pass,
    author     => $pass,
    translator => $pass,
    annotation => $pass,
    section    => sub {
        return &ZLR::Stuff::getSimilar("sections", @_); },
    format => sub {
        return &ZLR::Stuff::getSimilar("formats", @_); },
    cperm => sub {
        return &ZLR::Stuff::getSimilar("rawcpermissions", @_); },
    genres => sub {
        my $value=shift;
        my $r={};
        my @v=split /,/, $value;
        GF: for my $val (@v) {
            my $rawvalue=&ZLR::Stuff::getSimilar("genres", $val, @_);
            return undef unless defined $rawvalue;
            $r={%$r, %$rawvalue};
        }
        return (scalar %$r)? $r : undef;
    },
    date         => $datechk,
    disablemarks => sub { return 0+not(shift);                       },
    enablemarks  => sub { return 0+not(not(shift));                  },
    denywrite    => sub { return {(2+(2*not(not(shift)))) => undef}; },
    denyread     => sub { return {(2+(2*not(not(shift)))) => undef}; },
    allowread    => sub { return {(2+(2*not(shift)))      => undef}; },
    allowwrite   => sub {
        my $v=shift;
        return {5 => undef} if($v eq "2");
        return {(2+(2*not($v))) => undef};
    },
);
#▶3 %canadd
%canadd=qw(
    title       1
    author      1
    translator  1
    annotation  1
    genres      1
);
#▶3 %proprenames
%proprenames=qw(
    section       rawsection
    disablemarks  canvote
    enablemarks   canvote
    allowwrite    rawwritepermissions
    denywrite     rawwritepermissions
    allowread     rawreadpermissions
    denyread      rawreadpermissions
    genres        rawgenre
    format        rawformat
    cperm         rawcpermissions
);
#▶2 Настройки по умолчанию
my $o_optdir       = $ENV{"HOME"} . "/.zlrchecker"; # Каталог с настройками
my $o_watchauthors = "watchauthors";                # Настройки
$o_verbose      = 0;
#▶2 Другие глобальные переменные
$VERSION="%PV%";
my $defopt="default options";
#▶2 Check
sub Check($) {
    #▶3 Объявление переменных
    my $author=shift;
    my %modified=();
    my %cmodified=();
    my @publications=keys %{$author->info->publications};
    push @publications, keys %{$author->savedinfo->publications};
    #▶3 Проверка по свойствам, полученным со страницы indexdate
    for my $publication (@publications) {
        next if(defined $modified{$publication});
        #▶4 Объявление переменных
        my $pubinfo=$author->info->publications->{$publication};
        my $savedpubinfo=$author->savedinfo->publications->{$publication};
        #▶4 Извещение об изменении комментариев
        if(defined $savedpubinfo and
           defined $pubinfo and
           not defined $cmodified{$publication} and
           $author->check("comments", $publication) and
           (($pubinfo->comments      || 0) !=
            ($savedpubinfo->comments || 0)))
        {
            $author->notify($publication, "comments");
            $cmodified{$publication}=1;
        }
        #▶4 Произведение было удалено
        if(not defined $pubinfo) {
            $author->notify($publication); }
        #▶4 Произведение отсутствует в старом списке
        elsif(not defined $savedpubinfo) {
            $pubinfo->getDate();
            $author->notify($publication);
            $modified{$publication}="date";
        }
        #▶4 Проверка, не изменились ли свойства
        else {
            # FIXME allow notifying about title/section/genrelist/imgcount
            # changes
            PROP: for my $prop ("size",#"title", "section", "genrelist",
                               #"imgcount"
                               )
            {   # Эта жуткая конструкция написана для того, чтобы perl не писал 
                # предупреждения вида
                #   «Use of uninitialized value in string eq at …».
                if((not
                    ((not defined $savedpubinfo->$prop) and
                     (not defined $pubinfo->$prop))) and
                   ((not defined $savedpubinfo->$prop) or
                    (not defined $pubinfo->$prop)  or
                    ($savedpubinfo->$prop ne $pubinfo->$prop)))
                {
                    $author->notify($publication, $prop);
                    $modified{$publication}=$prop;
                    $pubinfo->getDate();
                    last PROP;
                }
            }
        }
        #▶4 Копирование старых свойств, полученных со страницы произведения
        unless(not defined $pubinfo or defined $pubinfo->source) {
            for my $prop ("update", "source", "genres", "cdate",
                          "sectionlink", "format", "formatlink")
            {
                $pubinfo->$prop($savedpubinfo->$prop)
                    if(defined $savedpubinfo->$prop and
                       not defined $pubinfo->$prop);
            } }
        #▲4
    }
    #▶3 Проверка по дате обновления
    if($author->i_isold) {
       # Именно для этого цикла по ключю «ordered» сохранялась ссылка на массив 
       # с отсортированными в порядке уменьшения даты обновления произведениями.
       for my $publication (@{$author->info->ordered}) {
           #▶4 Нам уже известно, что произведение изменилось
           # поэтому ничего делать не надо
           next if(defined $modified{$publication});
           #▶4 Объявление переменных
           my $date=$author->info->publications->{$publication}->getDate();
           my $sdate=$author->savedinfo->publications->{$publication}->update;
           #▶4 Дата обновления изменилась
           if(not defined $sdate or $date ne $sdate)
           {
               $author->info->publications->{$publication}{"update"}=$date;
               $author->update($publication, "update");
           }
           #▶4 Дата обновления не изменилась — выходим из цикла
           else {
               last; }
           #▲4
        } }
    #▲3
    return $author;
}
#▶2 HandleAuthor: проверка обновления одного автора
sub HandleAuthor($$) {
    my $author = ZLR::Author->new((shift), 0, options => (shift));
    return undef unless defined $author;
    Check($author);
    $author->info->save($author->file);
    $author->info->save(($author->file).".cache");
}
#▶2 main
sub main {
    #▶3 Getopt
    #▶4 Объявление переменных
    my ($aname, $dest, $help, $watchauthors, $user, $password, $ua, $root,
        $publication, $url, $prefercache);
    my %cmdlineoptions=(
        acache  => undef,
        pdir    => undef,
        minescq => undef,
    );
    my %options=(
        "prefercache|c!"  => \$prefercache,
        "root|R=s"        => \$root,
        "author|A=s"      => \$aname,
        "publication|P=s" => \$publication,
        "url|U=s"         => \$url,
        "agent=s"         => \$ua,
        "user|u=s"        => \$user,
        "password=s"      => \$password,
        "watches|w=s"     => \$watchauthors,
        "optdir|o=s"      => \$o_optdir,
        "dest|O=s"        => \$dest,
        "verbose|v+"      => \$::o_verbose,
        "acache|a=s"      => \($cmdlineoptions{"acache"}),
        "pdir|p=s"        => \($cmdlineoptions{"pubdir"}),
        "minescq=f"       => \($cmdlineoptions{"minescq"}),
        "help|?|h"        => \$help,
    );
    #▲4
    Getopt::Long::GetOptions(%options);
    Pod::Usage::pod2usage("-verbose" => 1, "-exitstatus" => 0) if($help);
    #▶4 $url -> $root, $author, $publication
    if(defined $url) {
        my @components=split /\/+/, $url;
        pop @components
            if($components[-1] =~ /^index(date|vote|title|_\d+)\.shtml$/i);
        if($components[0] =~ /^https?:$/i) {
            $root="$components[0]//$components[1]";
            splice @components, 0, 2;
        }
        if(scalar @components > 2 and $components[-3] =~ /^.$/) {
            $aname=$components[-2];
            $publication=$components[-1];
            splice @components, -3, 3;
        }
        elsif(scalar @components > 1 and $components[-2] =~ /^.$/) {
            $aname=$components[-1];
            splice @components, -2, 2;
        }
        elsif(scalar @components == 2) {
            $aname=$components[0];
            $publication=$components[1];
            undef @components;
        }
        elsif(scalar @components == 1) {
            $aname=$components[-1];
            undef @components;
        }
        $publication=~s/\.shtml$//i if(defined $publication);
        if(not defined $root and scalar @components) {
            $root="http://".(join "/", @components); }
    }
    #▶4 $ua, $root, $user, $password
    ZLR::UA::setUA($ua);
    ZLR::UA::setRoot($root) if(defined $root);
    ZLR::UA::authorize($user, $password) or die "Failed to authorize"
        if(defined $user);
    #▶4 Очистка %cmdlineoptions
    foreach (keys %cmdlineoptions) {
        delete $cmdlineoptions{$_} unless defined $cmdlineoptions{$_}; }
    #▶3 Проверка правильности
    if(defined $watchauthors) {
        die "Watchauthors file $watchauthors not found" unless -r $watchauthors;
        $o_watchauthors=Cwd::realpath($watchauthors);
    }
    $dest=Cwd::realpath($dest) if(defined $dest);
    mkdir $o_optdir or die "Unable to create directory $o_optdir"
                    unless -d $o_optdir;
    my $curdir=Cwd::cwd();
    $::File::oldwd=Cwd::cwd();
    $::File::optdir=$o_optdir;
    #▶3 Настройки по умолчанию
    my $authoropts=::YAML::load($o_watchauthors);
    warn "Failed to load author options" unless defined $authoropts;
    my $options=ZLR::Author::mergeOptions(((defined $authoropts)?
                                               ($authoropts->{$defopt}):
                                               (undef)),
                                           \%cmdlineoptions);
    ZLR::Author::setDefaults($options);
    #▶3 $author, $pubinfo, $action
  ACTION:
    my ($author, $pubinfo);
    my $action=lc(shift(@ARGV));
    if($action ne ";") {
        $action=lc(shift(@ARGV)) if($action eq "--");
        if(defined $aname) {
            $author=ZLR::Author->new($aname, $prefercache,
                options => ZLR::Author::mergeOptions($authoropts->{$aname},
                                                     \%cmdlineoptions));
            die "Author “$aname” not found" unless defined $author;
            $author->info->publications->{"about"}=
                ZLR::PubInfo->new($author, info => {filename => "about"})
                    if($author->info->hasabout);
            $author->info->save(($author->file).".cache") unless $prefercache;
        }
        if(defined $publication) {
            $pubinfo=$author->info->publications->{$publication};
            die "Publication “$publication” not found" unless defined $pubinfo
                                                           or $action eq "add";
        }
    }
    else {
        $action=lc(shift(@ARGV));
    }
    #▶3 Установка свойств произведения и раздела
    if($action eq "set" or $action eq "sectionset" or $action eq "add") {
        #▶4 Объявление переменных
        my ($psets, $prenames, $cref, $cadd, $defadd, $removes, $FH);
        my %chlist;
        #▶5 $action eq "set"
        if($action eq "set") {
            die "You must specify publication" unless defined $pubinfo;
            $pubinfo->getEditInfo();
            $cref     = $pubinfo;
            $psets    = \%propsets;
            $prenames = \%proprenames;
            $cadd     = \%canadd;
            $defadd   = {};
            $removes  = {};
        }
        #▶5 $action eq "add"
        elsif($action eq "add") {
            warn "You specified existing publication: its text will be replaced"
                            if defined $pubinfo;
            die "You must specify publication" unless defined $publication;
            my $filename=shift(@ARGV);
            if(not defined $filename or $filename eq "-") {
                $FH=*STDIN; }
            else {
                $FH=::File::open($filename, 1, 0, 1); }
            $pubinfo=ZLR::PubInfo->new($author, "new" => $publication)
                    unless defined $pubinfo;
            @{[%{$author->info->publications}]}[1]->getEditInfo();
            $cref     = $pubinfo;
            $psets    = \%propsets;
            $prenames = \%proprenames;
            $cadd     = {};
            $defadd   = {};
            $removes  = {};
        }
        #▶5 $action eq "sectionset"
        else {
            die "You must specify author" unless defined $author;
            $author->info->getStartWithList();
            $author->info->getSecretEmail();
            $author->info->getEditInfo();
            $author->info->getTop();
            $author->info->getSeeAlso();
            $author->info->getSubSections();
            $author->info->getBibliography();
            $author->info->getSubscriptions();
            $cref     = $author->info;
            $psets    = \%sectionpropsets;
            $prenames = \%sectionproprenames;
            $cadd     = \%sectioncanadd;
            $defadd   = \%sectiondefaultadd;
            $removes  = \%sectionremoves;
        }
        #▶4 Изменение настроек
        while(scalar @ARGV) {
            #▶5 Объявление переменных
            my ($prop, $value);
            last if $ARGV[0] eq ";";
            $prop=shift(@ARGV);
            my $add=$defadd->{$prop};
            #▶5 Форма prop[+]=value
            if($prop=~/(\w+)([+\-]?=)(.*)/o) {
                $prop=$1;
                $value=$3;
                $add=$2;
                $add=(($add=~/^\+/o)?
                        (1):
                        (($add=~/^-/o)?
                            (-1):
                            (0)
                        )
                    );
            }
            #▶5 Форма prop value
            else {
                $value=shift(@ARGV); }
            #▶5 Проверки
            $prop  = lc $prop;
            $value = "" unless defined $value;
            $value=Encode::decode_utf8($value) unless Encode::is_utf8($value);
            my $sub=$psets->{$prop};
            die "Unknown property: $prop" unless defined $sub;
            if(not defined $add or $add != -1) {
                $value=$sub->($value, $author, $cref);
                die "Invalid value for property $prop" unless defined $value;
            }
            #▶5 Переименование
            my $oldprop=$prop;
            $prop=$prenames->{$prop} if(defined $prenames->{$prop});
            #▶5 Приравнивание
            if(not defined $add or $add==0) {
                $cref->$prop($value); }
            #▶5 Удаление
            elsif($add == -1) {
                my $sub=$removes->{$oldprop};
                die "Unable to remove property $prop" unless defined $sub;
                $sub->($value, $author, $cref);
            }
            #▶5 Добавление
            else {
                #▶6 К этому свойству нельзя ничего добавлять
                unless($cadd->{$oldprop}) {
                    warn "You cannot add to property $oldprop\n";
                    warn "Here is the list of properties you can add to:\n";
                    print STDERR ::YAML::dump([keys %$cadd]);
                    return 4;
                }
                #▲6
                my $oldvalue=$cref->$prop;
                if(ref $oldvalue eq "HASH") {
                    $cref->$prop({%$oldvalue, %$value}); }
                elsif(ref $oldvalue eq "ARRAY") {
                    $cref->$prop([@$oldvalue, @$value]); }
                elsif(ref $value) {
                    $cref->$prop($value); }
                else {
                    $cref->$prop("$oldvalue$value"); }
            }
            #▲5
            $chlist{$oldprop}=1;
        }
        #▶4 Сохранение изменений
        if($action eq "set" or $action eq "add") {
            $pubinfo->setEditInfo(undef, $FH); }
        else {
            $author->info->setStartWith()           if($chlist{"startwith"});
            $author->info->setTop()                 if($chlist{"top30"});
            $author->info->setSeeAlso()             if($chlist{"seealso"});
            $author->info->setSubSections()         if($chlist{"subsection"});
            $author->info->setBibliography()        if($chlist{"bibliography"});
            $author->info->setSubscriptions()       if($chlist{"subscription"});
            $author->info->setPasswd($cref->passwd) if($chlist{"passwd"} or
                                                       $chlist{"secretemail"});
            $author->info->setEditInfo()
                if(scalar(grep {
                        /read|
                         write|
                         date|
                         title|
                         author|
                         annotation|
                         homepage|
                         birthplace|
                         subscriptionemail|
                               (?<!.)email/ox } (keys %chlist)));
        }
    }
    #▶3 Печать различной информации
    elsif($action eq "print") {
        die "You must specify author" unless defined $author;
        my $info=$author->info;
        binmode STDOUT, ":utf8";
        while(scalar(@ARGV)) {
            last if $ARGV[0] eq ";";
            my $what=shift(@ARGV);
            if($what eq "friends") {
                $info->getFriends() unless defined $info->friends;
                local $,="\n";
                print @{$info->friends}, "";
            }
            elsif($what eq "subscriptions") {
                $info->getSubscriptions() or
                            die "Failed to get subsciption list"
                        unless defined $info->rawsubscriptions;
                local $,="\n";
                print @{$info->rawsubscriptions}, "";
            }
            elsif($what eq "publications") {
                local $,="\n";
                print((keys %{$info->publications}), "");
            }
            elsif($what =~ /^([+0])(.*)/o) {
                my $str=$2;
                local $\=(($1 eq "+")?("\n"):("\0"));
                for my $publication (keys %{$info->publications}) {
                    print $author->subs($str, $publication); }
            }
            elsif($what =~ /^%(.*)/o) {
                print $author->subs($1, $publication); }
            else {
                die "Unknown request: $what"; }
        }
    }
    #▶3 Загрузка изображений
    elsif($action eq "setimage") {
        die "You must specify author" unless defined $author;
        while(scalar(@ARGV)) {
            last if $ARGV[0] eq ";";
            my $n=shift(@ARGV);
            if($n=~/1|first|index|firstpage|main|author/) {
                $n=2; }
            elsif($n=~/2|second|about|information/) {
                $n=1; }
            else {
                warn "Possible image specifications:\n";
                warn "- 1 for image displayed on index (main) page\n";
                warn "- 2 for image displayed on about page\n";
                die "Invalid image specification";
            }
            my $imagefile=shift(@ARGV);
            $author->info->setImage($imagefile, $n);
        }
    }
    #▶3 Загрузка иллюстраций к произведению
    elsif($action eq "addillustration") {
        die "You must specify publication" unless defined $publication;
        my $filename=shift(@ARGV);
        die "Usage: addillustration filename [description [author]]"
            unless defined $filename;
        my $description = shift(@ARGV);
        my $aname       = shift(@ARGV);
        $pubinfo->addIllustration($filename, $description, $aname);
    }
    #▶3 Голосование
    elsif($action eq "vote") {
        die "You must specify publication" unless defined $pubinfo;
        my $mark=shift(@ARGV)+0;
        die "Invalid mark: $mark" unless 0 <= $mark and $mark <= 10;
        $pubinfo->vote($mark);
    }
    #▶3 Изменение списка друзей
    elsif($action eq "setfriends") {
        die "You must specify author" unless defined $author;
        $author->info->getFriends();
        local $_;
        delete $authoropts->{$defopt};
        while(scalar(@ARGV)) {
            last if $ARGV[0] eq ";";
            my $friend=lc(shift(@ARGV));
            if($friend =~ /^:(un)?subscribe/o) {
                $author->info->frsubscribe(0+(not(defined $1)));
                next;
            }
            die "Invalid friend: “$friend”"
                unless $friend=~/^[+\-=]?([0-9a-z_]+|~)$/;
            my $action;
            if($friend=~s/^([+\-=])//o) {
                $action=$1; }
            if(defined $action and $action eq "=") {
                $author->info->friends([]); }
            if(defined $action and $action eq "-") {
                if($friend eq '~') {
                    my %friends=(map {($_ => 1)} @{$author->info->friends});
                    foreach (keys %$authoropts) {
                        delete $friends{$_} if(defined $friends{$_}); }
                    $author->info->friends([keys %friends]);
                }
                else {
                    $author->info->friends([grep {$_ ne $friend}
                                            @{$author->info->friends}]); }
            }
            else {
                if($friend eq '~') {
                    push @{$author->info->friends}, (keys %$authoropts); }
                else {
                    push @{$author->info->friends}, $friend; }
            }
        }
        $author->info->setFriends();
    }
    #▶3 Сохранение всего авторского раздела
    elsif($action eq "savesection") {
        #▶4 Проверки аргументов
        die "You must specify destination" unless defined $dest;
        die "You must specify author"      unless defined $author;
        #▶4 Объявление переменных
        #▶5 $opts
        my %opts=(
            comments      => 1,
            original      => 1,
            info          => 1,
            illustrations => 1,
            bibliography  => 1,
            top           => 1,
            seealso       => 1,
            friends       => 1,
        );
        local $_;
        while(scalar @ARGV) {
            last if $ARGV[0] eq ";";
            $_=shift(@ARGV);
            if(/^([+\-=])?(((\w+),)*(\w+))/o) {
                my $w  = $1;
                my @os = split /,/, $2;
                if($w eq "=") {
                    %opts=(); }
                if($w eq "-") {
                    for my $o (@os) {
                        my $lo=lc $o;
                        delete $opts{$lo} if(defined $opts{$lo});
                    }
                }
                else {
                    for my $o (@os) { $opts{lc $o}=1; } }
            }
            else { die "Invalid argument: $_"; }
        }
        #▶5 $arch
        my $arch=main::Archive->new($dest);
        #▶4 Обработка произведений
        for my $publication (keys %{$author->info->publications}) {
            my $pubinfo=$author->info->publications->{$publication};
            print STDERR "Processing $publication\n" if($::o_verbose);
            #▶5 html|fb2|original
            if($opts{"original"} or $opts{"html"} or $opts{"fb2"}) {
                my $purl=$pubinfo->url;
                my $text=ZLR::UA::get($purl);
                $arch->add("t-$publication.shtml", $text)
                    if($opts{"original"});
                if($opts{"html"} or $opts{"fb2"}) {
                    my $html=::Convert::zlrPrepare($text, $pubinfo);
                    $arch->add("t-$publication.html", $html) if($opts{"html"});
                    if($opts{"fb2"}) {
                        my $imgarch;
                        $imgarch=$arch unless($author->o_alwaysbinary);
                        my $fb2=::Convert::toFB2($html, $imgarch, $pubinfo,
                                                 $purl);
                        $arch->add("t-$publication.fb2", $fb2);
                    }
                }
                elsif($opts{"info"}) {
                    $pubinfo->getInfo($text); }
            }
            #▶5 info & ! html|fb2|original
            elsif($opts{"info"}) {
                $pubinfo->getDate(); }
            #▶5 edininfo
            if($opts{"info"} and defined $pubinfo->editprop) {
                $pubinfo->getEditInfo();
            }
            #▶5 illustrations
            if($opts{"illustrations"} and $pubinfo->imgcount) {
                $pubinfo->getIllustrations();
                for my $iref (keys %{$pubinfo->illustrations || {}},
                              keys %{$pubinfo->additions     || {}}) {
                    my $image=ZLR::UA::get($iref);
                    unless(defined $image) {
                        warn "Failed to load illustration $iref ".
                                                        "for $publication"; }
                    else {
                        my $imgname=$iref;
                        $imgname=~s/.*\/([^\/]+)$/$1/;
                        $arch->add("images/$aname/$publication/$imgname",
                                   $image);
                    }
                }
            }
            #▶5 comments
            if($opts{"comments"}) {
                my $curl=$pubinfo->url("comments");
                my $FH=$arch->add("c-$publication.yaml");
                ZLR::Comments->new($pubinfo)->getAllComments($FH);
            }
        }
        #▶4 Дополнительная информация
        #▶5 Иллюстрации к разделу
        if($opts{"illustrations"}) {
            $author->info->getImages();
            if(defined $author->info->sectionimages) {
                for my $imglink (@{$author->info->sectionimages}) {
                    my $image=ZLR::UA::get($imglink);
                    next unless defined $image;
                    $imglink=~s/^.*\///o;
                    $arch->add("images/+section/$imglink", $image);
                }
            }
        }
        #▶5 Библиография
        $author->info->getBibliography() if($opts{"bibliography"});
        $arch->add("I-bibliography.yaml",
                ::YAML::dump($author->info->bibliography))
                    if($opts{"bibliography"} and
                       defined $author->info->bibliography);
        #▶5 Список лучших
        $author->info->getTop() if($opts{"top"});
        $arch->add("I-top.yaml", ::YAML::dump($author->info->top))
                    if($opts{"top"} and defined $author->info->top);
        #▶5 См. также
        $author->info->getSeeAlso() if($opts{"seealso"});
        $arch->add("I-seealso.yaml", ::YAML::dump($author->info->seealso))
                    if($opts{"seealso"} and defined $author->info->seealso);
        #▶5 Список дружеских разделов
        $author->info->getFriends() if($opts{"friends"});
        $arch->add("I-friends.yaml", ::YAML::dump($author->info->friends))
                    if($opts{"friends"} and defined $author->info->friends);
        #▶5 info
        $author->info->getSecretEmail();
        $author->info->getEditInfo();
        $author->info->getSubSections();
        $author->info->getSubscriptions();
        $arch->add("I-info.yaml", $author->info->save()) if($opts{"info"});
        #▶4 Сохранение и выход
        close $arch;
        return 0;
    }
    #▶3 Отправка комментария
    elsif($action eq "post") {
        my $text;
        die "You must specify publication" unless defined $pubinfo;
        my $c=ZLR::Comments->new($pubinfo);
        my $tprintf;
        while(scalar @ARGV) {
            last if $ARGV[0] eq ";";
            my $o=shift(@ARGV);
            my $v=shift(@ARGV);
            my $printf;
            if($o=~s/%$//) {
                $printf=1; }
            if($printf and $o ne "file") {
                $v=$author->subs($v, $publication); }
            $v=Encode::decode_utf8($v) unless Encode::is_utf8($v);
            if(defined $text and ($o eq "file" or
                                  $o eq "text" or
                                  not defined $v))
            {
                die "Too many texts specified";
            }
            elsif($o eq "file" or not defined $v) {
                $v=$o unless defined $v;
                my $FH;
                if($v eq "-") { $FH=*STDIN; }
                else          { $FH=::File::open($v, 1, 0, 1); }
                sysread $FH, $text, $o_maxcommentlen;
                close $FH;
                $tprintf=$printf;
            }
            elsif($o eq "text") {
                $tprintf=$printf;
                $text=substr $v, 0, $o_maxcommentlen;
            }
            elsif($o eq "name")     { $c->o_name($v);  }
            elsif($o eq "email")    { $c->o_email($v); }
            elsif($o eq "homepage") { $c->o_url($v);   }
            else {
                die "Unknown option: $o";
            }
        }
        die "You must provide comment text" unless defined $text;
        $c->postComment((($tprintf)?($author->subs($text, $publication)):
                                   ($text)));
    }
    #▶3 Создание резервной копии
    elsif($action eq "backup") {
        die "You must specify author"      unless defined $author;
        die "You must specify destination" unless defined $dest;
        $author->backup($dest);
    }
    #▶3 Восстановление из резервной копии
    elsif($action eq "restore") {
        die "You must specify author"      unless defined $author;
        die "You must specify destination" unless defined $dest;
        $author->restore($dest);
    }
    #▶3 Удаление произведения
    elsif($action eq "delete") {
        die "You must specify publication" unless defined $pubinfo;
        $pubinfo->getEditInfo();
        $pubinfo->setEditInfo("Delete");
    }
    #▶3 Получение комментариев
    elsif($action eq "savecomments") {
        die "You must specify publication" unless defined $pubinfo;
        #▶4 $FH
        my $FH;
        if(defined $dest) {
            $FH=::File::open($dest, 1, 1, 1); }
        else {
            $FH=*STDOUT; }
        #▲4
        ZLR::Comments->new($pubinfo)->getAllComments($FH);
        #▶4 close/return
        close $FH;
        return 0;
    }
    #▶3 Получение HTML или fb2
    elsif($action eq "savehtml" or $action eq "savefb2") {
        die "You must specify publication" unless defined $pubinfo;
        #▶4 Получение HTML
        my $url=$pubinfo->url;
        my $text=ZLR::UA::get($url);
        die "Failed to download $url!" unless defined $text;
        my $html=::Convert::zlrPrepare($author, $pubinfo, $text);
        if($action eq "savehtml") {
            my $FH;
            unless(defined $dest) {
                $FH=*STDOUT; }
            else {
                $FH=::FILE::open($dest, 1, 1); }
            print $FH $html;
            close $FH;
            return 0;
        }
        #▶4 Получение FB2
        my $arch;
        my $imgarch;
        if(defined $dest) {
            $arch=main::Archive->new($dest);
            $imgarch=$arch unless($author->o_alwaysbinary);
        }
        my $fb2=::Convert::toFB2($html, $imgarch, $pubinfo, $url);
        die "Failed to format to fb2!" unless defined $fb2;
        if(defined $dest) {
            $arch->add("text.fb2", $fb2);
            close $arch;
        }
        else {
            print $fb2; }
        #▲4
        return 0;
    }
    #▶3 Получение FB2 из сторонних источников
    elsif($action eq "converthtml") {
        die "No publications are allowed" if(defined $pubinfo);
        my $arch;
        my $imgarch;
        if(defined $dest) {
            $arch=main::Archive->new($dest);
            $imgarch=$arch unless($author->o_alwaysbinary);
        }
        my $text;
        if(scalar @ARGV) {
            my $url=shift @ARGV;
            $text=ZLR::UA::get($url);
        }
        else {
            $text=join "\n", <STDIN>;
            $text=Encode::decode_utf8($text) unless Encode::is_utf8($text);
        }
        my $html=::Convert::typo($text, ZLR::Author->new());
        my $fb2=::Convert::htmlToFB2($html, $imgarch, $url);
        die "Failed to format to fb2!" unless defined $fb2;
        if(defined $dest) {
            $arch->add("text.fb2", $fb2);
            close $arch;
        }
        else {
            print $fb2; }
    }
    #▶3 Проверка обновлений
    elsif(not defined $action or $action eq "" or $action eq "check") {
        #▶4 Несколько дополнительных проверок
        die "No authors listed" unless defined $authoropts and
                                       scalar %$authoropts;
        die "Having only default options is not enough"
            if((scalar keys %$authoropts)==1 and
               defined $authoropts->{$defopt});
        #▶4 Получение обновлений
        local $_;
        foreach (keys %{$authoropts}) {
            next if($_ eq $defopt);
            my $aopts=$authoropts->{$_};
            HandleAuthor($_, $aopts);
        }
        #▲4
    }
    #▶3 Неизвестное действие
    else {
        die "Unknown action: $action"; }
    #▲3
    goto ACTION if scalar @ARGV and $ARGV[0] eq ";";
    return 0;
}
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
exit main(@ARGV);
# Regular expression for searching for missing `g' flags:
# ^\s*s\(\W\)\(\(\\\1\|\1\@!\_.\)*\)\@>\1\(\(\\\1\|\1\@!\_.\)*\)\@>\1\([^;g]*\);
#▶1 POD for --help switch
