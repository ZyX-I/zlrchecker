# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

DESCRIPTION="Check updates on samlib.ru authors' pages"
HOMEPAGE="http://zlrchecker.sourceforge.net"
SRC_URI="http://downloads.sourceforge.net/project/zlrchecker/${PV}/${P}.tar.xz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~m68k ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86 ~sparc-fbsd ~x86-fbsd"
IUSE=""

DEPEND="
	dev-lang/perl
	app-arch/xz-utils
"
	#virtual/perl-Getopt-Long
RDEPEND="
	dev-perl/libwww-perl
	dev-perl/HTML-Tree
	dev-perl/yaml
	dev-perl/XML-DOM
	virtual/perl-IO-Compress
	dev-perl/ImageInfo
	dev-perl/Term-ReadPassword

	${DEPEND}
"
src_compile() {
	emake DESTDIR="${D}" PV="${PV}" install
}
src_install() {
	doman *.1
	dodoc *.html
	dodoc *.pod
	dodoc Changelog
	dobin zlrchecker.pl
	dosym zlrchecker.pl /usr/bin/zlrchecker
	insinto /usr/share/man/ru/man1
	doins ru/*.1
}
pkg_postinst() {
	ewarn "Note for both Gentoo and other linux distro users:"
	ewarn "in order to use zlrchecker you must install YAML::LibYAML"
	ewarn "(using g-cpan -i YAML::LibYAML or from perl-experimental overlay)"
	ewarn "because dev-perl/yaml package fails to load some files generated"
	ewarn "by libyaml which is used on other linux distros."
}

