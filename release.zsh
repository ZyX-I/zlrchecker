#!/bin/zsh
typeset -gr             PN=$(basename $PWD)
typeset -gr             PV=$1
typeset -gr              P=${PN}-${PV}
typeset -gr       CATEGORY=www-misc

typeset -gr    COMPRESSPRG=xz
typeset -gr COMPRESSSUFFIX=.xz

typeset -gr           USER="zyxsf,${PN}"
typeset -gr         SERVER=frs.sourceforge.net
typeset -gr          PROOT=/home/frs/project/${PN[1]}/${PN[1,2]}/${PN}
typeset -gr           DEST=${USER}@${SERVER}:${PROOT}

typeset -gr           RDIR=$(realpath -s "r/${PV}")

typeset -gr           ARCH=${RDIR}/${P}.tar
typeset -gr COMPRESSEDARCH=${ARCH}${COMPRESSSUFFIX}

typeset -gr          DROOT=${RDIR}/debian

typeset -gr          RROOT=${HOME}/rpmbuild

typeset -gr          GROOT=$(realpath -s r/repository)
typeset -gr           DDIR=$(realpath -s r/distfiles)
typeset -gr       REPONAME=libru
typeset -gr           EDIR=${GROOT}/${CATEGORY}/${PN}
typeset -gr         EBUILD=${PN}.ebuild
typeset -gr      NEWEBUILD=${EDIR}/${P}.ebuild

[[ -e ${RDIR} ]] && exit 1
fakeroot -- zsh -c "
    mkdir -p ${RDIR}
    mkdir -p ${DDIR}
    hg archive -t tar ${ARCH}
    ${COMPRESSPRG} -z -c ${ARCH} > ${COMPRESSEDARCH} && rm ${ARCH} || exit 2
    cp ${COMPRESSEDARCH} ${DDIR}

    rm -rf ${RROOT}
    mkdir -p ${RROOT}
    mkdir -p ${RROOT}/BUILD
    mkdir -p ${RROOT}/SOURCES
    mkdir -p ${RROOT}/SPECS
    mkdir -p ${RROOT}/tmp
    mkdir -p ${RROOT}/RPMS
    mkdir -p ${RROOT}/SRPMS
    cp ${ARCH}.xz ${RROOT}/SOURCES
    chown root:root -R ${RROOT}
    cat ${PN}.spec | sed -e 's/%PV%/${PV}/g' | grep -v '^BuildRequires' > ${RROOT}/SPECS/${PN}.spec
    rpmbuild -bb ${RROOT}/SPECS/${PN}.spec
    cp ${RROOT}/RPMS/*/${P}*.rpm ${RDIR}
    rm -rf ${RROOT}

    rpm2tar -O ${RDIR}/${P}*.rpm > ${RDIR}/${P}.tar

    mkdir -p ${DROOT}
    mkdir -p ${DROOT}/DEBIAN
    find ${DROOT} -type d -exec chmod 755 '{}' +
    tar -xvf ${RDIR}/${P}.tar -C ${DROOT}
    cat ${PN}.dsc | sed -e 's/%PV%/${PV}/g' > ${DROOT}/DEBIAN/control
    dpkg-deb --build ${DROOT}
    mv ${DROOT}.deb ${RDIR}/${P}.deb
    rm -rf ${DROOT}

    gzip -9 ${RDIR}/${P}.tar

    mkdir -p ${EDIR}
    cp ${EBUILD} ${NEWEBUILD}
    mkdir -p ${GROOT}/profiles
    echo ${REPONAME} > ${GROOT}/profiles/repo_name
    if ! grep '^${CATEGORY}$' ${GROOT}/profiles/categories
    then
        echo ${CATEGORY} >> ${GROOT}/profiles/categories
    fi
    appareo --manifest --repository-dir ${GROOT} --download-dir ${DDIR}
    cp ${NEWEBUILD} $(dirname $NEWEBUILD)/Manifest ${RDIR}
"
[[ "$2" == "test" ]] && exit $?
# echo -n "Password for $USER:"
# read -r -s PASSWORD
# echo
# PASSWORD="${PASSWORD}\r"
# EXPECTSCRIPT="
# set timeout -1
# eval spawn \$argv
# expect *password:*
# send ${(qqq)PASSWORD}
# wait
# "
# echo $EXPECTSCRIPT | expect -f - \ 
scp -r $RDIR $GROOT $DEST || exit $?
echo
hg tag release-${PV} || exit $?
# echo $EXPECTSCRIPT | expect -f - \ 
hg push
echo
