Summary: Check updates on samlib.ru authors' pages.
Name: zlrchecker
Version: %PV%
Release: 1%{?dist}
License: GPL-3
Group: Web
Source: http://downloads.sourceforge.net/project/zlrchecker/%{version}/%{name}-%{version}.tar.xz
BuildRoot: %{_tmppath}/%{name}-%{version}
BuildRequires: perl
BuildRequires: xz
Requires: perl
Requires: perl-libwww-perl
Requires: perl-YAML-LibYAML
Requires: perl-HTML-Tree
Requires: perl-XML-DOM
Requires: perl-Image-Info
Requires: perl-Term-ReadPassword
URL: http://zlrchecker.sourceforge.net
%description
Zlrchecker is a program that allows you to check for publication updates and 
new comments on samlib.ru site, generate fictionbook-2.1 (FB2) files out 
of publication text and back up the comments for the given publication.

Update notifications are done using RSS feed, but you will need a web server to 
view it not only on the local machine.
%prep
rm -rf %{name}-%{version}
tar --use-compress-program=xz -xf ../SOURCES/%{name}-%{version}.tar.xz
cd %{name}-%{version}
chown -R root:root .
chmod -R a+rX,g-w,o-w .

%build
cd %{name}-%{version}
make PV=%{version}

%install
cd %{name}-%{version}
rm -rf %{buildroot}
make DESTDIR=%{buildroot} PV=%{version} install
find %{buildroot}/usr/share/man -type f -exec gzip -9 '{}' +
chown root:root -R %{buildroot}
find %{buildroot}/usr -type f -print -o -type l -print | sed "s@^%{buildroot}@@" > %{name}-%{version}-filelist
%files -f %{name}-%{version}/%{name}-%{version}-filelist

%clean
rm -rf %{name}-%{version}
rm -rf %{buildroot}

