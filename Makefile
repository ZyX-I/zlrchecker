POD2MAN=pod2man
POD2HTML=pod2html
MKDIR_P=mkdir -p
INSTALL=install
TAR=tar
CP=cp
RM=rm
RMDIR=rmdir
LN_S=ln -s
CAT=cat
SED=sed

PN=zlrchecker
P=$(PN)-$(PV)

DESTDIR=/
prefix=$(DESTDIR)/usr
exec_prefix=$(prefix)
bindir=$(exec_prefix)/bin

MANDIR=$(prefix)/share/man
MAN1DIR=$(MANDIR)/man1
RUMAN1DIR=$(MANDIR)/ru/man1
DOCDIR=$(prefix)/share/doc/$(P)

doc: zlrchecker.pod zlrchecker-ru.pod
	test -e ru || $(MKDIR_P) ru
	$(POD2MAN) $(PN).pod > $(PN).1
	$(POD2MAN) $(PN)-ru.pod > ru/$(PN).1
	$(POD2HTML) $(PN).pod > $(PN).html
	$(POD2HTML) $(PN)-ru.pod > $(PN)-ru.html
	$(RM) -f pod2htmi.tmp pod2htmd.tmp
	$(SED) -r -i -e 's/%PV%/$(PV)/g' $(PN).pl
	$(CAT) $(PN).pod >> $(PN).pl
install: doc zlrchecker.pl
	test -e $(MAN1DIR)   || $(MKDIR_P) $(MAN1DIR)
	test -e $(RUMAN1DIR) || $(MKDIR_P) $(RUMAN1DIR)
	test -e $(DOCDIR)    || $(MKDIR_P) $(DOCDIR)
	test -e $(bindir)    || $(MKDIR_P) $(bindir)
	$(INSTALL) -m 0644 $(PN).1 $(MAN1DIR)
	$(INSTALL) -m 0644 ru/$(PN).1 $(RUMAN1DIR)/$(PN).1
	$(INSTALL) -m 0644 $(PN).html $(DOCDIR)
	$(INSTALL) -m 0644 $(PN)-ru.html $(DOCDIR)
	$(INSTALL) -m 0644 $(PN).pod $(DOCDIR)
	$(INSTALL) -m 0644 $(PN)-ru.pod $(DOCDIR)
	$(INSTALL) -m 0644 Changelog $(DOCDIR)
	$(INSTALL) -m 0755 $(PN).pl $(bindir)
	$(LN_S) -s zlrchecker.pl $(bindir)/zlrchecker
clean:
	$(RM) -f $(PN).1 ru/$(PN).1 $(PN).html $(PN)-ru.html
	$(RMDIR) ru

