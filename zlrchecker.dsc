Package: zlrchecker
Version: %PV%
Section: web
Priority: optional
Architecture: all
Depends: perl, libwww-perl, libyaml-perl, libhtml-tree-perl, libxml-dom-perl, libimage-info-perl, libterm-readpassword-perl
Pre-depends: perl
Maintainer: Nikolay Pavlov <kp-pav@yandex.ru>
Description: Check updates on samlib.ru authors' pages.
 Zlrchecker is a program that allows you to check for publication updates and 
 new comments on samlib.ru site, generate fictionbook-2.1 (FB2) files out 
 of publication text and back up the comments for the given publication.
 .
 Update notifications are done using RSS feed, but you will need a web server to 
 view it not only on the local machine.
